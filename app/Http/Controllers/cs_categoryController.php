<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Flash;
use Response;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;

use App\Models\oc_product_to_category;
use Cart;

class cs_categoryController extends Controller
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
    }

    /**
     * Display a listing of the cs_category.
     *
     * @var $category
     * @param Request $request
     * @return Response
     */
    public function index($category, Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $ocProductToCategory = oc_product_to_category::where('category_id',$category)->get();

        if (empty(oc_product_to_category::where('category_id',$category)->first())) {
            Flash::error('Product not found');

            return redirect(route('cs.home'));
        }

        return view('cs_category.index',[
                        'ocProductToCategory' => $ocProductToCategory, 
                        'ocCategories' => $ocCategories
                    ]
                );
    }

    /**
     * Display a listing of the cs_category.
     *
     * @var $category
     * @param Request $request
     * @return Response
     */
    public function mobile($category, Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $ocProductToCategory = oc_product_to_category::where('category_id',$category)->get();

        if (empty(oc_product_to_category::where('category_id',$category)->first())) {
            Flash::error('Product not found');

            return redirect(route('cs.home'));
        }

        return view('mobile.cs_category.index',[
                        'ocProductToCategory' => $ocProductToCategory, 
                        'ocCategories' => $ocCategories
                    ]
                );
    }
}
