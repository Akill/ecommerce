<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'username', 'phone', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'status', 'mobile'
    ];

    public function getPictureAttribute($picture)
    {
        $picture = $picture ? url('storage/'.$picture): asset('photo/avatar.png')  ;
        
        return $picture;
    }

    public function getStatusAttribute()
    {
        $str = ($this->access == 'admin') ? true : false;
        return $str;
    }

    public function getMobileAttribute()
    {
        $str = ($this->access == 'admin') ? true : false;
        return $str;
    }
}
