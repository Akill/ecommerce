<?php

namespace App\Repositories;

use App\Models\oc_category_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_category_descriptionRepository
 * @package App\Repositories
 * @version October 14, 2017, 5:09 pm UTC
 *
 * @method oc_category_description findWithoutFail($id, $columns = ['*'])
 * @method oc_category_description find($id, $columns = ['*'])
 * @method oc_category_description first($columns = ['*'])
*/
class oc_category_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'oc_category_id',
        'language_id',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category_description::class;
    }
}
