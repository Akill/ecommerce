<table class="table table-responsive" id="ocLanguages-table">
    <thead>
        <tr>
            <th>Nama</th>
        <th>Kode</th>
        <th>Locale</th>
        <th>Gambar</th>
        <th>Directory</th>
        <th>Sort Order</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocLanguages as $ocLanguage)
        <tr>
            <td>{!! $ocLanguage->name !!}</td>
            <td>{!! $ocLanguage->code !!}</td>
            <td>{!! $ocLanguage->locale !!}</td>
            <td>{!! $ocLanguage->image !!}</td>
            <td>{!! $ocLanguage->directory !!}</td>
            <td>{!! $ocLanguage->sort_order !!}</td>
            <td>{!! $ocLanguage->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocLanguages.destroy', $ocLanguage->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocLanguages.show', [$ocLanguage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocLanguages.edit', [$ocLanguage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>