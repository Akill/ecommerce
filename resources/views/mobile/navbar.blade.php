    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">Celebestore</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">shopping_cart</i>
                            <span class="label-count">{{Cart::count()}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">KERANJANG</li>
                            <li class="body">
                                <ul class="menu">
                                @foreach(Cart::content() as $carts)
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>{{\Illuminate\Support\Str::words($carts->name, 3,'....')}}</h4>
                                                <p>
                                                    <i class="material-icons">monetization_on</i> 
                                                    <span>Harga</span> Rp. {{number_format($carts->price,2,',','.')}}
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="{{ url('/mobile/cart') }}">Lihat Keranjang</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>