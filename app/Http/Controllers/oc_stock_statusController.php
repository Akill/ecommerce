<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_stock_statusRequest;
use App\Http\Requests\Updateoc_stock_statusRequest;
use App\Repositories\oc_stock_statusRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_stock_statusController extends AppBaseController
{
    /** @var  oc_stock_statusRepository */
    private $ocStockStatusRepository;

    public function __construct(oc_stock_statusRepository $ocStockStatusRepo)
    {
        $this->ocStockStatusRepository = $ocStockStatusRepo;
    }

    /**
     * Display a listing of the oc_stock_status.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocStockStatusRepository->pushCriteria(new RequestCriteria($request));
        $ocStockStatuses = $this->ocStockStatusRepository->all();

        return view('oc_stock_statuses.index')
            ->with('ocStockStatuses', $ocStockStatuses);
    }

    /**
     * Show the form for creating a new oc_stock_status.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_stock_statuses.create');
    }

    /**
     * Store a newly created oc_stock_status in storage.
     *
     * @param Createoc_stock_statusRequest $request
     *
     * @return Response
     */
    public function store(Createoc_stock_statusRequest $request)
    {
        $input = $request->all();

        $ocStockStatus = $this->ocStockStatusRepository->create($input);

        Flash::success('Oc Stock Status saved successfully.');

        return redirect(route('ocStockStatuses.index'));
    }

    /**
     * Display the specified oc_stock_status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            Flash::error('Oc Stock Status not found');

            return redirect(route('ocStockStatuses.index'));
        }

        return view('oc_stock_statuses.show')->with('ocStockStatus', $ocStockStatus);
    }

    /**
     * Show the form for editing the specified oc_stock_status.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            Flash::error('Oc Stock Status not found');

            return redirect(route('ocStockStatuses.index'));
        }

        return view('oc_stock_statuses.edit')->with('ocStockStatus', $ocStockStatus);
    }

    /**
     * Update the specified oc_stock_status in storage.
     *
     * @param  int              $id
     * @param Updateoc_stock_statusRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_stock_statusRequest $request)
    {
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            Flash::error('Oc Stock Status not found');

            return redirect(route('ocStockStatuses.index'));
        }

        $ocStockStatus = $this->ocStockStatusRepository->update($request->all(), $id);

        Flash::success('Oc Stock Status updated successfully.');

        return redirect(route('ocStockStatuses.index'));
    }

    /**
     * Remove the specified oc_stock_status from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            Flash::error('Oc Stock Status not found');

            return redirect(route('ocStockStatuses.index'));
        }

        $this->ocStockStatusRepository->delete($id);

        Flash::success('Oc Stock Status deleted successfully.');

        return redirect(route('ocStockStatuses.index'));
    }
}
