<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <select class="form-control" id="status" name="status">
    <option value="pending">Pending</option>
    <option value="sukses">Sukses</option>
    <option value="batal">Batal</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csTransaksis.index') !!}" class="btn btn-default">Cancel</a>
</div>
