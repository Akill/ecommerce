<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_description
 * @package App\Models
 * @version October 14, 2017, 9:32 pm UTC
 *
 * @property integer product_id
 * @property integer language_id
 * @property string name
 * @property string description
 * @property string tag
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 */
class oc_product_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'user_id',
        'language_id',
        'name',
        'description',
        'tag',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'product_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'tag' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'meta_keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Get the oc_product that wrote the oc_product_description.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\oc_product');
    }
    
}
