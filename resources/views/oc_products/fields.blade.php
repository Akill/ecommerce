<div class="form-group col-sm-12">    
    <!-- Model Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('model', 'Model:') !!}
        {!! Form::text('model', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Location Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('location', 'Lokasi:') !!}
        @php $prov = RajaOngkir::Provinsi()->all(); @endphp
        <select class="form-control" id="location" name="location">
        @foreach($prov as $i)
        <optgroup label="{{$i['province']}}">
            @php $kota = RajaOngkir::Kota()->find($i['province_id']); @endphp
            @foreach($kota as $ko)
            <option value="{{$ko}}">{{$ko}}</option>
            @endforeach
        </optgroup>
        @endforeach
        </select>
    </div>

    <!-- Quantity Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('quantity', 'Jumlah:') !!}
        {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Stock Status Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('stock_status_id', 'Stok Status:') !!}
        @php $items = App\Models\oc_stock_status::all(['name','id']) @endphp
        <select class="form-control" id="stock_status_id" name="stock_status_id">
        @foreach($items as $i)
        <option value="{{$i->id}}">{{$i->name}}</option>
        @endforeach
        </select>
    </div>

    <!-- Manufacturer Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('manufacturer_id', 'Manufaktur:') !!}
        @php $items = App\Models\oc_manufacturer::all(['name','id']) @endphp
        <select class="form-control" id="manufacturer_id" name="manufacturer_id">
        @foreach($items as $i)
        <option value="{{$i->id}}">{{$i->name}}</option>
        @endforeach
        </select>
    </div>

    <!-- Price Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('price', 'Harga:') !!}
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tax Class Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tax_class_id', 'Kelas Pajak:') !!}
        @php $items = App\Models\oc_tax_class::all(['title','id']) @endphp
        <select class="form-control" id="tax_class_id" name="tax_class_id">
        @foreach($items as $i)
        <option value="{{$i->id}}">{{$i->title}}</option>
        @endforeach
        </select>
    </div>

    <!-- Weight Class Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('weight_class_id', 'Kelas Berat:') !!}
        @php $items = App\Models\oc_weight_class::all(['value','id']) @endphp
        <select class="form-control" id="weight_class_id" name="weight_class_id">
        @foreach($items as $i)
        <option value="{{$i->id}}">{{$i->value}}</option>
        @endforeach
        </select>
    </div>

    <!-- Weight Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('weight', 'Berat:') !!}
        {!! Form::number('weight', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Length Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('length', 'Panjang:') !!}
        {!! Form::number('length', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Width Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('width', 'Lebar:') !!}
        {!! Form::number('width', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Height Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('height', 'Tinggi:') !!}
        {!! Form::number('height', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Length Class Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('length_class_id', 'Kelas Panjang:') !!}
        @php $items = App\Models\oc_length_class::all(['value','id']) @endphp
        <select class="form-control" id="length_class_id" name="length_class_id">
        @foreach($items as $i)
        <option value="{{$i->id}}">{{$i->value}}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group col-sm-6">
        <!-- Image Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('image', 'Gambar:') !!}
            {!! Form::file('image',null,array('id'=>'image','class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group col-sm-12">
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-default btn-block" data-toggle="modal" data-target="#myModal">Detail Product</button>
    </div>
</div>
<div class="form-group col-sm-12">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('ocProducts.index') !!}" class="btn btn-default">Batal</a>
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Produk</h4>
      </div>
      <div class="modal-body">
            <!-- Sku Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('sku', 'SKU:') !!}
                {!! Form::text('sku', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Upc Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('upc', 'UPC:') !!}
                {!! Form::text('upc', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Ean Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('ean', 'EAN:') !!}
                {!! Form::text('ean', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Jan Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('jan', 'JAN:') !!}
                {!! Form::text('jan', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Isbn Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('isbn', 'ISBN:') !!}
                {!! Form::text('isbn', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Mpn Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('mpn', 'MPN:') !!}
                {!! Form::text('mpn', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Points Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('points', 'Poin:') !!}
                {!! Form::number('points', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Date Available Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('date_available', 'Tanggal Berlaku:') !!}
                {!! Form::date('date_available', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Minimum Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('minimum', 'Minimum:') !!}
                {!! Form::number('minimum', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Sort Order Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('sort_order', 'Urutan Order:') !!}
                {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Viewed Field -->
            <div class="form-group col-sm-12">
                {!! Form::label('viewed', 'Tampilan:') !!}
                {!! Form::number('viewed', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Status Field -->
            <div class="form-group col-sm-4">
                {!! Form::label('status', 'Status:') !!}
                <label class="checkbox-inline">
                    {!! Form::hidden('status', false) !!}
                    {!! Form::checkbox('status', '1', '0') !!} Yes
                </label>
            </div>

            <!-- Shipping Field -->
            <div class="form-group col-sm-4">
                {!! Form::label('shipping', 'Pengiriman:') !!}
                <label class="checkbox-inline">
                    {!! Form::hidden('shipping', false) !!}
                    {!! Form::checkbox('shipping', '1', '0') !!} Yes
                </label>
            </div>

            <!-- Subtract Field -->
            <div class="form-group col-sm-4">
                {!! Form::label('subtract', 'Mengurangi:') !!}
                <label class="checkbox-inline">
                    {!! Form::hidden('subtract', false) !!}
                    {!! Form::checkbox('subtract', '1', '0') !!} Hide
                </label>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>