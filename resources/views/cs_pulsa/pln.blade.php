@extends('layouts.apps')

@section('content')
<div class="container">
@include('flash::message')
    <div class="row row-wrap">
        <div class="col-md-12">
            <div class="product-thumb">
                <form id="pulsa" method="POST" action="{!! url('/pulsa/buypln') !!}">
                    {{ csrf_field() }}
                    <div class="col-md-3">
                    <input class="form-control" type="text" name="idpel" id="idpel" placeholder="Nomor Meter / ID Pelanggan Anda">
                    </div>
                    <div class="col-md-3">
                    <input class="form-control" type="text" name="nohp" id="nohp" placeholder="Nomor HP Anda">
                    </div>
                    <div class="col-md-3">
                    <input class="form-control" type="text" name="kode" id="kode" placeholder="Kode Produk">
                    </div>
                    <div class="col-md-2">
                    <button type="submit" id="tombol" class="btn btn-block btn-sm btn-primary btn-squared add-to-cart">
                        Beli Sekarang
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <h3 class="text-size-20">Update Harga Pulsa Murah: {{date("d M Y")}}</h3>
    <br>

    <h2 class='text-center text-uppercase text-strong'>Token PLN / PLN Prabayar</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('PLN')->result == 'success')
        @foreach($portalpulsa->cekHarga('PLN')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <p class='text-size-14'>
    * Harga <a class="text-primary-hover" href="/pulsa">agen pulsa murah all operator</a> 
    dapat berubah sewaktu-waktu
    </p>
    <br>
    <blockquote>Tag: harga pulsa all operator, harga pulsa elektrik, daftar harga pulsa</blockquote>
    <br>
</div>
@endsection