@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Produk Kategori
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductToCategory, ['route' => ['ocProductToCategories.update', $ocProductToCategory->id], 'method' => 'patch']) !!}

                        @include('oc_product_to_categories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection