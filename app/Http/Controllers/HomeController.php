<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\oc_product;
use App\Models\users;
use App\Models\cs_shoppingcart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', ['product' => oc_product::all() ,'users' => users::all(),'shoppingcart' => cs_shoppingcart::all()]);
    }
}
