<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>celebestore</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('/stylemobile/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('/stylemobile/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('/stylemobile/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('/stylemobile/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('/stylemobile/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('/stylemobile/css/themes/all-themes.css') }}" rel="stylesheet" />

    <!-- Bootstrap Spinner Css -->
    <link href="{{ asset('/stylemobile/plugins/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet">

    <!-- Light Gallery Plugin Css -->
    <link href="{{ asset('/stylemobile/plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">

    <!-- Sweet Alert Css -->
    <link href="{{ asset('/stylemobile/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('/stylemobile/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />


</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <form id="pulsa" method="GET" action="{!! url('mobile/search/') !!}">
              <input type="text" name="cari" id="cari" placeholder="Apa yang anda butuhkan">
        </form>
        <!-- <input type="text" placeholder="START TYPING..."> -->
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    @include('mobile.navbar')
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ asset('/cs.png') }}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @auth
                    {{ Auth::user()->name }}
                    @else
                    Celebestore
                    @endauth
                    </div>
                    <div class="email">
                    @auth
                    {{ Auth::user()->email }}
                    @else
                    cs@celebestore.com
                    @endauth
                    </div>
                    <div class="btn-group user-helper-dropdown">
	                    @auth
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ url('/users/'.Auth::user()->id) }}"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/csShoppingcarts') }}"><i class="material-icons">shopping_cart</i>Pembelian</a></li>
                            <li><a href="{{ url('/csTransaksiusers') }}"><i class="material-icons">monetization_on</i>Deposite</a></li>
                            <li role="seperator" class="divider"></li>
                            <li>
                            <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Keluar
                            </a>
                            </li>
                        </ul>
                        @endauth
                    </div>
                </div>
            </div>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <!-- #User Info -->
            <!-- Menu -->
            @include('mobile.sidebar')
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        @include('mobile.sidebar2')
        <!-- #END# Right Sidebar -->
    </section>
    <!-- Content -->
	@yield('content')
    <!-- #END# Content -->

    <!-- Jquery Core Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('/stylemobile/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('/stylemobile/plugins/morrisjs/morris.js') }}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('/stylemobile/plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <!-- <script src="{{ asset('/stylemobile/plugins/flot-charts/jquery.flot.js') }}"></script> -->
    <!-- <script src="{{ asset('/stylemobile/plugins/flot-charts/jquery.flot.resize.js') }}"></script> -->
    <!-- <script src="{{ asset('/stylemobile/plugins/flot-charts/jquery.flot.pie.js') }}"></script> -->
    <!-- <script src="{{ asset('/stylemobile/plugins/flot-charts/jquery.flot.categories.js') }}"></script> -->
    <!-- <script src="{{ asset('/stylemobile/plugins/flot-charts/jquery.flot.time.js') }}"></script> -->

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('/stylemobile/js/admin.js') }}"></script>
    <!-- <script src="{{ asset('/stylemobile/js/pages/index.js') }}"></script> -->

    <!-- Jquery Spinner Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery-spinner/js/jquery.spinner.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('/stylemobile/js/demo.js') }}"></script>


    <!-- Light Gallery Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/light-gallery/js/lightgallery-all.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('/stylemobile/js/pages/medias/image-gallery.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('/stylemobile/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('/stylemobile/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ asset('/stylemobile/js/pages/forms/form-wizard.js') }}"></script>

</body>

</html>