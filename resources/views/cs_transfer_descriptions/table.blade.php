<table class="table table-responsive" id="csTransferDescriptions-table">
    <thead>
        <tr>
            <th>Description</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csTransferDescriptions as $csTransferDescription)
        <tr>
            <td>{!! $csTransferDescription->id !!}</td>
            <td>
                {!! Form::open(['route' => ['csTransferDescriptions.destroy', $csTransferDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('csTransferDescriptions.show', [$csTransferDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csTransferDescriptions.edit', [$csTransferDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>