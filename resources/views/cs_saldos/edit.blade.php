@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cs Saldo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($csSaldo, ['route' => ['csSaldos.update', $csSaldo->id], 'method' => 'patch']) !!}

                        @include('cs_saldos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection