<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_productRequest;
use App\Http\Requests\Updateoc_productRequest;
use App\Repositories\oc_productRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\oc_product;
use Auth;

class oc_productController extends AppBaseController
{
    /** @var  oc_productRepository */
    private $ocProductRepository;

    public function __construct(oc_productRepository $ocProductRepo)
    {
        $this->ocProductRepository = $ocProductRepo;
    }

    /**
     * Display a listing of the oc_product.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRepository->pushCriteria(new RequestCriteria($request));
        $ocProducts = $this->ocProductRepository->all();
        if (!Auth::user()->status) {
            $ocProducts = oc_product::where('user_id',Auth::user()->id)->get();
        }
        return view('oc_products.index')
            ->with('ocProducts', $ocProducts);
    }

    /**
     * Show the form for creating a new oc_product.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_products.create');
    }

    /**
     * Store a newly created oc_product in storage.
     *
     * @param Createoc_productRequest $request
     *
     * @return Response
     */
    public function store(Createoc_productRequest $request)
    {
        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/product/sampul/".$request->model."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['image'] = $fileName;

        $input['user_id'] = Auth::user()->id;

        $ocProduct = $this->ocProductRepository->create($input);

        Flash::success('Oc Product saved successfully.');

        return redirect(route('ocProducts.index'));
    }

    /**
     * Display the specified oc_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('ocProducts.index'));
        }

        return view('oc_products.show')->with('ocProduct', $ocProduct);
    }

    /**
     * Show the form for editing the specified oc_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('ocProducts.index'));
        }

        return view('oc_products.edit')->with('ocProduct', $ocProduct);
    }

    /**
     * Update the specified oc_product in storage.
     *
     * @param  int              $id
     * @param Updateoc_productRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_productRequest $request)
    {
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('ocProducts.index'));
        }

        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/product/sampul/".$request->model."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;
        
        $input['image'] = $fileName;

        $ocProduct = $this->ocProductRepository->update($input, $id);

        Flash::success('Oc Product updated successfully.');

        return redirect(route('ocProducts.index'));
    }

    /**
     * Remove the specified oc_product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('ocProducts.index'));
        }

        $this->ocProductRepository->delete($id);

        Flash::success('Oc Product deleted successfully.');

        return redirect(route('ocProducts.index'));
    }
}
