<li class="<?php echo e(Request::is('home') ? 'active' : ''); ?>"> <a href="<?php echo route('home'); ?>"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li> <?php 
$catalog = 
Request::is('ocCategories*') ? 'active' : 
Request::is('ocCategoryDescriptions*') ? 'active' : 
Request::is('ocProducts*') ? 'active' : 
Request::is('ocProductDescriptions*') ? 'active' : 
Request::is('ocManufacturers*') ? 'active' : 
Request::is('ocProductImages*') ? 'active' :
Request::is('ocProductToCategories*') ? 'active' :
Request::is('ocStockStatuses*') ? 'active' :
Request::is('ocWeightClasses*') ? 'active' :
Request::is('ocLengthClasses*') ? 'active' : '';
?> <li class="<?php echo e($catalog); ?> treeview menu-open"> <a href="#"> <i class="fa fa-tags"></i> <span>Katalog</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> <ul class="treeview-menu"> <?php echo $__env->make('layouts.menu.products.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('layouts.menu.categories.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <li class="<?php echo e(Request::is('ocManufacturers*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocManufacturers.index'); ?>"><span>Manufaktur</span></a> </li> <li class="<?php echo e(Request::is('ocStockStatuses*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocStockStatuses.index'); ?>"><span>Status Stok</span></a> </li> <li class="<?php echo e(Request::is('ocWeightClasses*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocWeightClasses.index'); ?>"><span>Kelas (Satuan) Berat</span></a> </li> <li class="<?php echo e(Request::is('ocLengthClasses*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocLengthClasses.index'); ?>"><span>Kelas (Satuan) Panjang </span></a> </li> </ul> </li> <?php 
$catalog = 
Request::is('ocLanguages*') ? 'active' : 
Request::is('ocBanners*') ? 'active' : 
Request::is('ocBannerImages*') ? 'active' : 
Request::is('ocBannerImageDescriptions*') ? 'active' : '';
?> <li class="<?php echo e($catalog); ?> treeview menu-open"> <a href="#"> <i class="fa fa-television"></i> <span>Desain</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> <ul class="treeview-menu"> <li class="<?php echo e(Request::is('ocLanguages*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocLanguages.index'); ?>"><i class="fa fa-language"></i><span>Bahasa</span></a> </li> <li class="<?php echo e(Request::is('ocBanners*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocBanners.index'); ?>"><i class="fa fa-th-large"></i><span>Spanduk Celebes</span></a> </li> <li class="<?php echo e(Request::is('ocBannerImages*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocBannerImages.index'); ?>"><i class="fa fa-file-image-o"></i><span>Gambar Spanduk</span></a> </li> <li class="<?php echo e(Request::is('ocBannerImageDescriptions*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocBannerImageDescriptions.index'); ?>"><i class="fa fa-qrcode"></i><span>Deskripsi Gambar Spanduk</span></a> </li> </ul> </li> <?php 
$catalog = 
Request::is('ocTaxClasses*') ? 'active' : '';
?> <li class="<?php echo e($catalog); ?> treeview menu-open"> <a href="#"> <i class="fa fa-share-alt"></i> <span>Pemasaran</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> <ul class="treeview-menu"> <li class="<?php echo e(Request::is('ocTaxClasses*') ? 'active' : ''); ?>"> <a href="<?php echo route('ocTaxClasses.index'); ?>"><i class="fa fa-money"></i><span>Pajak</span></a> </li> </ul> </li> <?php 
$catalog = 
Request::is('users*') ? 'active' : 
Request::is('csSaldos*') ? 'active' : 
Request::is('csOrders*') ? 'active' : 
Request::is('csTransaksis*') ? 'active' : '';
?> <li class="<?php echo e($catalog); ?> treeview menu-open"> <a href="#"> <i class="fa fa-users"></i> <span>Pelanggan</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> <ul class="treeview-menu"> <li class="<?php echo e(Request::is('users*') ? 'active' : ''); ?>"> <a href="<?php echo route('users.index'); ?>"><i class="fa fa-user-md"></i><span>Pelanggan</span></a> </li> <li class="<?php echo e(Request::is('csOrders*') ? 'active' : ''); ?>"> <a href="<?php echo route('csOrders.index'); ?>"><i class="fa fa-user-plus"></i><span>Pesanan Pelanggan</span></a> </li> <li class="<?php echo e(Request::is('csTransaksis*') ? 'active' : ''); ?>"> <a href="<?php echo route('csTransaksis.index'); ?>"><i class="fa fa-cart-arrow-down"></i><span>Transaksi</span></a> </li> <li class="<?php echo e(Request::is('csSaldos*') ? 'active' : ''); ?>"> <a href="<?php echo route('csSaldos.index'); ?>"><i class="fa fa-credit-card"></i><span>Saldo</span></a> </li> </ul> </li> <?php 
$catalog = 
Request::is('csShoppingcarts*') ? 'active' : 
Request::is('csTransferDescriptions*') ? 'active' : '';
?> <li class="<?php echo e($catalog); ?> treeview menu-open"> <a href="#"> <i class="fa fa-shopping-cart"></i> <span>Penjualan</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> <ul class="treeview-menu"> <li class="<?php echo e(Request::is('csShoppingcarts*') ? 'active' : ''); ?>"> <a href="<?php echo route('csShoppingcarts.index'); ?>"><i class="fa fa-shopping-cart"></i><span>Keranjang Belanja</span></a> </li> <li class="<?php echo e(Request::is('csTransferDescriptions*') ? 'active' : ''); ?>"> <a href="<?php echo route('csTransferDescriptions.index'); ?>"><i class="fa fa-table"></i><span>Deskripsi Rekening</span></a> </li> </ul> </li>