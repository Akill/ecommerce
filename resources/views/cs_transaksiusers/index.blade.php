@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Transaksi Pembelian</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('csTransaksiusers.create') !!}">Tambah</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="callout callout-info">
            <h4>Info Panduan Deposit</h4>
            <p>Segera lakukan transfer setelah melakukan request deposit. Request akan ditindak lanjuti apabila dalam 12 jam belum ada konfirmasi. Jangan request berulang-ulang jika masih ada request sebelumnya yang masih aktif.</p>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('cs_transaksiusers.table')
            </div>
        </div>
        <div class="callout callout-warning">
            <h4>Info Transfer</h4>
            <p>{!! App\Models\cs_transfer_description::first()['description'] !!}</p>
        </div>
    </div>
@endsection

