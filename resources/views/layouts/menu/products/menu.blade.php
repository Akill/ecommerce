@php 
$active = Request::is('ocProducts*') ? 'active' : Request::is('ocProductDescriptions*') ? 'active' : 
Request::is('ocProductImages*') ? 'active' : Request::is('ocProductToCategories*') ? 'active' : ''; 
@endphp

<li class="{{ $active }} treeview menu-open">
    <a href="#">
        <span>Produk</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocProducts*') ? 'active' : '' }}">
            <a href="{!! route('ocProducts.index') !!}"><i class="fa fa-cube"></i></i><span>Produk</span></a>
        </li>

        <li class="{{ Request::is('ocProductDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocProductDescriptions.index') !!}"><i class="fa fa-file-text"></i><span>Deskripsi Produk</span></a>
        </li>

        <li class="{{ Request::is('ocProductImages*') ? 'active' : '' }}">
            <a href="{!! route('ocProductImages.index') !!}"><i class="fa fa-file-image-o"></i><span>Gambar Produk</span></a>
        </li>

        <li class="{{ Request::is('ocProductToCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocProductToCategories.index') !!}"><i class="fa fa-suitcase"></i><span>Produk Kategori</span></a>
        </li>
    </ul>
</li>