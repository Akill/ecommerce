<?php

namespace App\Repositories;

use App\Models\oc_product_to_category;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_to_categoryRepository
 * @package App\Repositories
 * @version October 15, 2017, 12:04 am UTC
 *
 * @method oc_product_to_category findWithoutFail($id, $columns = ['*'])
 * @method oc_product_to_category find($id, $columns = ['*'])
 * @method oc_product_to_category first($columns = ['*'])
*/
class oc_product_to_categoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_to_category::class;
    }
}
