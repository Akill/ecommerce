@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Kelas Pajak</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('ocTaxClasses.create') !!}">Tambah</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('oc_tax_classes.table')
            </div>
        </div>
    </div>
@endsection

