<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_transaksiRequest;
use App\Http\Requests\Updatecs_transaksiRequest;
use App\Repositories\cs_transaksiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cs_transaksiController extends AppBaseController
{
    /** @var  cs_transaksiRepository */
    private $csTransaksiRepository;

    public function __construct(cs_transaksiRepository $csTransaksiRepo)
    {
        $this->csTransaksiRepository = $csTransaksiRepo;
    }

    /**
     * Display a listing of the cs_transaksi.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csTransaksiRepository->pushCriteria(new RequestCriteria($request));
        $csTransaksis = $this->csTransaksiRepository->all();

        return view('cs_transaksis.index')
            ->with('csTransaksis', $csTransaksis);
    }

    /**
     * Show the form for creating a new cs_transaksi.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_transaksis.create');
    }

    /**
     * Store a newly created cs_transaksi in storage.
     *
     * @param Createcs_transaksiRequest $request
     *
     * @return Response
     */
    public function store(Createcs_transaksiRequest $request)
    {
        $input = $request->all();

        $csTransaksi = $this->csTransaksiRepository->create($input);

        Flash::success('Cs Transaksi saved successfully.');

        return redirect(route('csTransaksis.index'));
    }

    /**
     * Display the specified cs_transaksi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csTransaksi = $this->csTransaksiRepository->findWithoutFail($id);

        if (empty($csTransaksi)) {
            Flash::error('Cs Transaksi not found');

            return redirect(route('csTransaksis.index'));
        }

        return view('cs_transaksis.show')->with('csTransaksi', $csTransaksi);
    }

    /**
     * Show the form for editing the specified cs_transaksi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csTransaksi = $this->csTransaksiRepository->findWithoutFail($id);

        if (empty($csTransaksi)) {
            Flash::error('Cs Transaksi not found');

            return redirect(route('csTransaksis.index'));
        }

        return view('cs_transaksis.edit')->with('csTransaksi', $csTransaksi);
    }

    /**
     * Update the specified cs_transaksi in storage.
     *
     * @param  int              $id
     * @param Updatecs_transaksiRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_transaksiRequest $request)
    {
        $csTransaksi = $this->csTransaksiRepository->findWithoutFail($id);

        if (empty($csTransaksi)) {
            Flash::error('Cs Transaksi not found');

            return redirect(route('csTransaksis.index'));
        }

        $csTransaksi = $this->csTransaksiRepository->update($request->all(), $id);

        Flash::success('Cs Transaksi updated successfully.');

        return redirect(route('csTransaksis.index'));
    }

    /**
     * Remove the specified cs_transaksi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csTransaksi = $this->csTransaksiRepository->findWithoutFail($id);

        if (empty($csTransaksi)) {
            Flash::error('Cs Transaksi not found');

            return redirect(route('csTransaksis.index'));
        }

        $this->csTransaksiRepository->delete($id);

        Flash::success('Cs Transaksi deleted successfully.');

        return redirect(route('csTransaksis.index'));
    }
}
