<!DOCTYPE HTML>
<html>
<head>
    <title>Celebestore</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- meta info -->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="celebestore" />
    <meta name="description" content="celebestore">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap styles -->
    <link rel="stylesheet" href="{{asset('css/boostrap.css')}}">
    <!-- Font Awesome styles (icons) -->
    <link rel="stylesheet" href="{{asset('css/font_awesome.css')}}">
    <!-- Main Template styles -->
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <!-- IE 8 Fallback -->
    <!--
    [if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="{{asset('css/ie.css')}}" />
    <![endif]
    -->

    <!-- Your custom styles (blank file) -->
    <link rel="stylesheet" href="{{asset('css/mystyles.css')}}">

    <link rel="stylesheet" href="{{asset('css/switcher.css')}}">
    <!-- Demo Examples -->
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/apple.css')}}" title="apple" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/pink.css')}}" title="pink" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/teal.css')}}" title="teal" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/gold.css')}}" title="gold" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/downy.css')}}" title="downy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/atlantis.css')}}" title="atlantis" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/red.css')}}" title="red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/violet.css')}}" title="violet" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/pomegranate.css')}}" title="pomegranate" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/violet-red.css')}}" title="violet-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/mexican-red.css')}}" title="mexican-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/victoria.css')}}" title="victoria" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/orient.css')}}" title="orient" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/jgger.css')}}" title="jgger" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/de-york.css')}}" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/blaze-orange.css')}}" title="blaze-orange" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('css/schemes/hot-pink.css')}}" title="hot-pink" media="all" />
    <!-- END Demo Examples -->
    
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    @yield('css')
</head>

<body class="bg-grey">


    <div class="global-wrap">


        @include('layouts.setting')


        <!-- 
        //////////////////////////////////////
	    //////////////MAIN HEADER///////////// 
	    //////////////////////////////////////
        -->

        @include('layouts.header')

        @yield('carousel')


        <!-- <div class="gap"></div> -->
        <!-- SEARCH AREA -->
        <header class="main" style="background:  #4997bd;">
            <div class="container">
                <div class="row">
        <form class="form-group" method="POST" action="{{url('/search')}}" style="margin-top: 15px;">
            {!! csrf_field() !!}
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <label><i class="fa fa-search"></i><span>Cari</span>
                        </label> -->
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"></i> <span>Cari</span></span>
                        <input id="search" type="text" class="form-control" name="search" placeholder="Semua kebutuhan">
                        </div><!-- 
                        <div class="search-area-division search-area-division-input">
                            <input class="form-control" id="search" name="search" type="text" placeholder="Semua kebutuhan" />
                        </div> -->
                    </div>
                    <div class="col-md-3">
                        <!-- <label><i class="fa fa-map-marker"></i><span>di</span>
                        </label> -->
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-map-marker"></i> <span>di</span></span>
                            @php $oke = json_decode(json_encode(session()->get('prov')), FALSE); @endphp
                            <select class="selectpicker form-control" id="citys" name="citys" data-show-subtext="true" data-live-search="true">
                            @foreach($oke[0] as $provinsi)
                            <optgroup label="{{$provinsi->province}}">
                                @php $citycu = json_decode(json_encode(session()->get('provinsi.'.$provinsi->province)), FALSE); @endphp
                                @foreach($citycu[0] as $kota)
                                <option value="{{$kota->city_name}}">{{$kota->city_name}}</option>
                                @endforeach
                            </optgroup>
                            @endforeach
                            </select>
                        </div>
                            <!-- <input class="form-control" type="text" name="citys" id="citys" placeholder="Kota" /> -->
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-block btn-white search-btn" type="submit">Cari</button>
                    </div>
                </div>
            </div>
        </form>
                </div>
            </div>
        </header>
        <!-- END SEARCH AREA -->

        <div class="gap"></div>

        <!-- 
        ///////////////////////////////////////
        //////////////END MAIN HEADER////////// 
        ///////////////////////////////////////
        -->


        <!-- 
        ///////////////////////////////////////
    	//////////////PAGE CONTENT///////////// 
    	///////////////////////////////////////
        -->


        @yield('content')


        <!-- 
        ///////////////////////////////////////
    	//////////////END PAGE CONTENT///////// 
    	///////////////////////////////////////
        -->

        <!-- 
        ///////////////////////////////////////
    	//////////////MAIN FOOTER////////////// 
    	///////////////////////////////////////
        -->

        @include('layouts.footer')
            
        <!-- 
        ///////////////////////////////////////
    	//////////////END MAIN  FOOTER///////// 
    	///////////////////////////////////////
        -->

        <!-- Scripts queries -->
        <script src="{{asset('js/jquery.js')}}"></script>
        <script src="{{asset('js/boostrap.min.js')}}"></script>
        <script src="{{asset('js/countdown.min.js')}}"></script>
        <script src="{{asset('js/flexnav.min.js')}}"></script>
        <script src="{{asset('js/magnific.js')}}"></script>
        <script src="{{asset('js/tweet.min.js')}}"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script> -->
        <script src="{{asset('js/fitvids.min.js')}}"></script>
        <script src="{{asset('js/mail.min.js')}}"></script>
        <script src="{{asset('js/ionrangeslider.js')}}"></script>
        <script src="{{asset('js/icheck.js')}}"></script>
        <script src="{{asset('js/fotorama.js')}}"></script>
        <script src="{{asset('js/card-payment.js')}}"></script>
        <script src="{{asset('js/owl-carousel.js')}}"></script>
        <script src="{{asset('js/masonry.js')}}"></script>
        <script src="{{asset('js/nicescroll.js')}}"></script>

        <!-- Custom scripts -->
        <script src="{{asset('js/custom.js')}}"></script>
        <script src="{{asset('js/switcher.js')}}"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
        @yield('js')
    </div>
</body>
</html>
