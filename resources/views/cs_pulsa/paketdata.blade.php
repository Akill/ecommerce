@extends('layouts.apps')

@section('content')
<div class="container">
@include('flash::message')
    <div class="row row-wrap">
        <div class="col-md-12">
            <div class="product-thumb">
                <form id="pulsa" method="POST" action="{!! url('/pulsa/buy') !!}">
                    {{ csrf_field() }}
                    <div class="col-md-4">
                    <input class="form-control" type="text" name="nohp" id="nohp" placeholder="Nomor HP Anda">
                    </div>
                    <div class="col-md-4">
                    <input class="form-control" type="text" name="kode" id="kode" placeholder="Kode Paket Data Operator">
                    </div>
                    <div class="col-md-2">
                    <button type="submit" id="tombol" class="btn btn-block btn-sm btn-primary btn-squared add-to-cart">
                        Beli Sekarang
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <h3 class="text-size-20">Update Harga Pulsa Murah: {{date("d M Y")}}</h3>
    <br>

    <h2 class='text-center text-uppercase text-strong'>AXIS INTERNET (AXD)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('AXD')->result == 'success')
        @foreach($portalpulsa->cekHarga('AXD')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>BOLT (BO)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('BO')->result == 'success')
        @foreach($portalpulsa->cekHarga('BO')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT INTERNET (IDN)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('IDN')->result == 'success')
        @foreach($portalpulsa->cekHarga('IDN')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT INTERNET EXTRA (IDX)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('IDX')->result == 'success')
        @foreach($portalpulsa->cekHarga('IDX')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT INTERNET FREEDOM (IFC)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('IFC')->result == 'success')
        @foreach($portalpulsa->cekHarga('IFC')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>SMARTFREN INTERNET (SMD)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('SMD')->result == 'success')
        @foreach($portalpulsa->cekHarga('SMD')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>SPEEDY (@WIFI ID) (SPN)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('SPN')->result == 'success')
        @foreach($portalpulsa->cekHarga('SPN')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>TELKOMSEL INTERNET (STG)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('STG')->result == 'success')
        @foreach($portalpulsa->cekHarga('STG')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>TRI INTERNET (TD)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('TD')->result == 'success')
        @foreach($portalpulsa->cekHarga('TD')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>XL INTERNET COMBO XTRA (XCX)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('XCX')->result == 'success')
        @foreach($portalpulsa->cekHarga('XCX')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>

    <h2 class='text-center text-uppercase text-strong'>XL INTERNET HOTROD (XH)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <th>Produk</th>
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($portalpulsa->cekHarga('XH')->result == 'success')
        @foreach($portalpulsa->cekHarga('XH')->message as $operator)
          <tr>
              <td>{{$operator->description}}</td>
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        @endif
        <tbody>
    </table>
    
    <p class='text-size-14'>
    * Harga <a class="text-primary-hover" href="/pulsa">agen pulsa murah all operator</a> 
    dapat berubah sewaktu-waktu
    </p>
    <br>
    <blockquote>Tag: harga pulsa all operator, harga pulsa elektrik, daftar harga pulsa</blockquote>
    <br>
</div>
@endsection