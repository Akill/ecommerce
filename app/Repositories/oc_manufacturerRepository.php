<?php

namespace App\Repositories;

use App\Models\oc_manufacturer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_manufacturerRepository
 * @package App\Repositories
 * @version October 14, 2017, 8:25 pm UTC
 *
 * @method oc_manufacturer findWithoutFail($id, $columns = ['*'])
 * @method oc_manufacturer find($id, $columns = ['*'])
 * @method oc_manufacturer first($columns = ['*'])
*/
class oc_manufacturerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'image',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_manufacturer::class;
    }
}
