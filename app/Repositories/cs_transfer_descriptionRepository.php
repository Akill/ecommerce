<?php

namespace App\Repositories;

use App\Models\cs_transfer_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_transfer_descriptionRepository
 * @package App\Repositories
 * @version October 30, 2017, 3:02 am UTC
 *
 * @method cs_transfer_description findWithoutFail($id, $columns = ['*'])
 * @method cs_transfer_description find($id, $columns = ['*'])
 * @method cs_transfer_description first($columns = ['*'])
*/
class cs_transfer_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_transfer_description::class;
    }
}
