<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cs_saldo
 * @package App\Models
 * @version October 28, 2017, 8:38 am UTC
 *
 * @property integer user_id
 * @property integer saldo
 * @property string akun
 */
class cs_saldo extends Model
{
    use SoftDeletes;

    public $table = 'cs_saldo';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'saldo',
        'akun'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'saldo' => 'integer',
        'akun' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
