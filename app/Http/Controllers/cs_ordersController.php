<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_ordersRequest;
use App\Http\Requests\Updatecs_ordersRequest;
use App\Repositories\cs_ordersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class cs_ordersController extends AppBaseController
{
    /** @var  cs_ordersRepository */
    private $csOrdersRepository;

    public function __construct(cs_ordersRepository $csOrdersRepo)
    {
        $this->csOrdersRepository = $csOrdersRepo;
    }

    /**
     * Display a listing of the cs_orders.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csOrdersRepository->pushCriteria(new RequestCriteria($request));
        $csOrders = $this->csOrdersRepository->all();

        return view('cs_orders.index')
            ->with('csOrders', $csOrders);
    }

    /**
     * Show the form for creating a new cs_orders.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_orders.create');
    }

    /**
     * Store a newly created cs_orders in storage.
     *
     * @param Createcs_ordersRequest $request
     *
     * @return Response
     */
    public function store(Createcs_ordersRequest $request)
    {
        $input = $request->all();

        $csOrders = $this->csOrdersRepository->create($input);

        Flash::success('Cs Orders saved successfully.');

        return redirect(route('csOrders.index'));
    }

    /**
     * Display the specified cs_orders.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csOrders = $this->csOrdersRepository->findWithoutFail($id);

        if (empty($csOrders)) {
            Flash::error('Cs Orders not found');

            return redirect(route('csOrders.index'));
        }

        return view('cs_orders.show')->with('csOrders', $csOrders);
    }

    /**
     * Show the form for editing the specified cs_orders.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csOrders = $this->csOrdersRepository->findWithoutFail($id);

        if (empty($csOrders)) {
            Flash::error('Cs Orders not found');

            return redirect(route('csOrders.index'));
        }

        return view('cs_orders.edit')->with('csOrders', $csOrders);
    }

    /**
     * Update the specified cs_orders in storage.
     *
     * @param  int              $id
     * @param Updatecs_ordersRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_ordersRequest $request)
    {
        $csOrders = $this->csOrdersRepository->findWithoutFail($id);

        if (empty($csOrders)) {
            Flash::error('Cs Orders not found');

            return redirect(route('csOrders.index'));
        }

        if ($request->file('resi_pengiriman')) {
            $file       = $request->file('resi_pengiriman');
            $fileName   = $file->getClientOriginalName();
            $request->file('resi_pengiriman')->move("image/orders/".Auth::user()->id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['resi_pengiriman'] = $fileName;

        $csOrders = $this->csOrdersRepository->update($input, $id);

        Flash::success('Cs Orders updated successfully.');

        return redirect(route('csOrders.index'));
    }

    /**
     * Remove the specified cs_orders from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csOrders = $this->csOrdersRepository->findWithoutFail($id);

        if (empty($csOrders)) {
            Flash::error('Cs Orders not found');

            return redirect(route('csOrders.index'));
        }

        $this->csOrdersRepository->delete($id);

        Flash::success('Cs Orders deleted successfully.');

        return redirect(route('csOrders.index'));
    }
}
