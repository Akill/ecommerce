<?php

namespace App\Repositories;

use App\Models\oc_tax_class;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_tax_classRepository
 * @package App\Repositories
 * @version October 14, 2017, 8:52 pm UTC
 *
 * @method oc_tax_class findWithoutFail($id, $columns = ['*'])
 * @method oc_tax_class find($id, $columns = ['*'])
 * @method oc_tax_class first($columns = ['*'])
*/
class oc_tax_classRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_tax_class::class;
    }
}
