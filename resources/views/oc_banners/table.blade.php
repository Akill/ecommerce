<table class="table table-responsive" id="ocBanners-table">
    <thead>
        <tr>
            <th>Nama</th>
        <th>Status</th>
            <th colspan="3">Aksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocBanners as $ocBanner)
        <tr>
            <td>{!! $ocBanner->name !!}</td>
            <td>{!! $ocBanner->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocBanners.destroy', $ocBanner->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocBanners.show', [$ocBanner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocBanners.edit', [$ocBanner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>