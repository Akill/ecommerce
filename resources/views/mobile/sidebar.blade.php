    <div class="menu">
        <ul class="list">
            <li class="header">MENU UTAMA</li>
            <li class="active">
                <a href="{{ url('/mobile') }}">
                    <i class="material-icons">home</i>
                    <span>Celebestore</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/mobile/pulsa') }}">
                    <i class="material-icons">phonelink_ring</i>
                    <span>Pulsa</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/mobile/pln') }}">
                    <i class="material-icons">important_devices</i>
                    <span>Listrik</span>
                </a>
            </li>
            <!-- <li>
                <a href="{{ url('#') }}">
                    <i class="material-icons">motorcycle</i>
                    <span>Grab & Gojek</span>
                </a>
            </li> -->
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">assignment</i>
                    <span>Kategori</span>
                </a>
                <ul class="ml-menu">
				    @foreach($ocCategories as $category)
						<li>
							<a href="{{ url('/mobile/'.$category->id) }}">
							{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
							</a>
						</li>
					@endforeach
                </ul>
            </li>
            @auth
            <li>
                <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="material-icons">person</i>
                    <span>Logout</span>
                </a>
            </li>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            @else
            <li>
                <a href="{{ url('/mobile/login') }}">
                    <i class="material-icons">person</i>
                    <span>Login</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/mobile/register') }}">
                    <i class="material-icons">assignment_ind</i>
                    <span>Register</span>
                </a>
            </li>
            @endauth
        </ul>
    </div>