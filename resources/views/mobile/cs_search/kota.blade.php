@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h4>Pencarian Berdasarkan Kota</h4>
            </div>
            <div class="row clearfix">
            @forelse($produk as $ProductToCategory)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;">
                            <div id="aniimated-thumbnialsx" class="list-unstyled row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
    								<a href="{{url('/mobile/'.App\Models\oc_product_to_category::where('product_id',$ProductToCategory->id)->first()['category_id'].'/'.$ProductToCategory->id)}}" data-sub-html="Demo Description">
    									<img class="img-responsive thumbnail" src="/image/product/sampul/{{ $ProductToCategory->model.'/'.$ProductToCategory->image }}" alt="" style="margin-bottom: 0px;">
    								</a>
                                </div>
    						</div>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h6>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->id)->first()['name'], 2,'')}}
                                <br>
                                <small>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->id)->first()['description'], 3,'')}}
                                </small>
                            </h6>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                Rp. {{number_format($ProductToCategory->price,2,',','.')}}
                            </h5>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Data<small>Tidak Ditemukan </small>
                            </h2>
                        </div>
                        <div class="body">
    						Silahkan hubungi admin
                        </div>
                    </div>
                </div>
    		@endforelse
            </div>
        </div>
    </section>
<!-- END CONTENT CONTAINER -->
@endsection
