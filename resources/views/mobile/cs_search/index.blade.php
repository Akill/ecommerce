@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
    <section class="content">
        <?php if (!isset($_GET['cari'])) { ?>
        <div class="container-fluid">
            <div class="block-header">
                <h4>Pencarian</h4>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                    <div class="body">
                      <form id="pulsa" method="GET" action="{!! url('mobile/search/') !!}">
                          <div class="form-group">
                            <div class="form-line">
                              <input class="form-control" type="text" name="cari" id="cari" placeholder="Apa yang anda butuhkan">
                            </div>
                          </div>
                          <div class="col-md-2">
                          <button type="submit" id="tombol" class="btn btn-block btn-sm btn-primary btn-squared add-to-cart">
                              Cari Sekarang
                          </button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if (isset($_GET['cari'])) { ?>
        <div class="container-fluid">
            <div class="block-header">
                <h4>Hasil Pencarian</h4>
            </div>
            <div class="row clearfix">
            @forelse($produk as $ProductToCategory)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;">
                            <div id="aniimated-thumbnialsx" class="list-unstyled row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
    								<a href="{{url('/mobile/'.App\Models\oc_product_to_category::where('product_id',$ProductToCategory->product_id)->first()['category_id'].'/'.$ProductToCategory->product_id)}}" data-sub-html="Demo Description">
    									<img class="img-responsive thumbnail" src="/image/product/sampul/{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image'] }}" alt="" style="margin-bottom: 0px;">
    								</a>
                                </div>
    						</div>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h6>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 2,'')}}
                                <br>
                                <small>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 3,'')}}
                                </small>
                            </h6>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}
                            </h5>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Data<small>Tidak Ditemukan </small>
                            </h2>
                        </div>
                        <div class="body">
    						Silahkan hubungi admin
                        </div>
                    </div>
                </div>
    		@endforelse
            </div>
        </div>
        <?php } ?>
    </section>
<!-- END CONTENT CONTAINER -->
@endsection
