<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_imageRequest;
use App\Http\Requests\Updateoc_product_imageRequest;
use App\Repositories\oc_product_imageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\oc_product_image;

class oc_product_imageController extends AppBaseController
{
    /** @var  oc_product_imageRepository */
    private $ocProductImageRepository;

    public function __construct(oc_product_imageRepository $ocProductImageRepo)
    {
        $this->ocProductImageRepository = $ocProductImageRepo;
    }

    /**
     * Display a listing of the oc_product_image.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductImageRepository->pushCriteria(new RequestCriteria($request));
        $ocProductImages = $this->ocProductImageRepository->all();
        if (!Auth::user()->status) {
            $ocProductImages = oc_product_image::where('user_id',Auth::user()->id)->get();
        }
        return view('oc_product_images.index')
            ->with('ocProductImages', $ocProductImages);
    }

    /**
     * Show the form for creating a new oc_product_image.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_images.create');
    }

    /**
     * Store a newly created oc_product_image in storage.
     *
     * @param Createoc_product_imageRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_imageRequest $request)
    {
        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/product/".$request->product_id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['image'] = $fileName;

        $input['user_id'] = Auth::user()->id;

        $ocProductImage = $this->ocProductImageRepository->create($input);

        Flash::success('Oc Product Image saved successfully.');

        return redirect(route('ocProductImages.index'));
    }

    /**
     * Display the specified oc_product_image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            Flash::error('Oc Product Image not found');

            return redirect(route('ocProductImages.index'));
        }

        return view('oc_product_images.show')->with('ocProductImage', $ocProductImage);
    }

    /**
     * Show the form for editing the specified oc_product_image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            Flash::error('Oc Product Image not found');

            return redirect(route('ocProductImages.index'));
        }

        return view('oc_product_images.edit')->with('ocProductImage', $ocProductImage);
    }

    /**
     * Update the specified oc_product_image in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_imageRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_imageRequest $request)
    {
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            Flash::error('Oc Product Image not found');

            return redirect(route('ocProductImages.index'));
        }

        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/product/".$request->product_id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['image'] = $fileName;

        $input['user_id'] = Auth::user()->id;

        $ocProductImage = $this->ocProductImageRepository->update($input, $id);

        Flash::success('Oc Product Image updated successfully.');

        return redirect(route('ocProductImages.index'));
    }

    /**
     * Remove the specified oc_product_image from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            Flash::error('Oc Product Image not found');

            return redirect(route('ocProductImages.index'));
        }

        $this->ocProductImageRepository->delete($id);

        Flash::success('Oc Product Image deleted successfully.');

        return redirect(route('ocProductImages.index'));
    }
}
