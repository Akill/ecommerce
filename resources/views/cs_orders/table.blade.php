<table class="table table-responsive" id="csOrders-table">
    <thead>
        <tr>
            <th>Produk</th>
            <th>User</th>
            <th class="hidden-xs hidden-sm">Paket</th>
            <th>No Resi</th>
            <th class="hidden-xs hidden-sm">Resi Pengiriman</th>
            <th class="hidden-xs hidden-sm">Verifikasi</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csOrders as $csOrders)
        <tr>
            <td>{!! $csOrders->id_product !!}</td>
            <td>{!! $csOrders->id_users !!}</td>
            @php
                $stat = json_decode($csOrders->paket, true);
            @endphp
            <td class="hidden-xs hidden-sm">{!! strtoupper($stat['paket']) !!} - {!! $stat['etd'] !!}</td>
            <td class="hidden-xs hidden-sm">{!! $csOrders->no_resi !!}</td>
            <td class="hidden-xs hidden-sm">
            @if(isset($csOrders->resi_pengiriman))
            <img src="{!! 'image/orders/'.$csOrders->id_users.'/'.$csOrders->resi_pengiriman !!}" alt="Image" width="75" height="75">
            @endif
            </td>
            <td>
            @if($csOrders->verification == 'Sukses')
            <span class="label label-success">{!! $csOrders->verification !!}</span>
            @elseif($csOrders->verification == 'Tunda')
            <span class="label label-warning">{!! $csOrders->verification !!}</span>
            @elseif($csOrders->verification == 'Batal')
            <span class="label label-danger">{!! $csOrders->verification !!}</span>
            @endif
            </td>
            <td>
                {!! Form::open(['route' => ['csOrders.destroy', $csOrders->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('csOrders.show', [$csOrders->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csOrders.edit', [$csOrders->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @if(Auth::user()->status)
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>