@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Bahasa
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocLanguage, ['route' => ['ocLanguages.update', $ocLanguage->id], 'method' => 'patch']) !!}

                        @include('oc_languages.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection