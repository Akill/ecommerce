-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 15 Feb 2018 pada 13.03
-- Versi Server: 10.0.33-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.27-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cs_orders`
--

CREATE TABLE `cs_orders` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `paket` varchar(255) NOT NULL,
  `no_resi` varchar(255) DEFAULT NULL,
  `resi_pengiriman` text,
  `verification` varchar(255) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cs_orders`
--

INSERT INTO `cs_orders` (`id`, `id_product`, `id_users`, `paket`, `no_resi`, `resi_pengiriman`, `verification`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, '{"value":13000,"etd":"3","note":"","paket":"tiki"}', '982349283125', 'dictionary-icon-8505.png', 'Sukses', '2018-02-13', '2018-02-14', NULL),
(2, 1, 2, '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, NULL, 'Tunda', '2018-02-13', '2018-02-13', NULL),
(3, 2, 2, '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, NULL, 'Batal', '2018-02-13', '2018-02-13', NULL),
(4, 1, 1, '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, NULL, 'Tunda', '2018-02-13', '2018-02-13', NULL),
(5, 2, 1, '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, NULL, 'Batal', '2018-02-13', '2018-02-13', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cs_saldo`
--

CREATE TABLE `cs_saldo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `saldo` int(11) NOT NULL,
  `akun` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cs_saldo`
--

INSERT INTO `cs_saldo` (`id`, `user_id`, `saldo`, `akun`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 21175, 'terverifikasi', '2017-10-29', '2017-10-31', NULL),
(2, 2, 786878, 'terverifikasi', '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cs_shoppingcart`
--

CREATE TABLE `cs_shoppingcart` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `districts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `paket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unpaid',
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cs_shoppingcart`
--

INSERT INTO `cs_shoppingcart` (`id`, `user_id`, `address`, `districts`, `city`, `province`, `zip_kode`, `bukti`, `content`, `paket`, `status`, `end_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 2, 'BTN Griya Blok A17 No.12', NULL, '275', '28', '90245', 'icon.png', '{"027c91341fd5cf4d2579b49c4b6a90da":{"rowId":"027c91341fd5cf4d2579b49c4b6a90da","id":"1","name":"Syari Fashion Wanita Pakaian Muslim Harga Grosir Terbaru","qty":"1","price":20000,"options":[],"tax":0,"subtotal":20000}}', 'tiki', '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, '2018-02-10 22:13:31', '2018-02-14 07:48:55', NULL),
(9, 2, 'kjhksdj', NULL, '360', '28', 'kjhk', 'student.png', '{"027c91341fd5cf4d2579b49c4b6a90da":{"rowId":"027c91341fd5cf4d2579b49c4b6a90da","id":"1","name":"Syari Fashion Wanita Pakaian Muslim Harga Grosir Terbaru","qty":"1","price":20000,"options":[],"tax":0,"subtotal":20000},"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'pos', '{"value":61000,"etd":"2-3 HARI","note":"","paket":"pos"}', NULL, '2018-02-10 23:55:36', '2018-02-14 07:49:05', NULL),
(10, 2, '74576', NULL, '336', '28', 'y876', NULL, '{"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'pos', '{"value":28500,"etd":"2-3 HARI","note":"","paket":"pos"}', NULL, '2018-02-12 05:54:35', '2018-02-12 05:54:35', NULL),
(11, 2, 'Jl. Kandea Komp. unhas No.6', NULL, '254', '28', '90245', NULL, '{"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'tiki', '{"value":13000,"etd":"1","note":"","paket":"tiki"}', NULL, '2018-02-13 02:58:31', '2018-02-13 02:58:31', NULL),
(12, 2, 'Jl. Kandea Komp. unhas No.6', NULL, '275', '28', '90245', NULL, '{"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'tiki', '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, '2018-02-13 03:28:30', '2018-02-13 03:28:30', NULL),
(13, 2, 'Jl. Kandea Komp. unhas No.6', NULL, '275', '28', '92201', NULL, '{"027c91341fd5cf4d2579b49c4b6a90da":{"rowId":"027c91341fd5cf4d2579b49c4b6a90da","id":"1","name":"Syari Fashion Wanita Pakaian Muslim Harga Grosir Terbaru","qty":"2","price":20000,"options":[],"tax":0,"subtotal":40000},"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'tiki', '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, '2018-02-13 03:29:44', '2018-02-13 03:29:44', NULL),
(14, 1, 'Sungguminasa', NULL, '132', '28', '90245', NULL, '{"027c91341fd5cf4d2579b49c4b6a90da":{"rowId":"027c91341fd5cf4d2579b49c4b6a90da","id":"1","name":"Syari Fashion Wanita Pakaian Muslim Harga Grosir Terbaru","qty":"1","price":20000,"options":[],"tax":0,"subtotal":20000},"370d08585360f5c568b18d1f2e4ca1df":{"rowId":"370d08585360f5c568b18d1f2e4ca1df","id":"2","name":"Mariana Dress Polka","qty":"1","price":85000,"options":[],"tax":0,"subtotal":85000}}', 'tiki', '{"value":13000,"etd":"3","note":"","paket":"tiki"}', NULL, '2018-02-13 04:03:51', '2018-02-13 04:03:51', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cs_transaksi`
--

CREATE TABLE `cs_transaksi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request` varchar(255) NOT NULL,
  `status` enum('pending','sukses','batal') NOT NULL DEFAULT 'pending',
  `confirm` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cs_transaksi`
--

INSERT INTO `cs_transaksi` (`id`, `user_id`, `request`, `status`, `confirm`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '786878', 'sukses', 'logo50x50.png', '2018-02-10', '2018-02-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cs_transfer_description`
--

CREATE TABLE `cs_transfer_description` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cs_transfer_description`
--

INSERT INTO `cs_transfer_description` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Silahkan melakukan transfer ke no rekening berikut ini : <br>\r\n- Atas Nama : Celebes Store <br>\r\n- No Rekening : 1982392873982 <br>\r\n- Bank : CELEBES <br>', '2018-02-10', '2018-02-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2017_10_15_060239_create_shoppingcart_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner`
--

CREATE TABLE `oc_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner`
--

INSERT INTO `oc_banner` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'DISKON', 1, '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`id`, `banner_id`, `link`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 'qwe.jpg', 1, '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner_image_description`
--

CREATE TABLE `oc_banner_image_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner_image_description`
--

INSERT INTO `oc_banner_image_description` (`id`, `language_id`, `banner_id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '20%', '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category`
--

CREATE TABLE `oc_category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category`
--

INSERT INTO `oc_category` (`id`, `user_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'qq.jpg', 0, 1, 1, 1, 1, '2018-01-10', '2018-01-10', NULL),
(2, 2, 'icon.png', 0, 1, 1, 1, 1, '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `oc_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category_description`
--

INSERT INTO `oc_category_description` (`id`, `user_id`, `oc_category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'Dress', 'Dress', 'Dress', 'Dress', 'Dress', '2018-01-10', '2018-01-10', NULL),
(2, 2, 2, 1, 'Baju', 'Baju Lengan Panjang Murah - Sweater Murah - Sweater Cewek Banana Modis Murah', 'Baju Lengan Panjang Murah', 'Baju Lengan Panjang Murah', 'Baju Lengan Panjang', '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_language`
--

CREATE TABLE `oc_language` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_language`
--

INSERT INTO `oc_language` (`id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Indonesia', 'id', 'id_ID', 'id.png', 'indonesia', 1, 1, '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_length_class`
--

INSERT INTO `oc_length_class` (`id`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mm', '2018-01-10', '2018-01-10', NULL),
(2, 'Cm', '2018-01-10', '2018-01-10', NULL),
(3, 'M', '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`id`, `name`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Havana', 'Havana.png', 1, '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product`
--

CREATE TABLE `oc_product` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `ean` varchar(14) DEFAULT NULL,
  `jan` varchar(13) DEFAULT NULL,
  `isbn` varchar(17) DEFAULT NULL,
  `mpn` varchar(64) DEFAULT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `points` int(8) DEFAULT NULL,
  `tax_class_id` int(11) NOT NULL,
  `date_available` date DEFAULT NULL,
  `weight` decimal(15,0) NOT NULL DEFAULT '0',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,0) NOT NULL DEFAULT '0',
  `width` decimal(15,0) NOT NULL DEFAULT '0',
  `height` decimal(15,0) NOT NULL DEFAULT '0',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) DEFAULT NULL,
  `minimum` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `viewed` int(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product`
--

INSERT INTO `oc_product` (`id`, `user_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Havana Dreds Bl Baju Murah Gamis', NULL, NULL, NULL, NULL, NULL, NULL, 'Makassar', 20, 1, '1.jpg', 1, NULL, 20000, NULL, 1, NULL, '460', 2, '2', '2', '2', 1, NULL, NULL, NULL, NULL, NULL, '2018-01-10', '2018-01-10', NULL),
(2, 2, 'Baju Muslim Wanita Mariana Dress Polka', NULL, NULL, NULL, NULL, NULL, NULL, 'Makassar', 1, 1, 'QWE.jpg', 1, NULL, 85000, NULL, 1, NULL, '702', 2, '2', '2', '2', 1, NULL, NULL, NULL, NULL, NULL, '2018-01-10', '2018-02-13', NULL),
(3, 2, 'Baju Wanita', NULL, NULL, NULL, NULL, NULL, NULL, 'Makassar', 10, 1, 'Screenshotoke.png', 1, NULL, 60000, NULL, 1, NULL, '700', 2, '70', '20', '5', 1, NULL, NULL, NULL, NULL, NULL, '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_description`
--

INSERT INTO `oc_product_description` (`id`, `user_id`, `product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'Syari Fashion Wanita Pakaian Muslim Harga Grosir Terbaru', 'material: balotelli\r\n\r\nukuran all size fit L \r\n\r\ntingkat kemiripan warna 85-95% \r\nkemiripan model baju gambar dan real 95-100% \r\n\r\norder masukan pilihan warna dan cadangan warna apabila tidak ada cadangan warna kami kirim random.\r\nKhusus dropship masukan nama pengirim. \r\nalamat selengkap mungkin. \r\n\r\nhappy shopping', 'Havana Dreds Bl Baju Murah', 'Havana Dreds Bl Baju Murah', 'Havana Dreds Bl Baju Murah', 'Havana Dreds Bl Baju Murah', '2018-01-10', '2018-01-10', NULL),
(2, 2, 2, 1, 'Mariana Dress Polka', 'REAL PIC BY ALIFIA STORE\r\n\r\nKODE : WD\r\nUKURAN : ALL SIZE L\r\nBAHAN : BALOTELI\r\n\r\nHARAP DIBACA SEBELUM ORDER :\r\nDalam pemesanan harap disediakan warna cadangan, Bila tidak akan kami kirimkan stock yang tersedia, Kecuali ada penulisan Kosong = Cancel,misal ( Pesan Warna Biru, Kosong = Cancel),bila warna biru tidak tersedia maka akan kami cancelkan\r\n\r\n--NB: Harap mencatumkan pilihan pesanan dan warna diketerangan pemesanan--\r\n\r\n++MAU PESAN BANYAK DENGAN HARGA GROSIRAN? LANGSUNG CONTACT KITA YAH HARGA BISA DI SESUAIKAN++\r\n\r\n!! Harap mencatumkan pilihan pesanan dan warna diketerangan pemesanan!!', 'Mariana Dress Polka', 'Mariana Dress Polka', 'Mariana Dress Polka', 'Mariana Dress Polka', '2018-01-10', '2018-01-10', NULL),
(3, 2, 3, 1, 'Baju Lengan Panjang Murah - Sweater Murah - Sweater Cewek Banana Modis Murah', 'PROMO GILA !!!\r\n\r\nBahan babyterri premium, tebal, tidak tembus, tidak gerah, adem. sangat-sangat nyaman dipakai. Dijamin bagus banget ditambah motifnya yang cute pasti suka. WAJIB COBAIN!\r\n\r\nVery recomended !!! \r\n\r\nSTOK SUPER LIMITED, Jangan sampai kehabisan.\r\n\r\nUkuran allsize, bisa digunakan buat kamu yang ukuran S,M,L\r\n\r\nJangan sampai salah beli ya dear, foto sama tapi bahan dan kualitas berbeda. Jangan sampai salah pilih. pembeli cerdas pilih yang berkualitas :)\r\n\r\nMau cantik tapi ga mahal hanya di gloryfame shop tempat grosir baju termurah :D', 'Baju Lengan Panjang Murah, Sweater Murah, Sweater Cewek Banana Modis Murah', 'Baju Lengan Panjang Murah', 'Baju Lengan Panjang Murah', 'Baju Lengan Panjang', '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_image`
--

INSERT INTO `oc_product_image` (`id`, `user_id`, `product_id`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, 'Baju_wanita___Baju_lengan_panjang_murah___sweater_murah___Sw.jpg.png', 1, '2018-02-14', '2018-02-14', NULL),
(2, 2, 2, 'olshopwulandary___Bbnv0IUnNqg____scaled.jpg', 1, '2018-02-14', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`id`, `user_id`, `product_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2018-01-10', '2018-01-10', NULL),
(2, 1, 1, 1, '2018-01-10', '2018-01-10', '2018-01-10'),
(3, 2, 2, 2, '2018-01-10', '2018-02-14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'In Stock', '2018-01-10', '2018-01-10', NULL),
(2, 1, 'Pre-Order', '2018-01-10', '2018-01-10', NULL),
(3, 1, 'Out Of Stock', '2018-01-10', '2018-01-10', NULL),
(4, 1, '2-3 Days', '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'GROSIR', 'Beli banyak lebih murah', '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`id`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kg', '2018-01-10', '2018-01-10', NULL),
(2, 'Gram', '2018-01-10', '2018-01-10', NULL),
(3, 'Ons', '2018-01-10', '2018-01-10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_identity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_number` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `districts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `access`, `username`, `type_of_identity`, `identity_number`, `gender`, `phone`, `job`, `address`, `districts`, `city`, `province`, `zip_kode`, `picture`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'akil', 'akil@admin.com', '$2y$10$2g5ACTSRpXU0DCprXiVxlewsBibf7vvAbbBWwJW1uv9BckkcJ9zA6', 'admin', 'akilsagitarius', 'KTP', '000117121991', 'Laki-laki', '082271111545', 'Programer', 'BTN GEMIT BLOK G NO.5', 'Pattallassang', 'Takalar', 'Sulawesi Selatan', '90245', 'Akil.jpg', 'PIwsiypbY7J38ybm5XXd8ot9lzpdXN7pzhq253ASY6AY82Y1oHYL19VZNrLq', '2017-10-14 14:00:28', '2017-10-17 18:12:40', NULL),
(2, 'Chairman', 'chairman@gmail.com', '$2y$10$eb1969VOwjKjuN/OFoNFFeTZ8OHuNKAIbvzOSqiaqJlirgawfwLAC', 'user', 'Chairman', 'KTP', '18739817398120019', 'Laki-laki', '81739817', 'Chairman Afrel', 'ajksdbkasj', 'kjsdfnksdj', 'kjnksjn', 'kjnkjnkj', 'nkjnknj', 'kjnkjkjn', 'QjxjxWRQUQKorZg9jlYwVg5dhgTwnUOqNEKVqstd8VuDHvv7KApzk9fdZPuz', '2017-10-15 02:28:25', '2018-02-14 03:05:29', NULL),
(3, 'celebestore', 'celebesku@gmail.com', '$2y$10$bL1JUW/jNTmUITZmA2587OGdVGgKwy60YAys2cdo18gflxA9Rf44S', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'UPKiF1aaVHVr9piP8IKBEAt9cdHo6NQEGih3OQdoOkKZBZQl8kkMXAwiedEW', '2017-10-29 18:53:42', '2017-10-29 18:53:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cs_orders`
--
ALTER TABLE `cs_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_saldo`
--
ALTER TABLE `cs_saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_shoppingcart`
--
ALTER TABLE `cs_shoppingcart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_transaksi`
--
ALTER TABLE `cs_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_transfer_description`
--
ALTER TABLE `cs_transfer_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_banner_image_description`
--
ALTER TABLE `oc_banner_image_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cs_orders`
--
ALTER TABLE `cs_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cs_saldo`
--
ALTER TABLE `cs_saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cs_shoppingcart`
--
ALTER TABLE `cs_shoppingcart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `cs_transaksi`
--
ALTER TABLE `cs_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cs_transfer_description`
--
ALTER TABLE `cs_transfer_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_banner_image_description`
--
ALTER TABLE `oc_banner_image_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
