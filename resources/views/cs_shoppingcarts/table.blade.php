<table class="table table-responsive" id="csShoppingcarts-table">
    <thead>
        <tr>
        @if(Auth::user()->status)
        <th>User</th>
        @endif
        <th class="hidden-xs hidden-sm">Bukti</th>
        <th class="hidden-lg hidden-md">Bukti</th>
        <th>Paket</th>
        <th>Status <div class="hidden-xs hidden-sm">Pengiriman</div></th>
        <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csShoppingcarts as $csShoppingcart)
        <tr>
            @if(Auth::user()->status)
            <td>{!! App\Models\users::where('id',$csShoppingcart->user_id)->first()['name'] !!}</td>
            @endif
            <td class="hidden-xs hidden-sm">
            @if(isset($csShoppingcart->bukti))
            <img src="{!! 'image/shoppingcart/'.$csShoppingcart->user_id.'/'.$csShoppingcart->bukti !!}" alt="Image" width="70" height="70">
            @endif
            </td>
            <td class="hidden-lg hidden-md">
            @if(isset($csShoppingcart->bukti))
            <img src="{!! 'image/shoppingcart/'.$csShoppingcart->user_id.'/'.$csShoppingcart->bukti !!}" alt="Image" width="20" height="20">
            @endif
            </td>
            @php
                $stat = json_decode($csShoppingcart->status, true);
            @endphp
            <td>{!! strtoupper($csShoppingcart->paket) !!}</td>
            <td>{!! $stat['etd'] !!}</td>
            <td>
                {!! Form::open(['route' => ['csShoppingcarts.destroy', $csShoppingcart->id], 'method' => 'delete']) !!}
                @if(Auth::user()->status)
                <div class='btn-group'>
                    <a href="{!! route('csShoppingcarts.show', [$csShoppingcart->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csShoppingcarts.edit', [$csShoppingcart->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apa anda yakin?')"]) !!}
                </div>
                @else
                <div class='btn-group'>
                    <a href="{!! route('cs.invoice', [$csShoppingcart->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csShoppingcarts.edit', [$csShoppingcart->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                </div>
                @endif
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>