@extends('layouts.apps')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div id="review-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
                        <h3>Tambah Komentar</h3>
                        <form>
                            <div class="form-group">
                                <label>Komentar</label>
                                <textarea class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kualitas</label>
                                <ul class="icon-list icon-list-inline star-rating" id="star-rating">
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                </ul>
                            </div>
                            <input type="submit" class="btn btn-primary" value="Submit" />
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="fotorama" data-nav="thumbs" data-allowfullscreen="1" data-thumbheight="150" data-thumbwidth="150">
                            @foreach($ocProductImage as $image)
                                <img src="{{url('image/product/'.$image->product_id.'/'.$image->image)}}" alt="Image Alternative text" title="Gamer Chick" />
                            @endforeach
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="product-info box">
                                <ul class="icon-group icon-list-rating text-color" title="4.5/5 rating">
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star-half-empty"></i>
                                    </li>
                                </ul>   <small><a href="#" class="text-muted">12 Komentar</a></small>
                                <h3>{{$ocProductDescription->name}}</h3>
                                <p class="product-info-price">Rp. {{number_format($ocProduct->price,2,',','.')}}</p>
                                <p class="text-smaller text-muted">{{$ocProductDescription->description}}</p>
                                <form id="idForm{{ $ocProduct->id }}" method="POST" action="{!! url('/cart/add') !!}">
                                    <ul class="list-inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $ocProduct->id }}">
                                        <input type="hidden" name="name" value="{{ App\Models\oc_product_description::where('product_id',$ocProduct->id)->first()['name'] }}">
                                        <input type="hidden" name="price" value="{{ App\Models\oc_product::where('id',$ocProduct->id)->first()['price'] }}">
                                        <input type="hidden" name="qty" value="1">
                                        <li>
                                            <button type="submit" id="tombol{{ $ocProduct->id }}" class="btn btn-primary btn-squared add-to-cart">
                                            <i class="fa fa-shopping-cart"></i> Tambah
                                            </button>
                                        </li>
                                        <li>
                                        <a href="#" class="btn btn-squared"><i class="fa fa-star"></i> Daftar Belanja</a>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="gap"></div>
                    <div class="tabbable">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-pencil"></i>Deskripsi</a>
                            </li>
                            <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-info"></i>Spesifikasi</a>
                            </li>
                            <li><a href="#tab-3" data-toggle="tab"><i class="fa fa-comments"></i>Komentar</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-1">
                                <p>{{$ocProductDescription->description}}</p>
                            </div>
                            <div class="tab-pane fade" id="tab-2">
                                <table class="table table-striped mb0">
                                    <tbody>
                                        <tr>
                                            <td>Weight</td>
                                            <td>{{$ocProduct->weight}} {{App\Models\oc_weight_class::find($ocProduct->weight_class_id)['value']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Dimentions</td>
                                            <td>{{$ocProduct->length}} x {{$ocProduct->width}} x {{$ocProduct->height}} cm</td>
                                        </tr>
                                        <tr>
                                            <td>Location</td>
                                            <td>{{$ocProduct->location}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="tab-3">
                                <ul class="comments-list">
                                    <li>
                                        <!-- REVIEW -->
                                        <article class="comment">
                                            <div class="comment-author">
                                                <img src="{{asset('img/gamer_chick_50x50.jpg')}}" alt="Image Alternative text" title="Gamer Chick" />
                                            </div>
                                            <div class="comment-inner">
                                                <ul class="icon-group icon-list-rating comment-review-rate" title="4/5 rating">
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star-o"></i>
                                                    </li>
                                                </ul><span class="comment-author-name">Alison Mackenzie</span>
                                                <p class="comment-content">Potenti diam ridiculus enim per orci aliquet quam proin sit neque porta conubia nam iaculis phasellus nam dignissim tincidunt sapien eros nam bibendum nunc vehicula accumsan lacus</p>
                                            </div>
                                        </article>
                                    </li>
                                    <li>
                                        <!-- REVIEW -->
                                        <article class="comment">
                                            <div class="comment-author">
                                                <img src="{{asset('img/ana_29_50x50.jpg')}}" alt="Image Alternative text" title="Ana 29" />
                                            </div>
                                            <div class="comment-inner">
                                                <ul class="icon-group icon-list-rating comment-review-rate" title="5/5 rating">
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                    <li><i class="fa fa-star"></i>
                                                    </li>
                                                </ul><span class="comment-author-name">Olivia Slater</span>
                                                <p class="comment-content">Nisl justo natoque pharetra adipiscing ultricies aliquam erat in condimentum hendrerit vulputate lacus fames aliquet volutpat habitasse himenaeos adipiscing sociosqu tincidunt</p>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                                <a class="popup-text btn btn-primary" href="#review-dialog" data-effect="mfp-zoom-out">
                                    <i class="fa fa-pencil"></i> Tambah Komentar
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gap"></div>
                    <h3>Produk yang sama</h3>
                    <div class="gap gap-mini"></div>
                    <div class="row row-wrap">
                        @forelse($ocProductToCategory as $ProductToCategory)
                        <div class="col-md-4">
                            <div class="product-thumb">
                                <header class="product-header">
                                    <a href="{{url($ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
                                    <img src="{{ asset('image/product/sampul/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image']) }}" alt="Image Alternative text" height="260" title="Green Furniture" />
                                    </a>
                                </header>
                                <div class="product-inner">
                                    <h5 class="product-title">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 3,'....')}}</h5>
                                    <p class="product-desciption">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 10,'....')}}</p>
                                    <div class="product-meta">
                                        <ul class="product-price-list">
                                            <li>
                                            <span class="product-price">Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}
                                            </span>
                                            </li>
                                        </ul>
                                        <form id="idFormcat{{ $ProductToCategory->product_id }}" method="POST" action="{!! url('/cart/add') !!}">
                                        {{ csrf_field() }}
                                        <ul class="product-actions-list">
                                                <input type="hidden" name="id" value="{{ $ProductToCategory->product_id }}">
                                                <input type="hidden" name="name" value="{{ App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'] }}">
                                                <input type="hidden" name="price" value="{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'] }}">
                                                <input type="hidden" name="qty" value="1">
                                                <li>
                                                <!-- <a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a> -->
                                                    <button type="submit" id="tombolcat{{ $ProductToCategory->product_id }}" class="btn btn-sm btn-primary btn-squared add-to-cart">
                                                    <i class="fa fa-shopping-cart"></i> Tambah
                                                    </button>
                                                </li>
                                                <li><a href="{{url($ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}" class="btn btn-sm btn-success btn-squared add-to-cart"><i class="fa fa-bars"></i> Detail</a>
                                                </li>
                                        </ul>
                                        </form>
                                    </div>
                                    <p class="product-location"><i class="fa fa-map-marker"></i> {{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['location'] }}</p>
                                </div>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                    <div class="gap gap-small"></div>
                </div>
                <div class="col-md-3">
                    <aside class="sidebar-right">
                        <div class="sidebar-box">
                            <h5>Tampil terakhir</h5>
                            <ul class="thumb-list">
                                @foreach(array_slice($session, count($session)-5, count($session)) as $se)
                                <li>
                                    <a href="#">
                                        <img src="{{ asset('image/product/sampul/'.App\Models\oc_product::where('id',$se)->first()['model'].'/'.App\Models\oc_product::where('id',$se)->first()['image']) }}" alt="Image Alternative text" title="Urbex Esch/Lux with Laney and Laaaaag" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <h5 class="thumb-list-item-title"><a href="#">{!! App\Models\oc_product_description::where('product_id',$se)->first()['name'] !!}</a></h5>
                                        <p class="thumb-list-item-price">Rp. {{number_format(App\Models\oc_product::where('id',$se)->first()['price'],2,',','.')}}</p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="sidebar-box">
                            <h5>Produk Kategori</h5>
                            <ul class="thumb-list">
                                @forelse($ocCategory as $category)
                                    @if($category->parent_id==0)
                                    <li>
                                        <a href="{{ url('/'.$category->id) }}">
                                            <img src="{!! asset('image/category/catalog/'.$category->image) !!}" alt="Image Alternative text" title="The Hidden Power of the Heart" />
                                        </a>
                                        <h5 class="thumb-list-item-title">
                                            <a href="{{ url('/'.$category->id) }}">
                                                {!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
                                            </a>
                                        </h5>
                                    </li>
                                    @endif
                                @empty
                                    <li>
                                        <a href="#">
                                            Category Tidak Ada
                                        </a>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>

        </div>
@endsection