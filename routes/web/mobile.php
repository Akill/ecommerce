<?php
Route::group(['prefix' => '/mobile'], function() {
	Auth::routesMobile();
	
	Route::get('/', 'cs_homeController@mobile')->name('cs.mobile.home');
	Route::get('/search', 'cs_searchController@getSearchm')->name('cs.mobile.getsearch');
	Route::get('/search/kota/{nama}', 'cs_searchController@searchkota')->name('cs.mobile.searchkota');

	// Route::post('/search', 'cs_searchController@searchm')->name('cs.mobile.search');
	// Route::get('/search/{id}', 'cs_searchController@searchdetail')->name('cs.mobile.searchdetail');
	
	Route::get('/pulsa', 'cs_pulsaController@mobile')->name('cs.mobile.pulsa');
	Route::get('/gojekgrab', 'cs_pulsaController@gojekgrab')->name('cs.mobile.gojekgrab');
	Route::get('/paketdata', 'cs_pulsaController@paketdata')->name('cs.mobile.paketdata');
	Route::get('/pln', 'cs_pulsaController@mobilepln')->name('cs.mobile.pln');

	Route::group(['prefix' => '/cart'], function() {
		Route::get('/', 'cs_cartController@cartm')->name('cs.mobile.cart');
		Route::post('/add', 'cs_cartController@addm')->name('cs.mobile.add');
		Route::get('/clear', 'cs_cartController@clearm')->name('cs.mobile.clear');
		Route::post('/remove/{id}', 'cs_cartController@removem')->name('cs.mobile.remove');
		Route::post('/update/{id}', 'cs_cartController@updatem')->name('cs.mobile.update');
	});

	Route::get('/home', 'cs_homeController@mobile')->name('mobile.home');

	Route::get('/{category}', 'cs_categoryController@mobile')->name('cs.mobile.category');
	Route::get('/{category}/{product}', 'cs_productController@mobile')->name('cs.mobile.product');

	Route::group(['middleware' => 'access'], function()
	{
		Route::resource('ocStockStatuses', 'oc_stock_statusController');

		Route::resource('ocManufacturers', 'oc_manufacturerController');

		Route::resource('ocTaxClasses', 'oc_tax_classController');

		Route::resource('ocWeightClasses', 'oc_weight_classController');

		Route::resource('ocLengthClasses', 'oc_length_classController');

		Route::resource('ocTaxClasses', 'oc_tax_classController');

		Route::resource('ocLanguages', 'oc_languageController');
		
		Route::resource('ocBanners', 'oc_bannerController');

		Route::resource('ocBannerImages', 'oc_banner_imageController');

		Route::resource('ocBannerImageDescriptions', 'oc_banner_image_descriptionController');

		Route::resource('csTransaksis', 'cs_transaksiController');

		Route::resource('csSaldos', 'cs_saldoController');
		
		Route::resource('csTransferDescriptions', 'cs_transfer_descriptionController');

		Route::resource('ocCategories', 'oc_categoryController');

		Route::resource('ocCategoryDescriptions', 'oc_category_descriptionController');

	});
	
	Route::group(['middleware' => 'mobile'], function(){
		Route::post('/checkout', 'cs_checkoutController@create')->name('cs.mobile.checkout_create');
		Route::post('/pulsa/buy', 'cs_pulsaController@buyMobile')->name('cs.mobile.pulsa.buy');
	});

		
	Route::group(['middleware' => 'auth'], function()
	{	
		Route::resource('users', 'usersController');
		
		Route::resource('csShoppingcarts', 'cs_shoppingcartController');

		Route::resource('ocProducts', 'oc_productController');

		Route::resource('ocProductDescriptions', 'oc_product_descriptionController');

		Route::resource('ocProductImages', 'oc_product_imageController');

		Route::resource('ocProductToCategories', 'oc_product_to_categoryController');
		
		Route::resource('csTransaksiusers', 'cs_transaksiuserController');

		Route::resource('csOrders', 'cs_ordersController');
		
		Route::post('/checkout/cost', 'cs_checkoutController@cost')->name('cs.mobile.cost');

		Route::post('/pulsa/buypln', 'cs_pulsaController@buyplnMobile')->name('cs.mobile.pulsa.buypln');

		Route::post('/checkout/buy', 'cs_checkoutController@store')->name('cs.mobile.checkout_strore');

		Route::post('/checkout/city', 'cs_checkoutController@city')->name('cs.mobile.city');

		Route::post('/checkout/kurir', 'cs_checkoutController@kurir')->name('cs.mobile.kurir');

		Route::get('/invoice/{id}', 'cs_shoppingcartController@invoice')->name('cs.mobile.invoice');

	});

});
