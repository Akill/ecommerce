<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProductToCategory->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Produk:') !!}
    <p>{!! $ocProductToCategory->product_id !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Kategori:') !!}
    <p>{!! $ocProductToCategory->category_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Tanggal Pembuatan:') !!}
    <p>{!! $ocProductToCategory->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Tanggal Pembaruan:') !!}
    <p>{!! $ocProductToCategory->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Tanggal Penghapusan:') !!}
    <p>{!! $ocProductToCategory->deleted_at !!}</p>
</div>

