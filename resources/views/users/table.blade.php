<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
        <th>Name</th>
        <th>Email</th>
        <!-- <th>Password</th> -->
        <th>Access</th>
        <th>Username</th>
        <!-- <th>Type Of Identity</th> -->
        <!-- <th>Identity Number</th> -->
        <th>Gender</th>
        <!-- <th>Phone</th> -->
        <!-- <th>Job</th> -->
        <!-- <th>Address</th> -->
        <!-- <th>Districts</th> -->
        <!-- <th>City</th> -->
        <!-- <th>Province</th> -->
        <!-- <th>Zip Kode</th> -->
        <!-- <th>Picture</th> -->
        <!-- <th>Remember Token</th> -->
        <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $users)
        <tr>
            <td>{!! $users->name !!}</td>
            <td>{!! $users->email !!}</td>
            <!-- <td>{!! $users->password !!}</td> -->
            <td>{!! $users->access !!}</td>
            <td>{!! $users->username !!}</td>
            <!-- <td>{!! $users->type_of_identity !!}</td> -->
            <!-- <td>{!! $users->identity_number !!}</td> -->
            <td>{!! $users->gender !!}</td>
            <!-- <td>{!! $users->phone !!}</td> -->
            <!-- <td>{!! $users->job !!}</td> -->
            <!-- <td>{!! $users->address !!}</td> -->
            <!-- <td>{!! $users->districts !!}</td> -->
            <!-- <td>{!! $users->city !!}</td> -->
            <!-- <td>{!! $users->province !!}</td> -->
            <!-- <td>{!! $users->zip_kode !!}</td> -->
            <!-- <td>{!! $users->picture !!}</td> -->
            <!-- <td>{!! $users->remember_token !!}</td> -->
            <td>
                {!! Form::open(['route' => ['users.destroy', $users->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>