<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/saya', function () {
//   return "Selamat Datang ".Auth::user()->name;
// })->middleware('access');


Auth::routes();

Route::get('/', 'cs_homeController@index')->name('cs.home');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'access'], function()
{
	Route::resource('ocStockStatuses', 'oc_stock_statusController');

	Route::resource('ocManufacturers', 'oc_manufacturerController');

	Route::resource('ocTaxClasses', 'oc_tax_classController');

	Route::resource('ocWeightClasses', 'oc_weight_classController');

	Route::resource('ocLengthClasses', 'oc_length_classController');

	Route::resource('ocTaxClasses', 'oc_tax_classController');

	Route::resource('ocLanguages', 'oc_languageController');
	
	Route::resource('ocBanners', 'oc_bannerController');

	Route::resource('ocBannerImages', 'oc_banner_imageController');

	Route::resource('ocBannerImageDescriptions', 'oc_banner_image_descriptionController');

	Route::resource('csTransaksis', 'cs_transaksiController');

	Route::resource('csSaldos', 'cs_saldoController');
	
	Route::resource('csTransferDescriptions', 'cs_transfer_descriptionController');

	Route::resource('ocCategories', 'oc_categoryController');

	Route::resource('ocCategoryDescriptions', 'oc_category_descriptionController');

});

Route::group(['middleware' => 'auth'], function()
{
	Route::resource('users', 'usersController');
	
	Route::resource('csShoppingcarts', 'cs_shoppingcartController');

	Route::resource('ocProducts', 'oc_productController');

	Route::resource('ocProductDescriptions', 'oc_product_descriptionController');

	Route::resource('ocProductImages', 'oc_product_imageController');

	Route::resource('ocProductToCategories', 'oc_product_to_categoryController');
	
	Route::resource('csTransaksiusers', 'cs_transaksiuserController');

	Route::resource('csOrders', 'cs_ordersController');
	
	Route::post('/checkout', 'cs_checkoutController@create')->name('cs.checkout_create');

	Route::post('/checkout/cost', 'cs_checkoutController@cost')->name('cs.cost');

	Route::post('/pulsa/buy', 'cs_pulsaController@buy')->name('cs.pulsa.buy');

	Route::post('/pulsa/buypln', 'cs_pulsaController@buypln')->name('cs.pulsa.buypln');

	Route::post('/checkout/buy', 'cs_checkoutController@store')->name('cs.checkout_strore');

	Route::post('/checkout/city', 'cs_checkoutController@city')->name('cs.city');

	Route::post('/checkout/kurir', 'cs_checkoutController@kurir')->name('cs.kurir');

	Route::get('/invoice/{id}', 'cs_shoppingcartController@invoice')->name('cs.invoice');

});

Route::post('/search', 'cs_searchController@search')->name('cs.search');
Route::get('/search', 'cs_searchController@getSearch')->name('cs.search');
Route::get('/pulsa', 'cs_pulsaController@index')->name('cs.pulsa');
Route::get('/gojekgrab', 'cs_pulsaController@gojekgrab')->name('cs.gojekgrab');
Route::get('/paketdata', 'cs_pulsaController@paketdata')->name('cs.paketdata');
Route::get('/pln', 'cs_pulsaController@pln')->name('cs.pln');


Route::group(['prefix' => '/cart'], function() {
	Route::get('/', 'cs_cartController@cart')->name('cs.cart');
	Route::post('/add', 'cs_cartController@add')->name('cs.add');
	Route::get('/clear', 'cs_cartController@clear')->name('cs.clear');
	Route::post('/remove/{id}', 'cs_cartController@remove')->name('cs.remove');
	Route::post('/update/{id}', 'cs_cartController@update')->name('cs.update');
});


Route::get('/{category}', 'cs_categoryController@index')->name('cs.category');
Route::get('/{category}/{product}', 'cs_productController@index')->name('cs.product');

