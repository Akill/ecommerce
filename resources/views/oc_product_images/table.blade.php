<table class="table table-responsive" id="ocProductImages-table">
    <thead>
        <th>Produk</th>
        <th>Gambar</th>
        <th class="hidden-xs hidden-sm">Urutan Pesanan</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
    @foreach($ocProductImages as $ocProductImage)
        <tr>
            <td>{!! \Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id', $ocProductImage->product_id)->first()->name, 2,'....') !!}</td>
            <td><img src="{!! 'image/product/'.$ocProductImage->product_id.'/'.$ocProductImage->image !!}" alt="Image" width="100" height="100"></td>
            <td class="hidden-xs hidden-sm">{!! $ocProductImage->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductImages.destroy', $ocProductImage->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductImages.show', [$ocProductImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductImages.edit', [$ocProductImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>