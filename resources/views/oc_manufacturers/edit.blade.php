@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Manufaktur
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocManufacturer, ['route' => ['ocManufacturers.update', $ocManufacturer->id], 'method' => 'patch']) !!}

                        @include('oc_manufacturers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection