@extends('layouts.apps')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <aside class="sidebar-left">
                <!-- <h3 class="mb20 text-center">I am looking for</h3> -->
                <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
                    @forelse($ocCategories as $category)
                        @if($category->parent_id==0)
                        <li>
                            <a href="{{ $category->id }}">
                                <i class="fa fa-angle-right"></i>{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
                            </a>
                        </li>
                        @endif
                    @empty
                        <li>
                            <a href="#">
                                Category Tidak Ada
                            </a>
                        </li>
                    @endforelse
                </ul>
            </aside>
            <div class="sidebar-box">
                <h5>Filter</h5>
                <form class="sign-up" method="POST" action="{{url('/search')}}">
                    {!! csrf_field() !!}
                    <hr>
                    <small class="help-block">Cari yang anda butuhkan</small>
                    <input class="form-control" id="search" name="search" type="text" placeholder="Semua kebutuhan" />
                    <!-- <input class="form-control" type="text" name="citys" id="citys" placeholder="Kota" /> -->
                    <br>
                    <small class="help-block">Pilih tempat yang anda cari</small>
                    @php $prov = RajaOngkir::Provinsi()->all(); @endphp
                    <select class="selectpicker form-control" id="citys" name="citys" data-show-subtext="true" data-live-search="true">
                    @foreach($prov as $i)
                    <optgroup label="{{$i['province']}}">
                        @php $kota = RajaOngkir::Kota()->byProvinsi($i['province_id'])->get(); @endphp
                        @foreach($kota as $ko)
                        <option value="{{$ko['city_name']}}">{{$ko['city_name']}}</option>
                        @endforeach
                    </optgroup>
                    @endforeach
                    </select>
                    <hr>
                    <button class="btn btn-primary" type="submit">Cari</button>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row row-wrap">
                @forelse($search as $ProductToCategory)
                @if( strtolower(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['location']) === strtolower($city) )
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="image/product/sampul/{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image'] }}" alt="Image Alternative text" title="Green Furniture" />
                        </header>
                        <div class="product-inner">
                            <h5 class="product-title">{{App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name']}}</h5>
                            <p class="product-desciption">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 10,'....')}}</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li>
                                    <span class="product-price">Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}
                                    </span>
                                    </li>
                                </ul>
                                <form id="idForm" method="POST" action="{!! url('/cart/add') !!}">
                                {{ csrf_field() }}
                                <ul class="product-actions-list">
                                        <input type="hidden" name="id" value="{{ $ProductToCategory->product_id }}">
                                        <input type="hidden" name="name" value="{{ App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'] }}">
                                        <input type="hidden" name="price" value="{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'] }}">
                                        <input type="hidden" name="qty" value="1">
                                        <li>
                                        <!-- <a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a> -->
                                            <button type="submit" id="tombol" class="btn btn-sm btn-primary btn-squared add-to-cart">
                                            <i class="fa fa-shopping-cart"></i> To Cart
                                            </button>
                                        </li>
                                        <li><a href="{{url($ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}" class="btn btn-sm btn-success btn-squared add-to-cart"><i class="fa fa-bars"></i> Details</a>
                                        </li>
                                </ul>
                                </form>
                            </div>
                            <p class="product-location"><i class="fa fa-map-marker"></i> {{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['location'] }}</p>{{App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['location'] == $city}}
                        </div>
                    </div>
                </div>
                @endif
                @empty
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="gap"></div>
<div class="container">
    <div class="row row-wrap">
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-truck sale-point-icon"></i>
                <h5 class="sale-point-title">Cepat & Bebas pengiriman</h5>
                <p class="sale-point-description">Melayani dengan cepat pembelian anda, dan bebas biaya pengiriman untuk wilayah teretentu dan jenis barang-barang tertentu.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-tags sale-point-icon"></i>
                <h5 class="sale-point-title">Diskon terbaik</h5>
                <p class="sale-point-description">Dapatkan diskon setiap hari besar di indonesia, serta dapatkan promo barang-barang populer dengan tanda ini.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-money sale-point-icon"></i>
                <h5 class="sale-point-title">Garansi uang kemabli</h5>
                <p class="sale-point-description">Kami melayani anda dengan mengutamakan pelayanan dan kualitas barang, jika merasa kurang silahkan hubungi kami.</p>
            </div>
        </div>
    </div>
</div>
@endsection