<table class="table table-responsive" id="ocManufacturers-table">
    <thead>
        <tr>
            <th>Nama</th>
        <th>Gambar</th>
        <th>Sort Order</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocManufacturers as $ocManufacturer)
        <tr>
            <td>{!! $ocManufacturer->name !!}</td>
            <td>{!! $ocManufacturer->image !!}</td>
            <td>{!! $ocManufacturer->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocManufacturers.destroy', $ocManufacturer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocManufacturers.show', [$ocManufacturer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocManufacturers.edit', [$ocManufacturer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>