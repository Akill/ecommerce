<table class="table table-responsive" id="ocProductDescriptions-table">
    <thead>
        <th>Produk</th>
        <th class="hidden-xs hidden-sm">Bahasa</th>
        <th>Nama</th>
        <th class="hidden-xs hidden-sm">Judul Halaman</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
    @foreach($ocProductDescriptions as $ocProductDescription)
        <tr>
            <td>
            {{ \Illuminate\Support\Str::words(App\Models\oc_product::where('id',$ocProductDescription->product_id)->first()['model'], 2,'....') }}
            </td>
            <td class="hidden-xs hidden-sm">{!! App\Models\oc_language::find($ocProductDescription->language_id)->name !!}</td>
            <td>{!! \Illuminate\Support\Str::words($ocProductDescription->name, 2,'....') !!}</td>
            <td class="hidden-xs hidden-sm">{!! $ocProductDescription->meta_title !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductDescriptions.destroy', $ocProductDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductDescriptions.show', [$ocProductDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductDescriptions.edit', [$ocProductDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apa anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>