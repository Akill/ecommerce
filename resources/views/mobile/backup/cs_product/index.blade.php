@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
<div class="content-container">
    
    <!-- Product Header -->
    <div class="content-header">

        <div class="breadcrumbs animated fadeIn"><!-- Product breadcrumb -->
            <a href="#">
            {!! App\Models\oc_category_description::where('oc_category_id',App\Models\oc_product_to_category::where('product_id',$ocProductDescription->id)->first()['category_id'])->first()['name'] !!}</a>
            <a class="active" href="#!">{{ \Illuminate\Support\Str::words($ocProductDescription->name, 3,'') }}</a>
        </div><!-- End Product breadcrumb -->

        <h2 class="product-title animated fadeIn">{{$ocProductDescription->name}}</h2><!-- Product title -->

        <!-- Product thumbnail slider -->
        <ul class="product-slider animated fadeInRight"><!-- Single thumbnail -->
            @php $i=1 @endphp
            @foreach($ocProductImage as $image)
            <li>
                <a class="fullscreen-icon swipebox" href="{{url('/image/product/'.$image->product_id.'/'.$image->image)}}" title="Gambar {{$i++}}">
                    <i class="fa fa-expand"></i>
                </a>
                <img src="{{url('/image/product/'.$image->product_id.'/'.$image->image)}}" alt="img" />
            </li>
            @endforeach
        </ul><!-- End single thumbnail -->
        <div class="slick-thumbs"><!-- Small thumb indicator -->
            <ul>
                @foreach($ocProductImage as $image)
                <li>
                    <img src="{{asset('/image/product/'.$image->product_id.'/'.$image->image)}}" alt="img" />
                </li>
                @endforeach
            </ul>
        </div><!-- End small thumb indicator -->
        <!-- End Product thumbnail slider -->

        <!-- Product meta -->
        <div class="product-meta animated fadeInUp">
            <div class="rating">
                <i class="fa fa-star active"></i>
                <i class="fa fa-star active"></i>
                <i class="fa fa-star active"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <div class="price">
                Rp. {{number_format($ocProduct->price,2,',','.')}}
            </div>
            <!-- Beside .in-stock class, you can also use .out-of-stock class -->
            <div class="availability in-stock">
                {{App\Models\oc_stock_status::where('id',$ocProduct->stock_status_id)->first()['name']}}
            </div>
        </div>
        <!-- End Product meta -->

    </div>
    <!-- End Product Header -->
    
    <!-- Product tabs -->
    <div class="product-tabs">
        <ul class="tabs">
            <li class="tab"><a class="active" href="#detail">Detail</a></li>
            <li class="tab"><a href="#spesifikasi">Spesifikasi</a></li>
            <li class="tab"><a href="#review">Review</a></li>
        </ul>
    </div>
    <!-- End Product tabs -->
    
    <!-- Product content -->
    <div class="product-content">
        
        <!-- Product detail tabs -->
        <div class="tab-content" id="detail">
            <p>{{$ocProductDescription->description}}</p>
        </div>
        <!-- End Product detail tabs -->

        <!-- Spesifikasi detail tabs -->
        <div class="tab-content" id="spesifikasi">
            <table class="table table-striped mb0">
                <tbody>
                    <tr>
                        <td>Weight</td>
                        <td>{{$ocProduct->weight}} {{App\Models\oc_weight_class::find($ocProduct->weight_class_id)['value']}}</td>
                    </tr>
                    <tr>
                        <td>Dimentions</td>
                        <td>{{$ocProduct->length}} x {{$ocProduct->width}} x {{$ocProduct->height}} cm</td>
                    </tr>
                    <tr>
                        <td>Location</td>
                        <td>{{$ocProduct->location}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- End Spesifikasi detail tabs -->

        <!-- Product review list tabs -->
        <div class="tab-content" id="review">
            
            <ol class="product-review-list">
                <li>
                    <div class="review-idty">
                        <div class="name">
                            Andriy Sheva
                        </div>
                        <div class="product-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="review-ctn">
                        awesome product
                    </div>
                </li>
                <li>
                    <div class="review-idty">
                        <div class="name">
                            Carlos de Mello
                        </div>
                        <div class="product-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="review-ctn">
                        The product has come softly, thank you maleo.
                    </div>
                </li>
                <li>
                    <div class="review-idty">
                        <div class="name">
                            Jackson Thiago
                        </div>
                        <div class="product-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="review-ctn">
                        No one doubt about quality of this product. Congratulations for all.
                    </div>
                </li>
                <li>
                    <div class="review-idty">
                        <div class="name">
                            Melanie Ricardo
                        </div>
                        <div class="product-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="review-ctn">
                        Only one thing, please give us some explanation more.
                    </div>
                </li>
            </ol>
        </div>
        <!-- End Product review list tabs -->

    </div>
    <!-- End Product content -->
    
    <!-- Product navigation -->
    <div class="product-action margin-bottom">
        <form id="idForm" method="POST" action="{!! url('mobile/cart/add') !!}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $ocProduct->id }}">
            <input type="hidden" name="name" value="{{ App\Models\oc_product_description::where('product_id',$ocProduct->id)->first()['name'] }}">
            <input type="hidden" name="price" value="{{ App\Models\oc_product::where('id',$ocProduct->id)->first()['price'] }}">
            <input type="hidden" name="qty" value="1">
            <div class="row">
                <center>
                    <button type="submit" id="tombol" class="btn green block margin-bottom_low">
                        <i class="fa fa-shopping-cart"></i> Tambah
                    </button>
                    <button class="btn grey block margin-bottom_low">
                        <i class="fa fa-star"></i> Daftar Belanja
                    </button> 
                </center>
            </div>
        </form>
    </div>
    <!-- End Product navigation -->

    <!-- Product share -->
    <div class="product-share">
        <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
        <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
        <a href="#" class="gplus"><i class="fa fa-google-plus"></i></a>
        <a href="#" class="pint"><i class="fa fa-pinterest"></i></a>
    </div>
    <!-- End Product share -->

    <div class="line"></div>
    
    <!-- Related product section -->
    <div class="page-block">
        
        <h2 class="block-title">
            <span>Mungkin yang anda inginkan</span>
        </h2>

        <ol class="product-list-slider">
            @forelse($ocProductToCategory as $ProductToCategory)
            <li>
                <div class="thumb">
                    <a href="{{url('/mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
                        <img src="{{ asset('image/product/sampul/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image']) }}" alt="">
                    </a>
                </div>
                <div class="product-ctn">
                    <div class="product-name">
                        <a href="{{url('/mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
                            {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 2,'....')}}
                        </a>
                    </div>
                    <div class="product-name">
                        <p class="product-desciption">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 10,'....')}}</p>
                    </div>
                    <div class="price">
                        <span class="price-current">Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}</span>
                    </div>
                </div>
            </li>
            @empty
            @endforelse
        </ol>

        <div class="clear"></div><!-- Use this class (.clear) to clearing float -->

    </div>
    <!-- End Related product section -->
</div>
<!-- END CONTENT CONTAINER -->
@endsection
