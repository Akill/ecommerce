@extends('layouts.apps')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <aside class="sidebar-left">
                <div class="box clearfix">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($cart as $carts)
                            <tr>
                                <td>{{$carts->name}}</td>
                                <td>{{$carts->qty}}</td>
                                <td>Rp. {{number_format($carts->price,2,',','.')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <ul class="cart-total-list mb0">
                        <li><span>Sub Total</span><span>Rp. {{ Cart::subtotal() }}</span>
                        </li>
                        <li><span>Pengiriman</span><span id="etd">@if(isset($req['etdx'])) {{ $req['etdx'] }} @endif</span>
                        </li>
                        <li><span>Paket</span><span id="paketx">@if(isset($req['province3']))Rp. {{number_format($req['harga'],2,',','.')}} @endif</span>
                        </li>
                        <li><span>Pajak</span><span>{{ Cart::tax() }}</span>
                        </li>
                        <li>
                            <span>Total</span><span id="total">Rp. {{ Cart::total() }}</span>
                            <input type="hidden" name="totalx" value="{{ Cart::total() }}">
                        </li>
                    </ul>
                </div>
            </aside>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <h3>Informasi Pembayaran</h3>
                        <legend>Address</legend>
                    @if(!isset($req['province1']) && !isset($req['province2']) && !isset($req['province3']) ) 
                        {!!Form::open(['route' => 'cs.city','name' => 'form1','id' => 'form1'])!!}
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="province">Provinsi</label>
                                </div>
                                <div class="col-md-8">
                                    <select class="form-control" name="province1" id="province1">     
                                        @foreach($rjo_pro as $rjo)
                                        <option value="{{ $rjo['province_id'] }}">{{ $rjo['province'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="submit" class="btn btn-primary" value="Pilih" form="form1">
                                </div>
                            </div>
                        {!!Form::close()!!}
                    @endif

                    @if(isset($req['province1']))

                    {!!Form::open(['route' => 'cs.cost','name' => 'form2','id' => 'form2'])!!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="city">Kota</label>
                            </div>
                            <input type="hidden" name="province2" id="province2" value="{{$req['province1']}}">
                            <div class="col-md-8">
                                <select class="form-control" name="city1" id="city1">
                                    @foreach($rjo_city as $rjo)
                                    <option value="{{ $rjo['city_id'] }}" @if($req['city1'] == $rjo['city_id']) selected @endif>{{ $rjo['city_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-primary" value="Pilih">
                            </div>
                        </div>
                    {!!Form::close()!!}

                    @endif

                    @if(isset($req['province2'])) 
                    {!!Form::open(['route' => 'cs.kurir','name' => 'form3','id' => 'form3'])!!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="kurir">Kurir</label>
                            </div>
                            <input type="hidden" name="city2" id="city2" value="{{$req['city1']}}">
                            <input type="hidden" name="province3" id="province3" value="{{$req['province2']}}">
                            <input type="hidden" name="status1" id="status1">
                            <input type="hidden" name="kurirxx" id="kurirxx">
                            <input type="hidden" name="harga" id="harga">
                            <input type="hidden" name="etdx" id="etdx">
                            <div class="col-md-8">
                                <select class="form-control" name="kurir1" id="kurir1">
                                    <optgroup id="optpaket" label="JNE">
                                        @php 
                                        $data = RajaOngkir::Cost([
                                            'origin'=>493,
                                            'destination'=> $req['city1'],
                                            'weight'=> $cost,
                                            'courier'=> 'jne',
                                        ])->get();
                                        @endphp
                                        @foreach($data[0]['costs'] as $kurirs)
                                        @php $kurirs['cost'][0]['paket'] = 'jne'; @endphp
                                        <option value="{{json_encode($kurirs['cost'][0])}}">{{$kurirs['service']}} - Rp. {{$kurirs['cost'][0]['value']}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup id="optpaket" label="TIKI">
                                        @php 
                                        $data = RajaOngkir::Cost([
                                            'origin'=>493,
                                            'destination'=> $req['city1'],
                                            'weight'=> $cost,
                                            'courier'=> 'tiki',
                                        ])->get();
                                        @endphp
                                        @foreach($data[0]['costs'] as $kurirs)
                                        @php $kurirs['cost'][0]['paket'] = 'tiki'; @endphp
                                        <option value="{{json_encode($kurirs['cost'][0])}}">{{$kurirs['service']}} - Rp. {{$kurirs['cost'][0]['value']}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup id="optpaket" label="POS">
                                        @php 
                                        $data = RajaOngkir::Cost([
                                            'origin'=>493,
                                            'destination'=> $req['city1'],
                                            'weight'=> $cost,
                                            'courier'=> 'pos',
                                        ])->get();
                                        @endphp
                                        @foreach($data[0]['costs'] as $kurirs)
                                        @php $kurirs['cost'][0]['paket'] = 'pos'; @endphp
                                        <option value="{{json_encode($kurirs['cost'][0])}}">{{$kurirs['service']}} - Rp. {{$kurirs['cost'][0]['value']}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-primary" value="Pilih">
                            </div>
                        </div>
                    {!!Form::close()!!}
                    @endif

                    @if(isset($req['city2'])) 

                        {!!Form::open(['route' => 'cs.checkout_strore'])!!}

                            <input type="hidden" name="city" id="city" value="{{$req['city2']}}">
                            <input type="hidden" name="province" id="province" value="{{$req['province3']}}">
                            <input type="hidden" name="status" id="status" value="{{$req['kurir1']}}">
                            <input type="hidden" name="paket" id="paket" value="{{$req['kurirxx']}}">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="district">Kelurahan</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="district" id="district" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="address">Alamat</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="address" id="address" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="zip_kode">Kode Pos</label>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="zip_kode" id="zip_kode" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""></label>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary" value="Proses Pembelian">
                                </div>
                            </div>
                        {!!Form::close()!!}

                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="gap"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    function convertToRupiah(angka)
    {
        var rupiah = '';        
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }

    $(document).ready(function () {
        $('#kurir1').change(function () {
            var obj = JSON.parse($(this).val());
            $("#paketx").text('Rp. '+convertToRupiah(obj.value));
            $("#kurirxx").val(obj.paket);
            $("#harga").val(obj.value);
            $("#etd").text(obj.etd);
            $("#etdx").val(obj.etd);
        });
    });
</script>
@endsection