<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\oc_product_description;

use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Flash;
use Response;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;

use App\Models\oc_product_to_category;
use App\Models\oc_product;
use Cart;

class cs_searchController extends Controller
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
    }
    /**
     * Seacrh oc_product in databases.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function search(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $query = $request->get('search');

        $city = $request->get('citys');

        $hasil = oc_product_description::where('description', 'LIKE', '%' . $query . '%')
                    ->orWhere('name', 'LIKE', '%' . $query . '%')
                    ->paginate(10);
        return view('cs_search.index',[
                        'search' => $hasil, 
                        'city' => $request->citys,
                        'ocCategories' => $ocCategories
                    ]
                );
    }

    /**
     * Seacrh oc_product in databases.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getSearch()
    {
        $ocCategories = $this->ocCategoryRepository->all();

        $hasil = oc_product_description::paginate(10);
        return view('cs_search.index',[
                        'search' => $hasil, 
                        'city' => null,
                        'ocCategories' => $ocCategories
                    ]
                );
    }
    /**
     * Seacrh oc_product in databases.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchm(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $query = $request->get('search');

        $hasil = oc_product_description::where('description', 'LIKE', '%' . $query . '%')
                    ->orWhere('name', 'LIKE', '%' . $query . '%')
                    ->paginate(10);
        return view('mobile.cs_search.index',[
                        'search' => $hasil, 
                        'ocCategories' => $ocCategories
                    ]
                );
    }

    /**
     * Seacrh oc_product in databases.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getSearchm(Request $request)
    {
        $ocCategories = $this->ocCategoryRepository->all();

        if ($request->input('cari') == NULL) {
            return view('mobile.cs_search.index',['ocCategories' => $ocCategories]);
        }
        else {
            $produk = oc_product_description::where('description', 'LIKE', '%' . $request->input('cari') . '%')
                    ->orWhere('name', 'LIKE', '%' . $request->input('cari') . '%')
                    ->paginate(10);
            return view('mobile.cs_search.index',[
                'ocCategories' => $ocCategories,
                'produk' => $produk,
                ]);
        }
    }

    /**
     * Seacrh oc_product in databases.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchdetail($id)
    {
        $ocCategories = $this->ocCategoryRepository->all();

        $hasil = oc_product_description::paginate(10);
        return view('mobile.cs_search.index',[
                        'search' => $hasil, 
                        'city' => $id,
                        'ocCategories' => $ocCategories
                    ]
                );
    }

    public function searchkota($nama)
    {
        $ocCategories = $this->ocCategoryRepository->all();
        $produk = oc_product::where('location', 'LIKE', '%' . $nama . '%')->paginate(10);

        $hasil = oc_product_description::paginate(10);
        return view('mobile.cs_search.kota',[
                        'produk' => $produk,
                        'ocCategories' => $ocCategories
                    ]
                );
    }
}
