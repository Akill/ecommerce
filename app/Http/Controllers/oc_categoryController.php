<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\oc_category;
use Auth;

class oc_categoryController extends AppBaseController
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
    }

    /**
     * Display a listing of the oc_category.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();
        if (!Auth::user()->status) {
            $ocCategories = oc_category::where('user_id',Auth::user()->id)->get();
        }
        return view('oc_categories.index')
            ->with('ocCategories', $ocCategories);
    }

    /**
     * Show the form for creating a new oc_category.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_categories.create');
    }

    /**
     * Store a newly created oc_category in storage.
     *
     * @param Createoc_categoryRequest $request
     *
     * @return Response
     */
    public function store(Createoc_categoryRequest $request)
    {
        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/category/catalog/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }

        $input = $request->all();

        $input['image'] = $fileName;

        $input['user_id'] = Auth::user()->id;

        $ocCategory = $this->ocCategoryRepository->create($input);

        Flash::success('Oc Category saved successfully.');

        return redirect(route('ocCategories.index'));
    }

    /**
     * Display the specified oc_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            Flash::error('Oc Category not found');

            return redirect(route('ocCategories.index'));
        }

        return view('oc_categories.show')->with('ocCategory', $ocCategory);
    }

    /**
     * Show the form for editing the specified oc_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            Flash::error('Oc Category not found');

            return redirect(route('ocCategories.index'));
        }

        return view('oc_categories.edit')->with('ocCategory', $ocCategory);
    }

    /**
     * Update the specified oc_category in storage.
     *
     * @param  int              $id
     * @param Updateoc_categoryRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_categoryRequest $request)
    {
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            Flash::error('Oc Category not found');

            return redirect(route('ocCategories.index'));
        }
        
        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/category/catalog/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }

        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $input['image'] = $fileName;

        $ocCategory = $this->ocCategoryRepository->update($input, $id);

        Flash::success('Oc Category updated successfully.');

        return redirect(route('ocCategories.index'));
    }

    /**
     * Remove the specified oc_category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            Flash::error('Oc Category not found');

            return redirect(route('ocCategories.index'));
        }

        $this->ocCategoryRepository->delete($id);

        Flash::success('Oc Category deleted successfully.');

        return redirect(route('ocCategories.index'));
    }
}
