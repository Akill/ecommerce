<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProductDescription->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Id Produk:') !!}
    <p>{!! $ocProductDescription->product_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Id Bahasa:') !!}
    <p>{!! App\Models\oc_language::find($ocProductDescription->language_id)->name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nama:') !!}
    <p>{!! $ocProductDescription->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Deskripsi:') !!}
    <p>{!! $ocProductDescription->description !!}</p>
</div>

<!-- Tag Field -->
<div class="form-group">
    {!! Form::label('tag', 'Tag:') !!}
    <p>{!! $ocProductDescription->tag !!}</p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    {!! Form::label('meta_title', 'Judul Halaman:') !!}
    <p>{!! $ocProductDescription->meta_title !!}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', 'Deskripsi Halaman:') !!}
    <p>{!! $ocProductDescription->meta_description !!}</p>
</div>

<!-- Meta Keyword Field -->
<div class="form-group">
    {!! Form::label('meta_keyword', 'Kata Kunci Halaman:') !!}
    <p>{!! $ocProductDescription->meta_keyword !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Tanggal Dibuat:') !!}
    <p>{!! $ocProductDescription->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Tanggal Diubah:') !!}
    <p>{!! $ocProductDescription->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Tanggal Dihapus:') !!}
    <p>{!! $ocProductDescription->deleted_at !!}</p>
</div>

