@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Spanduk
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocBanner, ['route' => ['ocBanners.update', $ocBanner->id], 'method' => 'patch']) !!}

                        @include('oc_banners.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection