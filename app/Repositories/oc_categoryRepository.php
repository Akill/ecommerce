<?php

namespace App\Repositories;

use App\Models\oc_category;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_categoryRepository
 * @package App\Repositories
 * @version October 14, 2017, 4:48 pm UTC
 *
 * @method oc_category findWithoutFail($id, $columns = ['*'])
 * @method oc_category find($id, $columns = ['*'])
 * @method oc_category first($columns = ['*'])
*/
class oc_categoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'parent_id',
        'top',
        'column',
        'sort_order',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category::class;
    }
}
