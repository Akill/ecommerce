	<!-- <div class="top-navbar"> -->
		<!-- 		
		<div class="navbar-top">
			<div class="site-title-top" style="margin-right: 10px;margin-left: 100px;">
				<i class="fa fa-user-circle-o"></i> Selamat datang, @auth {{Auth::user()->name}} @else member @endauth
			</div>
		</div> 
		-->
		<div class="top-navbar">
			<div class="top-navbar-left">
				<a href="{{ asset('#') }}" id="menu-left" data-activates="slide-out-left">
					<i class="fa fa-bars"></i>
				</a>
			</div>
			<div class="top-navbar-right">
				<a href="{{ asset('#') }}" class="dropdown-button" data-activates="dropdown1">
					<i class="fa fa-user-circle-o"></i>
				</a>				
				<ul id="dropdown1" class="dropdown-content">
					@if (Route::has('login'))
                        @if (Auth::check())
                        <li>
                            <a href="{{ url('/users/'.Auth::user()->id) }}">
                                <i class="fa fa-sign-in"></i>{{Auth::user()->name}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/mobile/home') }}">
                                <i class="fa fa-home"></i>Home
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="{{ url('/mobile/login') }}">
                                <i class="fa fa-sign-in"></i>Masuk
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/mobile/register') }}">
                                <i class="fa fa-edit"></i>Daftar
                            </a>
                        </li>
                        @endif
                    @endif
					<!-- <li><a href="{{ asset('#') }}"><i class="fa fa-server"></i> Login</a></li> -->
					<!-- <li><a href="{{ asset('form-account.html') }}"><i class="fa fa-user"></i> My Profile</a></li> -->
					<!-- <li><a href="{{ asset('order-history.html') }}"><i class="fa fa-history"></i> Order History</a></li> -->
					<li class="divider"></li>
				</ul>

				<a href="{{ asset('#') }}" id="menu-right" data-activates="slide-out-right">
					<span class="cart-badge">{{Cart::count()}}</span>
					<i class="fa fa-shopping-basket"></i>
				</a>
			</div>
			<div class="site-title">
				<h1>CELEBESTORE</h1>
			</div>
		</div>
	<!-- </div> -->