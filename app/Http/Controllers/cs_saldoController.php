<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_saldoRequest;
use App\Http\Requests\Updatecs_saldoRequest;
use App\Repositories\cs_saldoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cs_saldoController extends AppBaseController
{
    /** @var  cs_saldoRepository */
    private $csSaldoRepository;

    public function __construct(cs_saldoRepository $csSaldoRepo)
    {
        $this->csSaldoRepository = $csSaldoRepo;
    }

    /**
     * Display a listing of the cs_saldo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csSaldoRepository->pushCriteria(new RequestCriteria($request));
        $csSaldos = $this->csSaldoRepository->all();

        return view('cs_saldos.index')
            ->with('csSaldos', $csSaldos);
    }

    /**
     * Show the form for creating a new cs_saldo.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_saldos.create');
    }

    /**
     * Store a newly created cs_saldo in storage.
     *
     * @param Createcs_saldoRequest $request
     *
     * @return Response
     */
    public function store(Createcs_saldoRequest $request)
    {
        $input = $request->all();

        $csSaldo = $this->csSaldoRepository->create($input);

        Flash::success('Cs Saldo saved successfully.');

        return redirect(route('csSaldos.index'));
    }

    /**
     * Display the specified cs_saldo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csSaldo = $this->csSaldoRepository->findWithoutFail($id);

        if (empty($csSaldo)) {
            Flash::error('Cs Saldo not found');

            return redirect(route('csSaldos.index'));
        }

        return view('cs_saldos.show')->with('csSaldo', $csSaldo);
    }

    /**
     * Show the form for editing the specified cs_saldo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csSaldo = $this->csSaldoRepository->findWithoutFail($id);

        if (empty($csSaldo)) {
            Flash::error('Cs Saldo not found');

            return redirect(route('csSaldos.index'));
        }

        return view('cs_saldos.edit')->with('csSaldo', $csSaldo);
    }

    /**
     * Update the specified cs_saldo in storage.
     *
     * @param  int              $id
     * @param Updatecs_saldoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_saldoRequest $request)
    {
        $csSaldo = $this->csSaldoRepository->findWithoutFail($id);

        if (empty($csSaldo)) {
            Flash::error('Cs Saldo not found');

            return redirect(route('csSaldos.index'));
        }

        $csSaldo = $this->csSaldoRepository->update($request->all(), $id);

        Flash::success('Cs Saldo updated successfully.');

        return redirect(route('csSaldos.index'));
    }

    /**
     * Remove the specified cs_saldo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csSaldo = $this->csSaldoRepository->findWithoutFail($id);

        if (empty($csSaldo)) {
            Flash::error('Cs Saldo not found');

            return redirect(route('csSaldos.index'));
        }

        $this->csSaldoRepository->delete($id);

        Flash::success('Cs Saldo deleted successfully.');

        return redirect(route('csSaldos.index'));
    }
}
