<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

@if(Auth::user()->status)
<!-- Access Field -->
<div class="form-group col-sm-6">
    {!! Form::label('access', 'Akses:') !!}
    {!! Form::text('access', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    <input type="password" class="form-control" name="password" id="password">
</div>

<!-- Type Of Identity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_of_identity', 'Jenis Kartu Pengenal:') !!}
    {!! Form::text('type_of_identity', null, ['class' => 'form-control']) !!}
</div>

<!-- Identity Number Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('identity_number', 'Nomor Identitas:') !!}
    {!! Form::text('identity_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Jenis Kelamin:') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'No. Telp:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job', 'Pekerjaan:') !!}
    {!! Form::text('job', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Alamat:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Districts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('districts', 'Kecamatan:') !!}
    {!! Form::text('districts', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'Kota / Kabupaten:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province', 'Provinsi:') !!}
    {!! Form::text('province', null, ['class' => 'form-control']) !!}
</div>

<!-- Zip Kode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip_kode', 'Kode Pos:') !!}
    {!! Form::text('zip_kode', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('picture', 'Gambar:') !!}
    {!! Form::file('picture',null,array('id'=>'picture','class'=>'form-control')) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Batal</a>
</div>
