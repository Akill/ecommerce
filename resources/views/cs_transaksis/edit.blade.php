@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cs Transaksi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($csTransaksi, ['route' => ['csTransaksis.update', $csTransaksi->id], 'method' => 'patch']) !!}

                        @include('cs_transaksis.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection