<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('access')->default('user');
            $table->string('username')->nullable()->unique();
            $table->string('type_of_identity')->nullable();
            $table->text('identity_number')->nullable();
            $table->string('gender')->nullable();
            $table->string('phone')->nullable()->unique();
            $table->string('job')->nullable();
            $table->string('address')->nullable();
            $table->string('districts')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zip_kode')->nullable();
            $table->text('picture')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
