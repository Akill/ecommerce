<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingcartTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create(config('cart.database.table'), function (Blueprint $table) {
            $table->increments('id'); //kode id
            $table->integer('user_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('districts')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zip_kode')->nullable();
            $table->string('bukti')->nullable(); // bukti pembayaaran
            $table->longText('content')->nullable(); // json array pembelian barang
            $table->string('paket')->nullable(); // jenis paket yang di pilih (oke, reg, yes)
            $table->string('status')->default('unpaid');; // unpaid / paid (sudah di transfer atau belum)
            $table->date('end_date')->nullable(); // batas pembayaran
            $table->nullableTimestamps();
            $table->date('deleted_at')->nullable(); // waktu delete

            // $table->primary('id');
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop(config('cart.database.table'));
    }
}
