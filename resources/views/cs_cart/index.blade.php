@extends('layouts.apps')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table cart-table responsive">
                <thead>
                    <tr>
                        <th>Produk</th>
                        <th class="hidden-xs hidden-sm">Nama</th>
                        <th>Jumlah</th>
                        <th>Perbarui</th>
                        <th class="hidden-xs hidden-sm">Harga</th>
                        <th class="hidden-xs hidden-sm">Hapus</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cart as $carts)
                    <tr>
                        <td class="cart-item-image">
                            <a href="#">
                                <img src="image/product/sampul/{{ App\Models\oc_product::where('id',$carts->id)->first()['model'].'/'.App\Models\oc_product::where('id',$carts->id)->first()['image'] }}" alt="Image Alternative text" title="AMaze" width="70" height="70" />
                            </a>
                        </td>
                        <td class="hidden-xs hidden-sm"><a href="{{url('/'.App\Models\oc_product_to_category::where('product_id',$carts->id)->first()['category_id'].'/'.$carts->id)}}">{{\Illuminate\Support\Str::words($carts->name, 3,'....')}}</a>
                        </td>
                        <form method="POST" action="{{ url('/cart/update/'.$carts->rowId) }}">
                        {{ csrf_field() }}
                        <td class="cart-item-quantity">
                            <i class="fa fa-minus cart-item-minus"></i>
                                <input type="text" name="cart-quantity" id="cart-quantity" class="cart-quantity" value="{{$carts->qty}}" />
                            <i class="fa fa-plus cart-item-plus"></i>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fa fa-edit"></i> Perbarui
                            </button>
                        </td>
                        </form>
                        <td class="hidden-xs hidden-sm">{{number_format($carts->price,2,',','.')}}</td>
                        <td class="cart-item-remove hidden-sm hidden-xs">
                            <form role="form" method="POST" action="{!! url('cart/remove/'.$carts->rowId) !!}">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <button type="submit" class="btn btn-sm">
                                    <i class="fa fa-trash-o"></i> 
                                    <!-- <i class="fa fa-times"></i>  -->Hapus
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-3">
                <form action="{{ url('/cart/clear') }}">
                    <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-trash-o"></i> Bersihkan semua</button>
                </form>
            </div>
        </div>
        <div class="col-md-3">
            <ul class="cart-total-list">
                <li><span>Sub Total</span><span>{{ Cart::subtotal() }}</span>
                </li>
                <li><span>Total</span><span>{{ Cart::total() }}</span>
                </li>
            </ul>
            @auth
                {!!Form::open(['route' => 'cs.checkout_create'])!!}
                <button type="submit"  class="btn btn-primary btn-block btn-sm btn-squared">
                    Lanjutkan Pembayaran
                </button>
                <input type="hidden" name="checkout" id="checkout" style="display: none;" value="{{ Cart::content() }}">
                {!!Form::close()!!}
            @else
                <a href="{{ url(route('login')) }}" class="btn btn-primary btn-block btn-sm btn-squared">
                    <!-- <i class="fa fa-cc-visa"></i> -->
                    Masuk untuk melanjutkan
                </a>
            @endauth
        </div>
    </div>
    <div class="gap"></div>
</div>
@endsection