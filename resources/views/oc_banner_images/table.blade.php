<table class="table table-responsive" id="ocBannerImages-table">
    <thead>
        <tr>
            <th>Spanduk Id</th>
            <th>Link</th>
            <th>Gambar</th>
            <th>Urutan Tampilan</th>
            <th colspan="3">Aksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocBannerImages as $ocBannerImage)
        <tr>
            <td>{!! $ocBannerImage->banner_id !!}</td>
            <td>{!! $ocBannerImage->link !!}</td>
            <td>{!! $ocBannerImage->image !!}</td>
            <td>{!! $ocBannerImage->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocBannerImages.destroy', $ocBannerImage->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocBannerImages.show', [$ocBannerImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocBannerImages.edit', [$ocBannerImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>