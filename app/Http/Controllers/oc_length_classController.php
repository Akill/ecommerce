<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_length_classRequest;
use App\Http\Requests\Updateoc_length_classRequest;
use App\Repositories\oc_length_classRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_length_classController extends AppBaseController
{
    /** @var  oc_length_classRepository */
    private $ocLengthClassRepository;

    public function __construct(oc_length_classRepository $ocLengthClassRepo)
    {
        $this->ocLengthClassRepository = $ocLengthClassRepo;
    }

    /**
     * Display a listing of the oc_length_class.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLengthClassRepository->pushCriteria(new RequestCriteria($request));
        $ocLengthClasses = $this->ocLengthClassRepository->all();

        return view('oc_length_classes.index')
            ->with('ocLengthClasses', $ocLengthClasses);
    }

    /**
     * Show the form for creating a new oc_length_class.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_length_classes.create');
    }

    /**
     * Store a newly created oc_length_class in storage.
     *
     * @param Createoc_length_classRequest $request
     *
     * @return Response
     */
    public function store(Createoc_length_classRequest $request)
    {
        $input = $request->all();

        $ocLengthClass = $this->ocLengthClassRepository->create($input);

        Flash::success('Oc Length Class saved successfully.');

        return redirect(route('ocLengthClasses.index'));
    }

    /**
     * Display the specified oc_length_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            Flash::error('Oc Length Class not found');

            return redirect(route('ocLengthClasses.index'));
        }

        return view('oc_length_classes.show')->with('ocLengthClass', $ocLengthClass);
    }

    /**
     * Show the form for editing the specified oc_length_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            Flash::error('Oc Length Class not found');

            return redirect(route('ocLengthClasses.index'));
        }

        return view('oc_length_classes.edit')->with('ocLengthClass', $ocLengthClass);
    }

    /**
     * Update the specified oc_length_class in storage.
     *
     * @param  int              $id
     * @param Updateoc_length_classRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_length_classRequest $request)
    {
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            Flash::error('Oc Length Class not found');

            return redirect(route('ocLengthClasses.index'));
        }

        $ocLengthClass = $this->ocLengthClassRepository->update($request->all(), $id);

        Flash::success('Oc Length Class updated successfully.');

        return redirect(route('ocLengthClasses.index'));
    }

    /**
     * Remove the specified oc_length_class from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            Flash::error('Oc Length Class not found');

            return redirect(route('ocLengthClasses.index'));
        }

        $this->ocLengthClassRepository->delete($id);

        Flash::success('Oc Length Class deleted successfully.');

        return redirect(route('ocLengthClasses.index'));
    }
}
