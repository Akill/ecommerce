<table class="table table-responsive" id="ocStockStatuses-table">
    <thead>
        <tr>
            <th>Bahasa Id</th>
            <th>Nama</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocStockStatuses as $ocStockStatus)
        <tr>
            <td>{!! $ocStockStatus->language_id !!}</td>
            <td>{!! $ocStockStatus->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocStockStatuses.destroy', $ocStockStatus->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocStockStatuses.show', [$ocStockStatus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocStockStatuses.edit', [$ocStockStatus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>