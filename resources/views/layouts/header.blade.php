<div class="top-main-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="/">
                    <img src="{{asset('/celebestore-min.png')}}" alt="Image Alternative text" title="Image Title" />
                </a>
            </div>
            <!-- <div class="col-md-2"><h4>{{ config('app.name', 'AFREL') }}</h4></div> -->
            <div class="col-md-5 col-md-offset-3">
                <div class="pull-right">
                    <ul class="login-register">
                        <li class="shopping-cart shopping-cart-white"><a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i>Keranjang ( {{Cart::count()}} ) </a>
                            <div class="shopping-cart-box">
                                <ul class="shopping-cart-items">
                                @foreach(Cart::content() as $cart)
                                    <li>
                                        <a href="{{ url('#') }}">
                                            <img src="{{ asset('image/product/sampul/'.App\Models\oc_product::where('id',$cart->id)->first()['model'].'/'.App\Models\oc_product::where('id',$cart->id)->first()['image']) }}" alt="Image Alternative text" title="AMaze" />
                                            <h5>{{$cart->name}}</h5><span class="shopping-cart-item-price">{{$cart->price}}</span>
                                        </a>
                                    </li>
                                @endforeach
                                </ul>
                                <ul class="list-inline text-center">
                                    <li><a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i> Lihat Keranjang</a>
                                    </li>
                                    <li><a href="{{ url('/checkout') }}"><i class="fa fa-check-square"></i> Beli</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @if (Route::has('login'))
                            @if (Auth::check())
                            <li>
                                <a class="popup-text" href="#menu-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-sign-in"></i>{{Auth::user()->name}}
                                </a>
                            </li>
                            @else
                            <li>
                                <a class="popup-text" href="#login-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-sign-in"></i>Masuk
                                </a>
                            </li>
                            <li>
                                <a class="popup-text" href="#register-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-edit"></i>Daftar
                                </a>
                            </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="main" style="background: #3c7d9e;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flexnav-menu-button" id="flexnav-menu-button" style="margin-top: 10px;">Menu</div>
                <nav>
                    <ul class="nav nav-pills flexnav" id="flexnav" data-breakpoint="800">
                        <li><a href="/">Celebestore</a></li>
                        <li><a href="/pulsa">Pulsa</a></li>
                        <li><a href="/paketdata">Paket Data</a></li>
                        <!-- <li><a href="/paketdata">Paket Data</a></li> -->
                        <li><a href="/gojekgrab">Gojek & Grab</a></li>
                        <!-- <li><a href="/gojekgrab">Gojek & Grab</a></li> -->
                        <li><a href="/pln">Listrik PLN</a></li>
                        <!-- <li><a href="/pln">Listrik PLN</a></li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- LOGIN REGISTER LINKS CONTENT -->
<div id="login-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="fa fa-sign-in dialog-icon"></i>
    <h3>Member Login</h3>
    <h5>Selamat datang kembali. Silahkan login</h5>
    <form class="dialog-form" method="post" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label>E-mail</label>
            <input type="text" placeholder="email@saya.com" class="form-control" name="email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="password anda" class="form-control" name="password">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox">Ingatkan saya
            </label>
        </div>
        <input type="submit" value="Masuk" class="btn btn-primary">
    </form>
    <ul class="dialog-alt-links">
        <li><a class="popup-text" href="#register-dialog" data-effect="mfp-zoom-out">Belum punya akun</a>
        </li>
        <li><a class="popup-text" href="#password-recover-dialog" data-effect="mfp-zoom-out">Lupa password</a>
        </li>
    </ul>
</div>

<div id="register-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="fa fa-edit dialog-icon"></i>
    <h3>Member Register</h3>
    <h5>Silahkan register untuk mendaftarkan akun anda.</h5>
    <form class="dialog-form" method="post" action="{{ url('/register') }}">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Nama anda</label>
            <input type="text" placeholder="nama anda" class="form-control" name="name" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input type="text" placeholder="email@saya.com" class="form-control" name="email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="password anda" name="password" class="form-control">
        </div>
        <div class="form-group">
            <label>Repeat Password</label>
            <input type="password" placeholder="Silahkan ulangi password anda" name="password_confirmation" class="form-control">
        </div>
        <input type="submit" value="Daftar" class="btn btn-primary">
    </form>
    <ul class="dialog-alt-links">
        <li><a class="popup-text" href="#login-dialog" data-effect="mfp-zoom-out">Sudah mempunyai akun</a>
        </li>
    </ul>
</div>

<div id="password-recover-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="icon-retweet dialog-icon"></i>
    <h3>Password Recovery</h3>
    <h5>Anda lupa password? Jangan khawatir kami akan memperbaikinya.</h5>
    <form class="dialog-form" method="post" action="{{ url('/password/email') }}">
        <label>E-mail</label>
        <input type="text" placeholder="email@saya.com" class="span12">
        <input type="submit" value="Request new password" class="btn btn-primary">
    </form>
</div>
<div id="menu-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="icon-retweet dialog-icon fa fa-user"></i>
    <h3>Menu Anda</h3>
    <div class="pull-left">
        <a href="{{url('/home')}}" class="btn btn-default btn-flat">Profil</a>
    </div>
    <div class="pull-right">
        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Keluar
        </a>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
              style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>
<!-- END LOGIN REGISTER LINKS CONTENT -->