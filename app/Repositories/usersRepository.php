<?php

namespace App\Repositories;

use App\Models\users;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class usersRepository
 * @package App\Repositories
 * @version October 14, 2017, 10:00 pm UTC
 *
 * @method users findWithoutFail($id, $columns = ['*'])
 * @method users find($id, $columns = ['*'])
 * @method users first($columns = ['*'])
*/
class usersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'access',
        'username',
        'type_of_identity',
        'identity_number',
        'gender',
        'phone',
        'job',
        'address',
        'districts',
        'city',
        'province',
        'zip_kode',
        'picture',
        'remember_token'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return users::class;
    }
}
