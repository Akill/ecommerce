<!-- Product Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('product_id', 'Produk:') !!}
    @php $items = App\Models\oc_product::has('detail')->where('user_id',Auth::user()->id)->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Gambar:') !!}
    {!! Form::file('image',null,array('id'=>'image','class'=>'form-control')) !!}
</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_order', 'Urutan Pesanan:') !!}
    {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductImages.index') !!}" class="btn btn-default">Batal</a>
</div>
