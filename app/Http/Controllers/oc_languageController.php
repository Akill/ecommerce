<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_languageRequest;
use App\Http\Requests\Updateoc_languageRequest;
use App\Repositories\oc_languageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_languageController extends AppBaseController
{
    /** @var  oc_languageRepository */
    private $ocLanguageRepository;

    public function __construct(oc_languageRepository $ocLanguageRepo)
    {
        $this->ocLanguageRepository = $ocLanguageRepo;
    }

    /**
     * Display a listing of the oc_language.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLanguageRepository->pushCriteria(new RequestCriteria($request));
        $ocLanguages = $this->ocLanguageRepository->all();

        return view('oc_languages.index')
            ->with('ocLanguages', $ocLanguages);
    }

    /**
     * Show the form for creating a new oc_language.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_languages.create');
    }

    /**
     * Store a newly created oc_language in storage.
     *
     * @param Createoc_languageRequest $request
     *
     * @return Response
     */
    public function store(Createoc_languageRequest $request)
    {
        $input = $request->all();

        $ocLanguage = $this->ocLanguageRepository->create($input);

        Flash::success('Oc Language saved successfully.');

        return redirect(route('ocLanguages.index'));
    }

    /**
     * Display the specified oc_language.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            Flash::error('Oc Language not found');

            return redirect(route('ocLanguages.index'));
        }

        return view('oc_languages.show')->with('ocLanguage', $ocLanguage);
    }

    /**
     * Show the form for editing the specified oc_language.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            Flash::error('Oc Language not found');

            return redirect(route('ocLanguages.index'));
        }

        return view('oc_languages.edit')->with('ocLanguage', $ocLanguage);
    }

    /**
     * Update the specified oc_language in storage.
     *
     * @param  int              $id
     * @param Updateoc_languageRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_languageRequest $request)
    {
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            Flash::error('Oc Language not found');

            return redirect(route('ocLanguages.index'));
        }

        $ocLanguage = $this->ocLanguageRepository->update($request->all(), $id);

        Flash::success('Oc Language updated successfully.');

        return redirect(route('ocLanguages.index'));
    }

    /**
     * Remove the specified oc_language from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            Flash::error('Oc Language not found');

            return redirect(route('ocLanguages.index'));
        }

        $this->ocLanguageRepository->delete($id);

        Flash::success('Oc Language deleted successfully.');

        return redirect(route('ocLanguages.index'));
    }
}
