<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_manufacturerRequest;
use App\Http\Requests\Updateoc_manufacturerRequest;
use App\Repositories\oc_manufacturerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_manufacturerController extends AppBaseController
{
    /** @var  oc_manufacturerRepository */
    private $ocManufacturerRepository;

    public function __construct(oc_manufacturerRepository $ocManufacturerRepo)
    {
        $this->ocManufacturerRepository = $ocManufacturerRepo;
    }

    /**
     * Display a listing of the oc_manufacturer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocManufacturerRepository->pushCriteria(new RequestCriteria($request));
        $ocManufacturers = $this->ocManufacturerRepository->all();

        return view('oc_manufacturers.index')
            ->with('ocManufacturers', $ocManufacturers);
    }

    /**
     * Show the form for creating a new oc_manufacturer.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_manufacturers.create');
    }

    /**
     * Store a newly created oc_manufacturer in storage.
     *
     * @param Createoc_manufacturerRequest $request
     *
     * @return Response
     */
    public function store(Createoc_manufacturerRequest $request)
    {
        $input = $request->all();

        $ocManufacturer = $this->ocManufacturerRepository->create($input);

        Flash::success('Oc Manufacturer saved successfully.');

        return redirect(route('ocManufacturers.index'));
    }

    /**
     * Display the specified oc_manufacturer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            Flash::error('Oc Manufacturer not found');

            return redirect(route('ocManufacturers.index'));
        }

        return view('oc_manufacturers.show')->with('ocManufacturer', $ocManufacturer);
    }

    /**
     * Show the form for editing the specified oc_manufacturer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            Flash::error('Oc Manufacturer not found');

            return redirect(route('ocManufacturers.index'));
        }

        return view('oc_manufacturers.edit')->with('ocManufacturer', $ocManufacturer);
    }

    /**
     * Update the specified oc_manufacturer in storage.
     *
     * @param  int              $id
     * @param Updateoc_manufacturerRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_manufacturerRequest $request)
    {
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            Flash::error('Oc Manufacturer not found');

            return redirect(route('ocManufacturers.index'));
        }

        $ocManufacturer = $this->ocManufacturerRepository->update($request->all(), $id);

        Flash::success('Oc Manufacturer updated successfully.');

        return redirect(route('ocManufacturers.index'));
    }

    /**
     * Remove the specified oc_manufacturer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            Flash::error('Oc Manufacturer not found');

            return redirect(route('ocManufacturers.index'));
        }

        $this->ocManufacturerRepository->delete($id);

        Flash::success('Oc Manufacturer deleted successfully.');

        return redirect(route('ocManufacturers.index'));
    }
}
