@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
<div class="content-container">

	<h1 class="page-title animated fadeIn">Daftar Produk</h1>

	<!-- Product navigation -->
	<div class="product-list-navigation animated fadeInRight">
		<div class="product-num">Produk yang anda cari</div>
	</div>
	<!-- End Product navigation -->

	<!-- Product List -->	
	<ol class="product-list animated fadeInLeft">
        @forelse($search as $ProductToCategory)
        @if( strtolower(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['location']) === strtolower(RajaOngkir::Kota()->find($city)['city_name']) )
		<li><!-- Item #1 -->
			<div class="thumb">
				<a href="{{url($ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
					<img src="/image/product/sampul/{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image'] }}" alt="Image Alternative text" title="Green Furniture" />
				</a>
			</div>
			<div class="product-ctn">
				<div class="product-name">
					<a href="{{url('/mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
						{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'],3,'')}}
					</a>
				</div>
				<div class="product-name">
					<p class="product-desciption">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 3,'')}}</p>
				</div>
				<div class="price">
					<span class="price-current">Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}</span>
				</div>
			</div>
		</li><!-- End Item #1 -->
        @endif
        @empty
        @endforelse
	</ol>
	<!-- End Product List -->

	<div class="clear"></div><!-- Use this class (.clear) to clearing float -->

	<!-- Pagination -->
	<!-- 	
	<ul class="pagination">
		<li class="disabled"><a href="#!"><i class="fa fa-angle-double-left"></i></a></li>
		<li class="active"><a href="#!">1</a></li>
		<li class="waves-effect"><a href="#!">2</a></li>
		<li class="waves-effect"><a href="#!">3</a></li>
		<li class="waves-effect"><a href="#!">4</a></li>
		<li class="waves-effect"><a href="#!">5</a></li>
		<li class="waves-effect"><a href="#!"><i class="fa fa-angle-double-right"></i></a></li>
	</ul> 
	-->
	<!-- End Pagination -->
</div>
<!-- END CONTENT CONTAINER -->
@endsection
