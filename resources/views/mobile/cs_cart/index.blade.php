@extends('mobile.layouts')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Keranjang Belanja</h2>
                @include('flash::message')
            </div>
            <!-- Basic Example -->
            <div class="row clearfix">
            @foreach($cart as $carts)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                <a href="{{url('/mobile/'.App\Models\oc_product_to_category::where('product_id',$carts->id)->first()['category_id'].'/'.$carts->id)}}">
                                {{\Illuminate\Support\Str::words($carts->name, 2,'')}}
                                </a>
                                <br>
                                <small><span>Harga</span> Rp. {{number_format($carts->price,2,',','.')}}</small>
                            </h5>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" style="padding-left: 20px;">
                                        <li>
                                        <form role="form" method="POST" action="{!! url('/mobile/cart/remove/'.$carts->rowId) !!}">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-xs waves-effect">
                                                <i class="material-icons">remove_circle</i>
                                                <span>Hapus</span>
                                            </button>
                                        </form>
                                        </li>
                                        <br>
                                        <li>
                                        <form role="form" method="GET" action="{{url('/mobile/'.App\Models\oc_product_to_category::where('product_id',$carts->id)->first()['category_id'].'/'.$carts->id)}}">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-info btn-xs waves-effect">
                                                <i class="material-icons">archive</i>
                                                <span>Detail</span>
                                            </button>
                                        </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <p>
                            <form method="POST" action="{{ url('/mobile/cart/update/'.$carts->rowId) }}">
                                {{ csrf_field() }}
                                <div class="col-md-12">
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line">
                                            <input type="text" class="form-control text-center" value="{{$carts->qty}}" data-rule="quantity" name="cart-quantity" id="cart-quantity">
                                        </div>
                                        <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect">
                                    <i class="material-icons">create</i>
                                    <span>Perbarui</span>
                                </button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            <!-- #END# Basic Example -->
        </div>
        <!-- Spinners -->
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Total
                    </h2>
                </div>
                <div class="body">
                    <!-- <div class="row clearfix"> -->
                    <p>
                        <div class="subtotal">
                            <span class="title">Subtotal</span>
                            <span class="price">{{ Cart::subtotal() }}</span>
                        </div>
                        <hr>
                        <div class="total">
                            <span class="title">Total</span>
                            <span class="price">{{ Cart::total() }}</span>
                        </div>
                        <hr>
                        <form method="POST" action="/mobile/checkout">
                        {{ csrf_field() }}
                        <button type="submit"  class="btn btn-primary btn-block btn-sm btn-squared">
                            Lanjutkan Pembayaran
                        </button>
                        <input type="hidden" name="mm" id="mm" style="display: none;" value="mobile">
                        <input type="hidden" name="checkout" id="checkout" style="display: none;" value="{{ Cart::content() }}">
                        </form>
                    </p>
                    <!-- </div> -->
                </div>
            </div>
        </div>
        <!-- #END# Spinners -->
    </section>
@endsection
