<!-- Oc Category Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('oc_category_id', 'Oc Category Id:') !!}
    @php $items = App\Models\oc_category::has('detail','<',1)->get(); @endphp
    <select class="form-control" id="oc_category_id" name="oc_category_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ $i->id }}</option>
    @endforeach
    @if (Route::is('ocCategoryDescriptions.edit'))
    <option value="{{$ocCategoryDescription->oc_category_id}}">{{ $ocCategoryDescription->oc_category_id }}</option>
    @endif
    </select>
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language_id', 'Language Id:') !!}
    {!! Form::number('language_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_title', 'Meta Title:') !!}
    {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_description', 'Meta Description:') !!}
    {!! Form::text('meta_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Keyword Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_keyword', 'Meta Keyword:') !!}
    {!! Form::text('meta_keyword', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCategoryDescriptions.index') !!}" class="btn btn-default">Cancel</a>
</div>
