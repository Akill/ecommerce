<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $csTransaksiuser->id !!}</p>
</div>

<!-- Request Field -->
<div class="form-group">
    {!! Form::label('request', 'Request:') !!}
    <p>{!! $csTransaksiuser->request !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $csTransaksiuser->status !!}</p>
</div>

<!-- Confirm Field -->
<div class="form-group">
    {!! Form::label('confirm', 'Confirm:') !!}
    <p>{!! $csTransaksiuser->confirm !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $csTransaksiuser->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $csTransaksiuser->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $csTransaksiuser->deleted_at !!}</p>
</div>

