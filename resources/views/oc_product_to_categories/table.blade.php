<table class="table table-responsive" id="ocProductToCategories-table">
    <thead>
        <th>Produk</th>
        <th>Kategori</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
    @foreach($ocProductToCategories as $ocProductToCategory)
        <tr>
            <td>
            {!! App\Models\oc_product_description::where('product_id', $ocProductToCategory->product_id)->first()->name !!}
            </td>
            <td>{!! App\Models\oc_category_description::where('oc_category_id', $ocProductToCategory->category_id)->first()->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductToCategories.destroy', $ocProductToCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductToCategories.show', [$ocProductToCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductToCategories.edit', [$ocProductToCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>