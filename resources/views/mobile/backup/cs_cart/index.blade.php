@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
<div class="content-container">

	<h1 class="page-title">Keranjang Belanja</h1>
	@include('flash::message')
	<!-- Cart Item List -->
	<ol class="cart-item">
		@foreach($cart as $carts)
			<li>
				<div class="thumb">
					<img src="/image/product/sampul/{{ App\Models\oc_product::where('id',$carts->id)->first()['model'].'/'.App\Models\oc_product::where('id',$carts->id)->first()['image'] }}" alt="">
				</div>
				<div class="cart-detail">
					<h3 class="product-name">
						<a href="{{url('/mobile/'.App\Models\oc_product_to_category::where('product_id',$carts->id)->first()['category_id'].'/'.$carts->id)}}">
						{{\Illuminate\Support\Str::words($carts->name, 2,'')}}
						</a>
					</h3>
					<div class="price">
						<span>Harga</span> Rp. {{number_format($carts->price,2,',','.')}}
					</div>
				</div>
				<table>
					<thead>
						<th>Jumlah</th>
						<th>Perbarui</th>
						<th>Hapus</th>
					</thead>
					<tbody>
						<tr>
	                        <form method="POST" action="{{ url('/mobile/cart/update/'.$carts->rowId) }}">
							<td>
		                        {{ csrf_field() }}
								<input type="number" name="cart-quantity" id="cart-quantity" class="cart-quantity" value="{{$carts->qty}}" />
							</td>
							<td>
		                        <button type="submit" class="btn blue block">
		                            <i class="fa fa-edit"></i>
		                        </button>
							</td>
							</form>
							<form role="form" method="POST" action="{!! url('/mobile/cart/remove/'.$carts->rowId) !!}">
							<td>
		                        {{ csrf_field() }}
		                        <button type="submit" class="btn red block">
		                            <i class="fa fa-times"></i> 
		                        </button>
							</td>
		                    </form>
						</tr>
					</tbody>
				</table>
			</li>
		@endforeach
		<li class="cart-nav"><!-- Cart Item Navigation -->
	        <form action="{{ url('/mobile/cart/clear') }}">
	            <button type="submit" class="btn red"><i class="fa fa-trash-o"></i> Bersihkan semua</button>
	        </form>
		</li><!-- End Cart Item Navigation -->
	</ol>
	<!-- End Cart Item List -->

	<!-- Shopping cart action (total amount & button nav) -->
	<div class="cart-action">
		<div class="subtotal">
			<span class="title">Subtotal</span>
			<span class="price">{{ Cart::subtotal() }}</span>
		</div>
		<div class="total">
			<span class="title">Total</span>
			<span class="price">{{ Cart::total() }}</span>
		</div>
        {!!Form::open(['route' => 'cs.mobile.checkout_create'])!!}
        <button type="submit"  class="btn blue block btn-sm btn-squared">
            Lanjutkan Pembayaran
        </button>
        <input type="hidden" name="mm" id="mm" style="display: none;" value="mobile">
        <input type="hidden" name="checkout" id="checkout" style="display: none;" value="{{ Cart::content() }}">
        {!!Form::close()!!}
	</div>
	<!-- End Shopping cart action -->
</div>
<!-- END CONTENT CONTAINER -->
@endsection
