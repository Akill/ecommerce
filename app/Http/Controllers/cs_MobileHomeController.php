<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;

class cs_MobileHomeController extends Controller
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('mobile.cs_home.index',['ocCategories' => $ocCategories]);
    }
}
