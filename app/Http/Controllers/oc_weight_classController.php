<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_weight_classRequest;
use App\Http\Requests\Updateoc_weight_classRequest;
use App\Repositories\oc_weight_classRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_weight_classController extends AppBaseController
{
    /** @var  oc_weight_classRepository */
    private $ocWeightClassRepository;

    public function __construct(oc_weight_classRepository $ocWeightClassRepo)
    {
        $this->ocWeightClassRepository = $ocWeightClassRepo;
    }

    /**
     * Display a listing of the oc_weight_class.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocWeightClassRepository->pushCriteria(new RequestCriteria($request));
        $ocWeightClasses = $this->ocWeightClassRepository->all();

        return view('oc_weight_classes.index')
            ->with('ocWeightClasses', $ocWeightClasses);
    }

    /**
     * Show the form for creating a new oc_weight_class.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_weight_classes.create');
    }

    /**
     * Store a newly created oc_weight_class in storage.
     *
     * @param Createoc_weight_classRequest $request
     *
     * @return Response
     */
    public function store(Createoc_weight_classRequest $request)
    {
        $input = $request->all();

        $ocWeightClass = $this->ocWeightClassRepository->create($input);

        Flash::success('Oc Weight Class saved successfully.');

        return redirect(route('ocWeightClasses.index'));
    }

    /**
     * Display the specified oc_weight_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            Flash::error('Oc Weight Class not found');

            return redirect(route('ocWeightClasses.index'));
        }

        return view('oc_weight_classes.show')->with('ocWeightClass', $ocWeightClass);
    }

    /**
     * Show the form for editing the specified oc_weight_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            Flash::error('Oc Weight Class not found');

            return redirect(route('ocWeightClasses.index'));
        }

        return view('oc_weight_classes.edit')->with('ocWeightClass', $ocWeightClass);
    }

    /**
     * Update the specified oc_weight_class in storage.
     *
     * @param  int              $id
     * @param Updateoc_weight_classRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_weight_classRequest $request)
    {
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            Flash::error('Oc Weight Class not found');

            return redirect(route('ocWeightClasses.index'));
        }

        $ocWeightClass = $this->ocWeightClassRepository->update($request->all(), $id);

        Flash::success('Oc Weight Class updated successfully.');

        return redirect(route('ocWeightClasses.index'));
    }

    /**
     * Remove the specified oc_weight_class from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            Flash::error('Oc Weight Class not found');

            return redirect(route('ocWeightClasses.index'));
        }

        $this->ocWeightClassRepository->delete($id);

        Flash::success('Oc Weight Class deleted successfully.');

        return redirect(route('ocWeightClasses.index'));
    }
}
