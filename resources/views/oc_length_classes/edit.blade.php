@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Kelas (Satuan) Panjang
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocLengthClass, ['route' => ['ocLengthClasses.update', $ocLengthClass->id], 'method' => 'patch']) !!}

                        @include('oc_length_classes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection