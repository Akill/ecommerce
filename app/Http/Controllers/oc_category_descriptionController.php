<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_category_descriptionRequest;
use App\Http\Requests\Updateoc_category_descriptionRequest;
use App\Repositories\oc_category_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\oc_category;
use App\Models\oc_category_description;
use Auth;

class oc_category_descriptionController extends AppBaseController
{
    /** @var  oc_category_descriptionRepository */
    private $ocCategoryDescriptionRepository;

    public function __construct(oc_category_descriptionRepository $ocCategoryDescriptionRepo)
    {
        $this->ocCategoryDescriptionRepository = $ocCategoryDescriptionRepo;
    }

    /**
     * Display a listing of the oc_category_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryDescriptions = $this->ocCategoryDescriptionRepository->all();
        if (!Auth::user()->status) {
            $ocCategoryDescriptions = oc_category_description::where('user_id',Auth::user()->id)->get();
        }
        return view('oc_category_descriptions.index')
            ->with('ocCategoryDescriptions', $ocCategoryDescriptions);
    }

    /**
     * Show the form for creating a new oc_category_description.
     *
     * @return Response
     */
    public function create()
    {
        $items = oc_category::has('detail','<',1)->first();
        if (empty($items)) {
            Flash::error('Category not found');

            return redirect(route('ocCategoryDescriptions.index'));
        }
        return view('oc_category_descriptions.create');
    }

    /**
     * Store a newly created oc_category_description in storage.
     *
     * @param Createoc_category_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_descriptionRequest $request)
    {
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->create($input);

        Flash::success('Oc Category Description saved successfully.');

        return redirect(route('ocCategoryDescriptions.index'));
    }

    /**
     * Display the specified oc_category_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            Flash::error('Oc Category Description not found');

            return redirect(route('ocCategoryDescriptions.index'));
        }

        return view('oc_category_descriptions.show')->with('ocCategoryDescription', $ocCategoryDescription);
    }

    /**
     * Show the form for editing the specified oc_category_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            Flash::error('Oc Category Description not found');

            return redirect(route('ocCategoryDescriptions.index'));
        }

        return view('oc_category_descriptions.edit')->with('ocCategoryDescription', $ocCategoryDescription);
    }

    /**
     * Update the specified oc_category_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_category_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_descriptionRequest $request)
    {
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            Flash::error('Oc Category Description not found');

            return redirect(route('ocCategoryDescriptions.index'));
        }
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->update($input, $id);

        Flash::success('Oc Category Description updated successfully.');

        return redirect(route('ocCategoryDescriptions.index'));
    }

    /**
     * Remove the specified oc_category_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            Flash::error('Oc Category Description not found');

            return redirect(route('ocCategoryDescriptions.index'));
        }

        $this->ocCategoryDescriptionRepository->delete($id);

        Flash::success('Oc Category Description deleted successfully.');

        return redirect(route('ocCategoryDescriptions.index'));
    }
}
