@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1>Pulsa Dan Paket Data</h1>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                <div class="body">
                  <form id="pulsa" method="POST" action="{!! url('mobile/pulsa/buy') !!}">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <div class="form-line">
                          <input class="form-control" type="text" name="nohp" id="nohp" placeholder="Nomor HP Anda">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="form-line">
                          <input class="form-control" type="text" name="kode" id="kode" placeholder="Kode Pulsa Operator S10">
                        </div>
                      </div>
                      <div class="col-md-2">
                      <button type="submit" id="tombol" class="btn btn-block btn-sm btn-primary btn-squared add-to-cart">
                          Beli Sekarang
                      </button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                <div class="body">
                  <h5>Update Harga Pulsa Murah: <br>{{date("d M Y")}}</h5>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>TELKOMSEL MURAH (S)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('S')->result == 'success')
                      @foreach($portalpulsa->cekHarga('S')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>AXIS (AX)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('AX')->result == 'success')
                      @foreach($portalpulsa->cekHarga('AX')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>INDOSAT (I)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('I')->result == 'success')
                      @foreach($portalpulsa->cekHarga('I')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>INDOSAT SMS (IS)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('IS')->result == 'success')
                      @foreach($portalpulsa->cekHarga('IS')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>INDOSAT TRANSFER PULSA (ITR)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('ITR')->result == 'success')
                      @foreach($portalpulsa->cekHarga('ITR')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>SMARTFREN (SM)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('SM')->result == 'success')
                      @foreach($portalpulsa->cekHarga('SM')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>TELKOMSEL SMS (TS)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('TS')->result == 'success')
                      @foreach($portalpulsa->cekHarga('TS')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>TELKOMSEL TRANSFER PULSA (STR)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('STR')->result == 'success')
                      @foreach($portalpulsa->cekHarga('STR')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>TRI (T)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('T')->result == 'success')
                      @foreach($portalpulsa->cekHarga('T')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <h5 class='text-center text-uppercase text-strong'>XL (X)</h5>

                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
                            <th>Kode</th>
                            <th>harga</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($portalpulsa->cekHarga('X')->result == 'success')
                      @foreach($portalpulsa->cekHarga('X')->message as $operator)
                        <tr>
                            <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
                            <td>{{$operator->code}}</td>
                            <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
                            <td>
                            @if($operator->status == 'normal')
                            <span style='color:green'>{{$operator->status}}</span>
                            @else
                            <span style='color:red'>{{$operator->status}}</span>
                            @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                  </table>
                  <br>

                  <p class='text-size-14'>
                  * Harga <a class="text-primary-hover" href="mobile/pulsa">agen pulsa murah all operator</a> 
                  dapat berubah sewaktu-waktu
                  </p>
                  <br>
                  <blockquote>Tag: harga pulsa all operator, harga pulsa elektrik, daftar harga pulsa</blockquote>
                  <br>
                </div>
              </div>
            </div>
        </div>
    </section>

<!-- END CONTENT CONTAINER -->
@endsection
