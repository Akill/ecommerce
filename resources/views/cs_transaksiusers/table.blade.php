<table class="table table-responsive" id="csTransaksiusers-table">
    <thead>
        <tr>
        <th>Request</th>
        <th>Status</th>
        <th>Tanggal Deposite</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csTransaksiusers as $csTransaksiuser)
    @if($csTransaksiuser->user_id == Auth::user()->id)
        <tr>
            <td>Rp. {!! number_format($csTransaksiuser->request,2,',','.') !!}</td>
            <td><span class="label label-success">{!! $csTransaksiuser->status !!}</span></td>
            <td>{!! $csTransaksiuser->created_at !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('csTransaksiusers.show', [$csTransaksiuser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csTransaksiusers.edit', [$csTransaksiuser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                </div>
            </td>
        </tr>
    @endif
    @endforeach
    </tbody>
</table>