@extends('mobile.layouts')

@section('content')
<!-- CONTENT CONTAINER -->
<div class="content-container">
    
    <div class="row row-wrap">
        <div class="col-md-12">
            <div class="product-thumb">
                <form id="pulsa" method="POST" action="{!! url('mobile/pulsa/buy') !!}">
                    {{ csrf_field() }}
                    <div class="col-md-4">
                    <input class="form-control" type="text" name="nohp" id="nohp" placeholder="Nomor HP Anda">
                    </div>
                    <div class="col-md-4">
                    <input class="form-control" type="text" name="kode" id="kode" placeholder="Kode Pulsa Operator S10">
                    </div>
                    <div class="col-md-2">
                    <button type="submit" id="tombol" class="btn btn-block btn-sm btn-primary btn-squared add-to-cart">
                        Beli Sekarang
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <h3 class="text-size-20">Update Harga Pulsa Murah: {{date("d M Y")}}</h3>
    <br>

    <h2 class='text-center text-uppercase text-strong'>TELKOMSEL MURAH (S)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('S')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>AXIS (AX)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('AX')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT (I)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('I')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT SMS (IS)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('IS')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>INDOSAT TRANSFER PULSA (ITR)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('ITR')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>SMARTFREN (SM)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('SM')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>TELKOMSEL SMS (TS)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('TS')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>TELKOMSEL TRANSFER PULSA (STR)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('STR')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>TRI (T)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('T')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <h2 class='text-center text-uppercase text-strong'>XL (X)</h2>

    <table class="table table-striped">
        <thead>
          <tr>
              <!-- <th class="hidden-sm hidden-xs">Produk</th> -->
              <th>Kode</th>
              <th>harga</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($portalpulsa->cekHarga('X')->message as $operator)
          <tr>
              <!-- <td class="hidden-sm hidden-xs">{{$operator->description}}</td> -->
              <td>{{$operator->code}}</td>
              <td>Rp. {{number_format($operator->price + 200,0,',','.')}}</td>
              <td>
              @if($operator->status == 'normal')
              <span style='color:green'>{{$operator->status}}</span>
              @else
              <span style='color:red'>{{$operator->status}}</span>
              @endif
              </td>
          </tr>
        @endforeach
        <tbody>
    </table>
    <br>

    <p class='text-size-14'>
    * Harga <a class="text-primary-hover" href="mobile/pulsa">agen pulsa murah all operator</a> 
    dapat berubah sewaktu-waktu
    </p>
    <br>
    <blockquote>Tag: harga pulsa all operator, harga pulsa elektrik, daftar harga pulsa</blockquote>
    <br>
</div>
<!-- END CONTENT CONTAINER -->
@endsection
