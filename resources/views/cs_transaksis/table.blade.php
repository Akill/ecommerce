<table class="table table-responsive" id="csTransaksis-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Request</th>
        <th>Status</th>
        <th>Confirm</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csTransaksis as $csTransaksi)
        <tr>
            <td>{!! $csTransaksi->user_id !!}</td>
            <td>{!! $csTransaksi->request !!}</td>
            <td>{!! $csTransaksi->status !!}</td>
            <td><img src="/image/saldo/{!! $csTransaksi->user_id !!}/{!! $csTransaksi->confirm !!}" width="100px" height="100px"></td>
            <td>
                {!! Form::open(['route' => ['csTransaksis.destroy', $csTransaksi->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('csTransaksis.show', [$csTransaksi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csTransaksis.edit', [$csTransaksi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>