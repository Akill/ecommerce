@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Gambar Produk
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductImage, ['route' => ['ocProductImages.update', $ocProductImage->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('oc_product_images.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection