<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cs_transaksiuser
 * @package App\Models
 * @version October 29, 2017, 12:39 pm UTC
 *
 * @property integer user_id
 * @property string request
 * @property string status
 * @property string confirm
 */
class cs_transaksiuser extends Model
{
    use SoftDeletes;

    public $table = 'cs_transaksi';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'request',
        'status',
        'confirm'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'request' => 'string',
        'status' => 'string',
        'confirm' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
