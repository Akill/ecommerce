<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;

use App\Models\oc_category;
use RajaOngkir;
use Log;
use Session;

class cs_homeController extends Controller
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
        // $cek = session()->has('prov') ? : null;
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!session()->has('prov')){
            $prov = RajaOngkir::Provinsi()->all();
            session()->push('prov', $prov);
            foreach($prov as $i){
                $kota = RajaOngkir::Kota()->byProvinsi($i['province_id'])->get();
                session()->push('provinsi.'.$i['province'], $kota);
            }
        }
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        // $ocCategories = $this->ocCategoryRepository->all();
        $ocCategories = $this->ocCategoryRepository->paginate(5);

        return view('cs_home.index',['ocCategories' => $ocCategories]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function mobile(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = oc_category::paginate(5);
        if(!session()->has('prov')){
            $prov = RajaOngkir::Provinsi()->all();
            session()->push('prov', $prov);
            foreach($prov as $i){
                $kota = RajaOngkir::Kota()->byProvinsi($i['province_id'])->get();
                session()->push('provinsi.'.$i['province'], $kota);
            }
        }
        return view('mobile.cs_home.index',['ocCategories' => $ocCategories]);
    }
}
