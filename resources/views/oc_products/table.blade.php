<table class="table table-responsive" id="ocProducts-table">
    <thead>
        <th>Model</th>
        <th class="hidden-xs hidden-sm">Lokasi</th>
        <th>Harga</th>
        <th class="hidden-xs hidden-sm">Jumlah</th>
        <th class="hidden-xs hidden-sm">Status Stok</th>
        <th class="hidden-xs hidden-sm">Gambar</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProducts as $ocProduct)
        <tr>
            <td>{!! \Illuminate\Support\Str::words($ocProduct->model, 3,'....') !!}</td>
            <td class="hidden-xs hidden-sm">{!! $ocProduct->location !!}</td>
            <td>{!! $ocProduct->price !!}</td>
            <td class="hidden-xs hidden-sm">{!! $ocProduct->quantity !!}</td>
            <td class="hidden-xs hidden-sm">{!! App\Models\oc_stock_status::find($ocProduct->stock_status_id)->name !!}</td>
            <td class="hidden-xs hidden-sm"><img src="image/product/sampul/{!! $ocProduct->model.'/'.$ocProduct->image !!}" alt="Image" width="80" height="80"></td>
            <td>
                {!! Form::open(['route' => ['ocProducts.destroy', $ocProduct->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProducts.show', [$ocProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProducts.edit', [$ocProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apa anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>