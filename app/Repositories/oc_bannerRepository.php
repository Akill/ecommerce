<?php

namespace App\Repositories;

use App\Models\oc_banner;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_bannerRepository
 * @package App\Repositories
 * @version October 16, 2017, 10:24 pm UTC
 *
 * @method oc_banner findWithoutFail($id, $columns = ['*'])
 * @method oc_banner find($id, $columns = ['*'])
 * @method oc_banner first($columns = ['*'])
*/
class oc_bannerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_banner::class;
    }
}
