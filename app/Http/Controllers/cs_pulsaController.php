<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;
use App\Http\Controllers\portalpulsa;
use App\Models\cs_saldo;
use Auth;

use App\Http\Requests\Createcs_saldoRequest;
use App\Http\Requests\Updatecs_saldoRequest;
use App\Repositories\cs_saldoRepository;

class cs_pulsaController extends Controller
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    /** @var  cs_saldoRepository */
    private $csSaldoRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo, cs_saldoRepository $csSaldoRepo)
    {
        $this->csSaldoRepository = $csSaldoRepo;
        $this->ocCategoryRepository = $ocCategoryRepo;
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function mobile(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('mobile.cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }
    
    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function pln(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('cs_pulsa.pln',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function mobilepln(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('mobile.cs_pulsa.pln',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function paketdata(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('cs_pulsa.paketdata',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function gojekgrab(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return view('cs_pulsa.gojekgrab',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function buy(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $cek = cs_saldo::where('user_id',Auth::user()->id)->first();
        // return $cek->id;
        if (count($cek) > 0) {
            foreach ($portalpulsa->cekHarga($request['kode'])->message as $harga) {
                $ck = $harga->price;
            }

            if ($cek['saldo'] > $ck) {
                $hasil = $portalpulsa->prosesPulsa($request['kode'],$request['nohp'],2);
                Flash::success("Transaksi anda berhasil. ".$hasil->message);
                
                $saldoUpdate = cs_saldo::find($cek->id);
                $saldoUpdate->saldo = ($cek->saldo) - ($ck + 200);
                $saldoUpdate->save();

                // Flash::success("Transaksi anda berhasil.");
                return redirect(route('home'));
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
            else {
                Flash::warning('Saldo anda kurang, silahkan ini saldo anda terlebih dahulu.');
                return back()->withInput();
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
        }
        else {
            Flash::error('Saldo Anda kurang, silahkan ini saldo anda terlebih dahulu.');
            return back()->withInput();
            // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
        }
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function buyMobile(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $cek = cs_saldo::where('user_id',Auth::user()->id)->first();
        // return $cek->id;
        if (count($cek) > 0) {
            foreach ($portalpulsa->cekHarga($request['kode'])->message as $harga) {
                $ck = $harga->price;
            }

            if ($cek['saldo'] > $ck) {
                $hasil = $portalpulsa->prosesPulsa($request['kode'],$request['nohp'],2);
                Flash::success("Transaksi anda berhasil. ".$hasil->message);
                
                $saldoUpdate = cs_saldo::find($cek->id);
                $saldoUpdate->saldo = ($cek->saldo) - ($ck + 200);
                $saldoUpdate->save();

                // Flash::success("Transaksi anda berhasil.");
                return redirect(route('mobile.home'));
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
            else {
                Flash::warning('Saldo anda kurang, silahkan ini saldo anda terlebih dahulu.');
                return back()->withInput();
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
        }
        else {
            Flash::error('Saldo Anda kurang, silahkan ini saldo anda terlebih dahulu.');
            return back()->withInput();
            // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
        }
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function buypln(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $cek = cs_saldo::where('user_id',Auth::user()->id)->first();
        // return $cek->id;
        if (count($cek) > 0) {
            foreach ($portalpulsa->cekHarga($request['kode'])->message as $harga) {
                $ck = $harga->price;
            }

            if ($cek['saldo'] > $ck) {
                $hasil = $portalpulsa->prosesPLN($request['kode'],$request['nohp'],$request['idpel'],1);
                Flash::success("Transaksi anda berhasil. ".$hasil->message);
                
                $saldoUpdate = cs_saldo::find($cek->id);
                $saldoUpdate->saldo = ($cek->saldo) - ($ck + 200);
                $saldoUpdate->save();

                // Flash::success("Transaksi anda berhasil.");
                return redirect(route('home'));
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
            else {
                Flash::warning('Saldo anda kurang, silahkan ini saldo anda terlebih dahulu.');
                return back()->withInput();
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
        }
        else {
            Flash::error('Anda belum memiliki saldo, silahkan ini saldo anda terlebih dahulu.');
            return back()->withInput();
            // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
        }
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function buyplnMobile(Request $request)
    {
        $portalpulsa = new portalpulsa;
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        $cek = cs_saldo::where('user_id',Auth::user()->id)->first();
        // return $cek->id;
        if (count($cek) > 0) {
            foreach ($portalpulsa->cekHarga($request['kode'])->message as $harga) {
                $ck = $harga->price;
            }

            if ($cek['saldo'] > $ck) {
                $hasil = $portalpulsa->prosesPLN($request['kode'],$request['nohp'],$request['idpel'],1);
                Flash::success("Transaksi anda berhasil. ".$hasil->message);
                
                $saldoUpdate = cs_saldo::find($cek->id);
                $saldoUpdate->saldo = ($cek->saldo) - ($ck + 200);
                $saldoUpdate->save();

                // Flash::success("Transaksi anda berhasil.");
                return redirect(route('mobile.home'));
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
            else {
                Flash::warning('Saldo anda kurang, silahkan ini saldo anda terlebih dahulu.');
                return back()->withInput();
                // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
            }
        }
        else {
            Flash::error('Anda belum memiliki saldo, silahkan ini saldo anda terlebih dahulu.');
            return back()->withInput();
            // return view('cs_pulsa.index',['ocCategories' => $ocCategories, 'portalpulsa' => $portalpulsa]);
        }
    }

    /**
     * Update the specified cs_saldo in storage.
     *
     * @param  int              $id
     * @param Updatecs_saldoRequest $request
     *
     * @return Response
     */
    public function updateSaldo($id, Updatecs_saldoRequest $request)
    {
        $csSaldo = $this->csSaldoRepository->findWithoutFail($id);
        $csSaldo = $this->csSaldoRepository->update($request->all(), $id);
    }
}
