@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Deskripsi Gambar Spanduk
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocBannerImageDescriptions.store']) !!}

                        @include('oc_banner_image_descriptions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
