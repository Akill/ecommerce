<!-- Product Image Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProductImage->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Produk Id:') !!}
    <p>{!! App\Models\oc_product_description::find($ocProductImage->product_id)->name !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Gambar:') !!}
    <p>{!! $ocProductImage->image !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Urutan Pesanan:') !!}
    <p>{!! $ocProductImage->sort_order !!}</p>
</div>

