<?php

namespace App\Repositories;

use App\Models\cs_orders;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_ordersRepository
 * @package App\Repositories
 * @version February 13, 2018, 1:49 pm UTC
 *
 * @method cs_orders findWithoutFail($id, $columns = ['*'])
 * @method cs_orders find($id, $columns = ['*'])
 * @method cs_orders first($columns = ['*'])
*/
class cs_ordersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_product',
        'id_users',
        'paket',
        'no_resi',
        'resi_pengiriman',
        'verification'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_orders::class;
    }
}
