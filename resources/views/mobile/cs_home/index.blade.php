@extends('mobile.layouts')

@section('content')
    <section class="content" style="margin-top: 75px;">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- With Captions -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-top: 5px;padding-left: 5px;padding-right: 5px;padding-bottom: 5px;">
                            <div id="carousel-example-generic_2" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                	<?php $i=0; ?>
									@foreach(App\Models\oc_banner::all() as $banner)
                                    <li data-target="#carousel-example-generic_2" data-slide-to="{{$i++}}" class="active"></li>
                                    @endforeach
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
									@foreach(App\Models\oc_banner::all() as $banner)
                                    <div class="item active">
                                        <img src="{{asset('image/banner/'.App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['image'])}}" />
                                        <div class="carousel-caption">
                                            <p>
                                                <h5>{{$banner->name}}</h5>
                                                <a class="btn btn-info waves-effect" href="{{url('/mobile/'.App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['link'])}}">
                                                    {{App\Models\oc_banner_image_description::where('banner_id',$banner->id)->first()['title']}}
                                                </a>
                                            </p>
                                        </div>
                                    </div>
								    @endforeach
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic_2" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic_2" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="icon-button-demo">
                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                            <div class="icon-button-demo">
                                <center>
                                    <button type="button" class="btn btn-default btn-circle-lg waves-effect waves-circle waves-float">
                                        <i class="material-icons">important_devices</i>
                                    </button>
                                    <h6>
                                        Listrik
                                    </h6>
                                </center>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                            <div class="icon-button-demo">
                                <center>
                                    <a href="{{ url('/mobile/pulsa') }}">
                                    <button type="button" class="btn btn-default btn-circle-lg waves-effect waves-circle waves-float">
                                        <i class="material-icons">phonelink_ring</i>
                                    </button>
                                    </a>
                                    <h6>
                                        Pulsa
                                    </h6>
                                </center>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                            <div class="icon-button-demo">
                                <center>
                                    <a href="{{ url('/csTransaksiusers') }}">
                                    <button type="button" class="btn btn-default btn-circle-lg waves-effect waves-circle waves-float">
                                        <i class="material-icons">monetization_on</i>
                                    </button>
                                    </a>
                                    <h6>
                                        Deposite
                                    </h6>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <!-- #END# With Captions -->
            </div>
            @forelse($ocCategories as $category)
	        @if($category->parent_id==0)
            <div class="block-header">
                <h2>{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}</h2>
            </div>
            <!-- Basic Example -->
            <div class="row clearfix">
            @forelse(App\Models\oc_product_to_category::where('category_id',$category->id)->paginate(6) as $ProductToCategory)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;">
                            <div id="aniimated-thumbnialsx" class="list-unstyled row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                                    <a href="{{url('mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}" data-sub-html="Demo Description">
                                        <img height="100" class="img-responsive thumbnail" src="image/product/sampul/{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image'] }}" alt="" style="margin-bottom: 0px;">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 1,'')}}
                                <br>
                                <small>
								{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 2,'')}}
                                </small>
                            </h5>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                <small>
                                Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}
                                </small>
                            </h5>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Data<small>Tidak Ditemukan </small>
                            </h2>
                        </div>
                        <div class="body">
							Silahkan hubungi admin
                        </div>
                    </div>
                </div>
            @endforelse
            </div>
            @endif
            @empty
			    Category Tidak Ada
			@endforelse
            <!-- #END# Basic Example -->
        </div>
    </section>
@endsection
