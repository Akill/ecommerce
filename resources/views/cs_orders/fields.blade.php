@if(Auth::user()->status)
<!-- Id Product Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_product', 'Produk Id:') !!}
    {!! Form::number('id_product', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Users Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_users', 'User Id:') !!}
    {!! Form::number('id_users', null, ['class' => 'form-control']) !!}
</div>

<!-- Paket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paket', 'Paket:') !!}
    {!! Form::text('paket', null, ['class' => 'form-control']) !!}
</div>

<!-- No Resi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_resi', 'No Resi:') !!}
    {!! Form::text('no_resi', null, ['class' => 'form-control']) !!}
</div>

<!-- Resi Pengiriman Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('resi_pengiriman', 'Resi Pengiriman:') !!}
    {!! Form::textarea('resi_pengiriman', null, ['class' => 'form-control']) !!}
</div>

<!-- Verification Field -->
<div class="form-group col-sm-6">
    {!! Form::label('verification', 'Verification:') !!}
    {!! Form::text('verification', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csOrders.index') !!}" class="btn btn-default">Cancel</a>
</div>
@else
<!-- No Resi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_resi', 'No Resi:') !!}
    {!! Form::text('no_resi', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('resi_pengiriman', 'Resi Pengiriman:') !!}
    {!! Form::file('resi_pengiriman',null,array('id'=>'resi_pengiriman','class'=>'form-control')) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csOrders.index') !!}" class="btn btn-default">Batal</a>
</div>
@endif
