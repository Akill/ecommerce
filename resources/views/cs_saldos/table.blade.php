<table class="table table-responsive" id="csSaldos-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Saldo</th>
        <th>Akun</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($csSaldos as $csSaldo)
        <tr>
            <td>{!! $csSaldo->user_id !!}</td>
            <td>{!! $csSaldo->saldo !!}</td>
            <td>{!! $csSaldo->akun !!}</td>
            <td>
                {!! Form::open(['route' => ['csSaldos.destroy', $csSaldo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('csSaldos.show', [$csSaldo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('csSaldos.edit', [$csSaldo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>