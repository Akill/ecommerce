<?php

namespace App\Repositories;

use App\Models\oc_banner_image;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_banner_imageRepository
 * @package App\Repositories
 * @version October 16, 2017, 10:25 pm UTC
 *
 * @method oc_banner_image findWithoutFail($id, $columns = ['*'])
 * @method oc_banner_image find($id, $columns = ['*'])
 * @method oc_banner_image first($columns = ['*'])
*/
class oc_banner_imageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'banner_id',
        'link',
        'image',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_banner_image::class;
    }
}
