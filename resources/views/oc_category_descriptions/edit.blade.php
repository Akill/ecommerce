@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Deskripsi Kategori
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCategoryDescription, ['route' => ['ocCategoryDescriptions.update', $ocCategoryDescription->id], 'method' => 'patch']) !!}

                        @include('oc_category_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection