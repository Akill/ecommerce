@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cs Transfer Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($csTransferDescription, ['route' => ['csTransferDescriptions.update', $csTransferDescription->id], 'method' => 'patch']) !!}

                        @include('cs_transfer_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection