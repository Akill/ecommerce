<!-- TOP AREA -->
<div class="top-area">
    <div class="owl-carousel owl-slider" id="owl-carousel-slider" data-inner-pagination="true" data-white-pagination="true">
    @foreach(App\Models\oc_banner::all() as $banner)
        <div>
            <div class="bg-holder">
                <img src="{{asset('image/banner/'.App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['image'])}}" alt="Image Alternative text" title="Bridge" />
                <div class="vert-center text-white text-center slider-caption">
                    <h2 class="text-uc">{{$banner->name}}</h2>
                    <!-- <div class="countdown countdown-big" data-countdown="Jul 23, 2014 5:30:00"></div> -->
                    <a class="btn btn-lg btn-primary" href="{{url(App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['link'])}}">
                    {{App\Models\oc_banner_image_description::where('banner_id',$banner->id)->first()['title']}}
                    </a>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>
<!-- END TOP AREA -->