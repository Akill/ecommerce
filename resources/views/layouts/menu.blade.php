<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
@php 
$catalog = 
Request::is('ocCategories*') ? 'active' : 
Request::is('ocCategoryDescriptions*') ? 'active' : 
Request::is('ocProducts*') ? 'active' : 
Request::is('ocProductDescriptions*') ? 'active' : 
Request::is('ocManufacturers*') ? 'active' : 
Request::is('ocProductImages*') ? 'active' :
Request::is('ocProductToCategories*') ? 'active' :
Request::is('ocStockStatuses*') ? 'active' :
Request::is('ocWeightClasses*') ? 'active' :
Request::is('ocLengthClasses*') ? 'active' : '';
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-tags"></i> <span>Katalog</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @include('layouts.menu.products.menu')

        @include('layouts.menu.categories.menu')    

        <li class="{{ Request::is('ocManufacturers*') ? 'active' : '' }}">
            <a href="{!! route('ocManufacturers.index') !!}"><span>Manufaktur</span></a>
        </li>

        <li class="{{ Request::is('ocStockStatuses*') ? 'active' : '' }}">
            <a href="{!! route('ocStockStatuses.index') !!}"><span>Status Stok</span></a>
        </li>

        <li class="{{ Request::is('ocWeightClasses*') ? 'active' : '' }}">
            <a href="{!! route('ocWeightClasses.index') !!}"><span>Kelas (Satuan) Berat</span></a>
        </li>

        <li class="{{ Request::is('ocLengthClasses*') ? 'active' : '' }}">
            <a href="{!! route('ocLengthClasses.index') !!}"><span>Kelas (Satuan) Panjang </span></a>
        </li>
    </ul>
</li>
@php 
$catalog = 
Request::is('ocLanguages*') ? 'active' : 
Request::is('ocBanners*') ? 'active' : 
Request::is('ocBannerImages*') ? 'active' : 
Request::is('ocBannerImageDescriptions*') ? 'active' : '';
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-television"></i> <span>Desain</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocLanguages*') ? 'active' : '' }}">
            <a href="{!! route('ocLanguages.index') !!}"><i class="fa fa-language"></i><span>Bahasa</span></a>
        </li>

        <li class="{{ Request::is('ocBanners*') ? 'active' : '' }}">
            <a href="{!! route('ocBanners.index') !!}"><i class="fa fa-th-large"></i><span>Spanduk Celebes</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImages*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImages.index') !!}"><i class="fa fa-file-image-o"></i><span>Gambar Spanduk</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImageDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImageDescriptions.index') !!}"><i class="fa fa-qrcode"></i><span>Deskripsi Gambar Spanduk</span></a>
        </li>
    </ul>
</li>
@php 
$catalog = 
Request::is('ocTaxClasses*') ? 'active' : '';
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-share-alt"></i> <span>Pemasaran</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocTaxClasses*') ? 'active' : '' }}">
            <a href="{!! route('ocTaxClasses.index') !!}"><i class="fa fa-money"></i><span>Pajak</span></a>
        </li>
    </ul>
</li>
@php 
$catalog = 
Request::is('users*') ? 'active' : 
Request::is('csSaldos*') ? 'active' : 
Request::is('csOrders*') ? 'active' : 
Request::is('csTransaksis*') ? 'active' : '';
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-users"></i> <span>Pelanggan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{!! route('users.index') !!}"><i class="fa fa-user-md"></i><span>Pelanggan</span></a>
        </li>

        <li class="{{ Request::is('csOrders*') ? 'active' : '' }}">
            <a href="{!! route('csOrders.index') !!}"><i class="fa fa-user-plus"></i><span>Pesanan Pelanggan</span></a>
        </li>

        <li class="{{ Request::is('csTransaksis*') ? 'active' : '' }}">
            <a href="{!! route('csTransaksis.index') !!}"><i class="fa fa-cart-arrow-down"></i><span>Transaksi</span></a>
        </li>

        <li class="{{ Request::is('csSaldos*') ? 'active' : '' }}">
            <a href="{!! route('csSaldos.index') !!}"><i class="fa fa-credit-card"></i><span>Saldo</span></a>
        </li>
    </ul>
</li>
@php 
$catalog = 
Request::is('csShoppingcarts*') ? 'active' : 
Request::is('csTransferDescriptions*') ? 'active' : '';
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-shopping-cart"></i> <span>Penjualan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('csShoppingcarts*') ? 'active' : '' }}">
            <a href="{!! route('csShoppingcarts.index') !!}"><i class="fa fa-shopping-cart"></i><span>Keranjang Belanja</span></a>
        </li>

        <li class="{{ Request::is('csTransferDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('csTransferDescriptions.index') !!}"><i class="fa fa-table"></i><span>Deskripsi Rekening</span></a>
        </li>
    </ul>
</li>

