<?php

namespace App\Repositories;

use App\Models\oc_length_class;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_length_classRepository
 * @package App\Repositories
 * @version October 14, 2017, 8:30 pm UTC
 *
 * @method oc_length_class findWithoutFail($id, $columns = ['*'])
 * @method oc_length_class find($id, $columns = ['*'])
 * @method oc_length_class first($columns = ['*'])
*/
class oc_length_classRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_length_class::class;
    }
}
