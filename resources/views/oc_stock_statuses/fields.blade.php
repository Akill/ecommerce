<!-- Language Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language_id', 'Bahasa Id:') !!}
    {!! Form::number('language_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocStockStatuses.index') !!}" class="btn btn-default">Batal</a>
</div>
