<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>CELEBESTORE</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('stylemobile/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('stylemobile/css/responsive.css') }}">
	<link rel="shortcut icon" href="{{ asset('stylemobile/images/favicon.png') }}">
	<style type="text/css">
		/*.content-container {
		    padding-top: 70px;
		}
		.top-navbar {
		    height: 100px;
		}
		.navbar-top {
			border-bottom-width: 1px;
		    border-bottom-style: solid;
		    border-bottom-color: #eee;
		}
		.site-title-top {
	        text-align: right;
		    margin: 0 auto;
		    max-width: calc(100% - 100px);
		    padding: 7px 0;
		}*/
	</style>
</head>

<body>

<div id="main">

	<!-- LEFT SIDEBAR -->
	@include('mobile.sidebar')
	<!-- END LEFT SIDEBAR -->

	<!-- RIGHT SIDEBAR -->
	@include('mobile.sidebar2')
	<!-- END RIGHT SIDEBAR -->

	<!-- MAIN PAGE -->
	<div id="page">
		
		<!-- FIXED Top Navbar -->
		@include('mobile.navbar')
		<!-- End FIXED Top Navbar -->

		@yield('content')

		<!-- FOOTER -->
		<!-- <div class="footer"> -->
			
			<!-- Footer main Section -->
			<!-- 
			<div class="footer-main">
				<p>
					<span class="block text-small">
					Terima kasih sudah berkunjung ke toko online kami. <br>
					Temukan produk pilihan mu dengan harga terjangkau.
					</span>
					<i class="fa fa-cc-amex"></i>
					<i class="fa fa-cc-mastercard"></i>
					<i class="fa fa-credit-card"></i>
					<i class="fa fa-cc-paypal"></i>
					<i class="fa fa-cc-visa"></i>
					<i class="fa fa-google-wallet"></i>
					<i class="fa fa-cc-discover"></i>
					<i class="fa fa-cc-jcb"></i>
				</p>
				<p>
					<span class="block text-small">Anda punya masalah? Hubungi Kami</span>
					(0411) 877827 | admin@celebestore.com | <a href="{{ asset('#') }}">Live Chat</a>
				</p>

				<div class="social-footer">
					<a href="{{ asset('#') }}" class="facebook"><i class="fa fa-facebook"></i></a>
					<a href="{{ asset('#') }}" class="twitter"><i class="fa fa-twitter"></i></a>
					<a href="{{ asset('#') }}" class="gplus"><i class="fa fa-google-plus"></i></a>
				</div>
			</div>
			 -->
			<!-- End Footer main Section -->

			<!-- Copyright Section -->
			<!-- 
			<div class="copyright">
				<span class="block">&copy; 2017 Celebes Store</span>
				<div class="navigation">
					<a href="{{ asset('#') }}">Term & Condition</a>
					<a href="{{ asset('#') }}">Privacy Policy</a>
				</div>
			</div> 
			-->
			<!-- End Copyright Section -->

		<!-- </div> -->
		<!-- End FOOTER -->

		<!-- Back to top Link -->
		<div id="to-top" class="main-bg"><i class="fa fa-long-arrow-up"></i></div>

	</div>
	<!-- END MAIN PAGE -->

</div><!-- #main -->

<script type="text/javascript" src="{{ asset('stylemobile/js/jquery-3.1.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('stylemobile/js/materialize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('stylemobile/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('stylemobile/js/jquery.slicknav.js') }}"></script>
<script type="text/javascript" src="{{ asset('stylemobile/js/jquery.swipebox.js') }}"></script>
<script type="text/javascript" src="{{ asset('stylemobile/js/custom.js') }}"></script>

</body>
</html>