<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $csOrders->id !!}</p>
</div>

<!-- Id Product Field -->
<div class="form-group">
    {!! Form::label('id_product', 'Id Product:') !!}
    <p>{!! $csOrders->id_product !!}</p>
</div>

<!-- Id Users Field -->
<div class="form-group">
    {!! Form::label('id_users', 'Id Users:') !!}
    <p>{!! $csOrders->id_users !!}</p>
</div>

<!-- Paket Field -->
<div class="form-group">
    {!! Form::label('paket', 'Paket:') !!}
    <p>{!! $csOrders->paket !!}</p>
</div>

<!-- No Resi Field -->
<div class="form-group">
    {!! Form::label('no_resi', 'No Resi:') !!}
    <p>{!! $csOrders->no_resi !!}</p>
</div>

<!-- Resi Pengiriman Field -->
<div class="form-group">
    {!! Form::label('resi_pengiriman', 'Resi Pengiriman:') !!}
    <p>{!! $csOrders->resi_pengiriman !!}</p>
</div>

<!-- Verification Field -->
<div class="form-group">
    {!! Form::label('verification', 'Verification:') !!}
    <p>{!! $csOrders->verification !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $csOrders->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $csOrders->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $csOrders->deleted_at !!}</p>
</div>

