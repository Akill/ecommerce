<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Produk:') !!}
    @php $items = App\Models\oc_product::has('detail','<',1)->where('user_id',Auth::user()->id)->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ $i->model }}</option>
    @endforeach
    @if (Route::is('ocProductDescriptions.edit'))
    <option value="{{$ocProductDescription->id}}">{{ $ocProductDescription->model }}</option>
    @endif
    </select>
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language_id', 'Id Bahasa:') !!}
    @php $items = App\Models\oc_language::all(['name','id']); @endphp
    <select class="form-control" id="language_id" name="language_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ $i->name }}</option>
    @endforeach
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Deskripsi:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Tag Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tag', 'Tag:') !!}
    {!! Form::textarea('tag', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_title', 'Judul Halaman:') !!}
    {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_description', 'Halaman Deskripsi:') !!}
    {!! Form::text('meta_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Keyword Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_keyword', 'Kata Kunci Halaman:') !!}
    {!! Form::text('meta_keyword', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductDescriptions.index') !!}" class="btn btn-default">Batal</a>
</div>
