<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cs_orders
 * @package App\Models
 * @version February 13, 2018, 1:49 pm UTC
 *
 * @property integer id_product
 * @property integer id_users
 * @property string paket
 * @property string no_resi
 * @property string resi_pengiriman
 * @property string verification
 */
class cs_orders extends Model
{
    use SoftDeletes;

    public $table = 'cs_orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_product',
        'id_users',
        'paket',
        'no_resi',
        'resi_pengiriman',
        'verification'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_product' => 'integer',
        'id_users' => 'integer',
        'paket' => 'string',
        'no_resi' => 'string',
        'resi_pengiriman' => 'string',
        'verification' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
