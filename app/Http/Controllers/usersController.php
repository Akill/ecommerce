<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateusersRequest;
use App\Http\Requests\UpdateusersRequest;
use App\Repositories\usersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class usersController extends AppBaseController
{
    /** @var  usersRepository */
    private $usersRepository;

    public function __construct(usersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the users.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->status) {
            $this->usersRepository->pushCriteria(new RequestCriteria($request));
            $users = $this->usersRepository->all();

            return view('users.index')
                ->with('users', $users);
        }
        Flash::success('Data Berhasil diperbarui');

        return redirect(route('home'));
    }

    /**
     * Show the form for creating a new users.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->status) {
            return view('users.create');
        }
        Flash::error('Users not found');

        return redirect(route('home'));
    }

    /**
     * Store a newly created users in storage.
     *
     * @param CreateusersRequest $request
     *
     * @return Response
     */
    public function store(CreateusersRequest $request)
    {
        if (Auth::user()->status) {

            $input = $request->all();

            $users = $this->usersRepository->create($input);

            Flash::success('Users saved successfully.');

            return redirect(route('users.index'));
        }
        Flash::error('Users not found');

        return redirect(route('home'));
    }

    /**
     * Display the specified users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        if (Auth::user()->id != $id && !Auth::user()->status) {
            Flash::error('Users not found');

            return redirect(route('home'));
        }

        return view('users.edit')->with('users', $users);
    }

    /**
     * Update the specified users in storage.
     *
     * @param  int              $id
     * @param UpdateusersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateusersRequest $request)
    {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        $users = $this->usersRepository->update($request->all(), $id);

        Flash::success('Users updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->status) {

            $users = $this->usersRepository->findWithoutFail($id);

            if (empty($users)) {
                Flash::error('Users not found');

                return redirect(route('users.index'));
            }

            $this->usersRepository->delete($id);

            Flash::success('Users deleted successfully.');

            return redirect(route('users.index'));
        }
        Flash::error('Users not found');

        return redirect(route('home'));
    }
}
