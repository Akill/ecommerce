<table class="table table-responsive" id="ocWeightClasses-table">
    <thead>
        <tr>
            <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocWeightClasses as $ocWeightClass)
        <tr>
            <td>{!! $ocWeightClass->value !!}</td>
            <td>
                {!! Form::open(['route' => ['ocWeightClasses.destroy', $ocWeightClass->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocWeightClasses.show', [$ocWeightClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocWeightClasses.edit', [$ocWeightClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>