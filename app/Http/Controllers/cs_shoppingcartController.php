<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_shoppingcartRequest;
use App\Http\Requests\Updatecs_shoppingcartRequest;
use App\Repositories\cs_shoppingcartRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\cs_shoppingcart;
use PDF;

class cs_shoppingcartController extends AppBaseController
{
    /** @var  cs_shoppingcartRepository */
    private $csShoppingcartRepository;

    public function __construct(cs_shoppingcartRepository $csShoppingcartRepo)
    {
        $this->csShoppingcartRepository = $csShoppingcartRepo;
    }

    /**
     * Display a listing of the cs_shoppingcart.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csShoppingcartRepository->pushCriteria(new RequestCriteria($request));
        $csShoppingcarts = $this->csShoppingcartRepository->all();
        if (!Auth::user()->status) {
            $csShoppingcarts = cs_shoppingcart::where('user_id',Auth::user()->id)->get();
        }
        return view('cs_shoppingcarts.index')
            ->with('csShoppingcarts', $csShoppingcarts);
    }

    /**
     * Show the form for creating a new cs_shoppingcart.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_shoppingcarts.create');
    }

    /**
     * Store a newly created cs_shoppingcart in storage.
     *
     * @param Createcs_shoppingcartRequest $request
     *
     * @return Response
     */
    public function store(Createcs_shoppingcartRequest $request)
    {
        $input = $request->all();

        $csShoppingcart = $this->csShoppingcartRepository->create($input);

        Flash::success('Cs Shoppingcart saved successfully.');

        return redirect(route('csShoppingcarts.index'));
    }

    /**
     * Display the specified cs_shoppingcart.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csShoppingcart = $this->csShoppingcartRepository->findWithoutFail($id);

        if (empty($csShoppingcart)) {
            Flash::error('Cs Shoppingcart not found');

            return redirect(route('csShoppingcarts.index'));
        }

        return view('cs_shoppingcarts.show')->with('csShoppingcart', $csShoppingcart);
    }

    /**
     * Show the form for editing the specified cs_shoppingcart.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csShoppingcart = $this->csShoppingcartRepository->findWithoutFail($id);

        if (empty($csShoppingcart)) {
            Flash::error('Cs Shoppingcart not found');

            return redirect(route('csShoppingcarts.index'));
        }

        return view('cs_shoppingcarts.edit')->with('csShoppingcart', $csShoppingcart);
    }

    /**
     * Update the specified cs_shoppingcart in storage.
     *
     * @param  int              $id
     * @param Updatecs_shoppingcartRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_shoppingcartRequest $request)
    {
        $csShoppingcart = $this->csShoppingcartRepository->findWithoutFail($id);

        if (empty($csShoppingcart)) {
            Flash::error('Cs Shoppingcart not found');

            return redirect(route('csShoppingcarts.index'));
        }
        if ($request->file('bukti')) {
            $file       = $request->file('bukti');
            $fileName   = $file->getClientOriginalName();
            $request->file('bukti')->move("image/shoppingcart/".Auth::user()->id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['bukti'] = $fileName;

        $csShoppingcart = $this->csShoppingcartRepository->update($input, $id);

        Flash::success('Cs Shoppingcart updated successfully.');

        return redirect(route('csShoppingcarts.index'));
    }

    /**
     * Remove the specified cs_shoppingcart from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csShoppingcart = $this->csShoppingcartRepository->findWithoutFail($id);

        if (empty($csShoppingcart)) {
            Flash::error('Cs Shoppingcart not found');

            return redirect(route('csShoppingcarts.index'));
        }

        $this->csShoppingcartRepository->delete($id);

        Flash::success('Cs Shoppingcart deleted successfully.');

        return redirect(route('csShoppingcarts.index'));
    }

    public function invoice($id)
    {
        if (!Auth::user()->status) {
            $csShoppingcarts = cs_shoppingcart::find($id);
        }

        $nomor = $id;
        $pdf = PDF::loadView('invoice',compact('nomor','csShoppingcarts'));
        return $pdf->stream('invoice.pdf');
    }
}
