<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Models\oc_category;

class cs_cartController extends Controller
{
    /**
     * Add to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function add(Request $request)
    {
        session(['test' => 'cart']);
        Cart::add(array('id' => $request->input('id'), 
                            'name' => $request->input('name'), 
                            'tax' => 0, 
                            'qty' => $request->input('qty'), 
                            'price' => $request->input('price'),
                            )
                        );


        // return Cart::content();
        return redirect()->route('cs.cart');
    }

    /**
     * Add to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function cart()
    {
        return view('cs_cart.index',['cart' => Cart::content()]);
    }

    /**
     * Remove id to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function remove($id)
    {
        Cart::remove($id);
        return redirect()->route('cs.cart');
    }

    /**
     * Clear all to cart.
     *
     *
     * @return Content cart
     */
    public function clear()
    {
        Cart::destroy();
        return redirect()->route('cs.cart');
    }

    /**
     * Clear all to cart.
     *
     *
     * @return Content cart
     */
    public function update(Request $request, $id)
    {
        // return $id;
        Cart::update($id, ['qty' => $request->input('cart-quantity')]);
        return redirect()->route('cs.cart');
    }

    /**
     * Add to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function addm(Request $request)
    {
        session(['test' => 'mobile/cart']);
        Cart::add(array('id' => $request->input('id'), 
                            'name' => $request->input('name'), 
                            'tax' => 0, 
                            'qty' => $request->input('qty'), 
                            'price' => $request->input('price'),
                            )
                        );


        // return Cart::content();
        return redirect()->route('cs.mobile.cart');
    }

    /**
     * Add to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function cartm()
    {
        $ocCategory = oc_category::all();
        return view('mobile.cs_cart.index',['cart' => Cart::content(), 'ocCategories' => $ocCategory]);
    }

    /**
     * Remove id to cart.
     *
     * @param  int $id
     *
     * @return Content cart
     */
    public function removem($id)
    {
        Cart::remove($id);
        return redirect()->route('cs.mobile.cart');
    }

    /**
     * Clear all to cart.
     *
     *
     * @return Content cart
     */
    public function clearm()
    {
        Cart::destroy();
        return redirect()->route('cs.mobile.cart');
    }

    /**
     * Clear all to cart.
     *
     *
     * @return Content cart
     */
    public function updatem(Request $request, $id)
    {
        // return $id;
        Cart::update($id, ['qty' => $request->input('cart-quantity')]);
        return redirect()->route('cs.mobile.cart');
    }
}
