<?php

namespace App\Repositories;

use App\Models\oc_banner_image_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_banner_image_descriptionRepository
 * @package App\Repositories
 * @version October 16, 2017, 10:26 pm UTC
 *
 * @method oc_banner_image_description findWithoutFail($id, $columns = ['*'])
 * @method oc_banner_image_description find($id, $columns = ['*'])
 * @method oc_banner_image_description first($columns = ['*'])
*/
class oc_banner_image_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'banner_id',
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_banner_image_description::class;
    }
}
