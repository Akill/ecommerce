<?php

namespace App\Repositories;

use App\Models\cs_shoppingcart;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_shoppingcartRepository
 * @package App\Repositories
 * @version October 15, 2017, 1:32 pm UTC
 *
 * @method cs_shoppingcart findWithoutFail($id, $columns = ['*'])
 * @method cs_shoppingcart find($id, $columns = ['*'])
 * @method cs_shoppingcart first($columns = ['*'])
*/
class cs_shoppingcartRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'address',
        'districts',
        'city',
        'province',
        'zip_kode',
        'bukti',
        'content',
        'paket',
        'status',
        'end_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_shoppingcart::class;
    }
}
