@extends('mobile.layouts')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Image Gallery -->
            <div class="block-header">
                <h2>
                    {{ \Illuminate\Support\Str::words($ocProductDescription->name, 3,'') }}
                </h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;">
                            <div id="aniimated-thumbnialsx" class="list-unstyled row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                                    <a href="#" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="{{ url('image/product/sampul/'.$ocProduct->model.'/'.$ocProduct->image) }}" alt="" style="margin-bottom: 0px;">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                Rp. {{number_format($ocProduct->price,2,',','.')}}
                                <br>
                                <small>
                                {{App\Models\oc_stock_status::where('id',$ocProduct->stock_status_id)->first()['name']}}
                                </small>
                            </h5>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <center>
                            <form id="idForm" method="POST" action="{!! url('mobile/cart/add') !!}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $ocProduct->id }}">
                                <input type="hidden" name="name" value="{{ App\Models\oc_product_description::where('product_id',$ocProduct->id)->first()['name'] }}">
                                <input type="hidden" name="price" value="{{ App\Models\oc_product::where('id',$ocProduct->id)->first()['price'] }}">
                                <input type="hidden" name="qty" value="1">
                                <div class="row">
                                    <button type="submit" class="btn btn-primary waves-effect">
                                        <i class="material-icons">add_shopping_cart</i>
                                        <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header">
                            <h2>
                            Detail Gambar
                                {!! App\Models\oc_category_description::where('oc_category_id',App\Models\oc_product_to_category::where('product_id',$ocProductDescription->id)->first()['category_id'])->first()['name'] !!}
                                <small>{{ \Illuminate\Support\Str::words($ocProductDescription->name, 3,'') }}</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                @php $i=1 @endphp
                                @foreach($ocProductImage as $image)
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;padding-left: 0px;">
                                    <a href="{{url('/image/product/'.$image->product_id.'/'.$image->image)}}" data-sub-html="Gambar {{$i++}}">
                                        <img class="img-responsive thumbnail" src="{{url('/image/product/'.$image->product_id.'/'.$image->image)}}">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ \Illuminate\Support\Str::words($ocProductDescription->name, 3,'') }}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;padding-left: 0px;">
                                    <b>Detail Produk</b>
                                    <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading" role="tab" id="headingOne_1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="true" aria-controls="collapseOne_1">
                                                        Deskripsi Produk
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1">
                                                <div class="panel-body">
                                                    {!! $ocProductDescription->description !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading" role="tab" id="headingTwo_1">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseTwo_1" aria-expanded="false"
                                                       aria-controls="collapseTwo_1">
                                                        Spesifikasi
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
                                                <div class="panel-body">
                                                    <table class="table table-striped mb0">
                                                        <tbody>
                                                            <tr>
                                                                <td>Weight</td>
                                                                <td>{{$ocProduct->weight}} {{App\Models\oc_weight_class::find($ocProduct->weight_class_id)['value']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Dimentions</td>
                                                                <td>{{$ocProduct->length}} x {{$ocProduct->width}} x {{$ocProduct->height}} cm</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Location</td>
                                                                <td>{{$ocProduct->location}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="block-header">
                <h1>Daftar Produk</h1>
            </div>
            <div class="block-header">
                <h2>Mungkin yang anda inginkan</h2>
            </div>
            <!-- Basic Example -->
            <div class="row clearfix">
            @forelse($ocProductToCategory as $ProductToCategory)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="body" style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;">
                            <div id="aniimated-thumbnialsx" class="list-unstyled row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                                    <a href="{{url('/mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="{{ asset('image/product/sampul/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image']) }}" alt="" style="margin-bottom: 0px;">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 2,'')}}
                                <br>
                                <small>
                                {{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 3,'')}}
                                </small>
                            </h5>
                        </div>
                        <div class="header" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;padding-right: 5px;">
                            <h5>
                                Rp. {{number_format(App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['price'],2,',','.')}}
                            </h5>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data<small>Tidak Ditemukan </small>
                            </h2>
                        </div>
                        <div class="body">
                            Silahkan hubungi admin
                        </div>
                    </div>
                </div>
            @endforelse
            </div>
            <!-- #END# Basic Example -->
        </div>
    </section>
@endsection
