@if(Auth::user()->status)
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Districts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('districts', 'Districts:') !!}
    {!! Form::text('districts', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province', 'Province:') !!}
    {!! Form::text('province', null, ['class' => 'form-control']) !!}
</div>

<!-- Zip Kode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip_kode', 'Zip Kode:') !!}
    {!! Form::text('zip_kode', null, ['class' => 'form-control']) !!}
</div>

<!-- Bukti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bukti', 'Bukti:') !!}
    {!! Form::text('bukti', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Paket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paket', 'Paket:') !!}
    {!! Form::text('paket', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csShoppingcarts.index') !!}" class="btn btn-default">Batal</a>
</div>
@else
<!-- Bukti Field -->
<div class="form-group col-sm-12">
    {!! Form::label('bukti', 'Bukti:') !!}
    {!! Form::file('bukti',null,array('id'=>'bukti','class'=>'form-control')) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csShoppingcarts.index') !!}" class="btn btn-default">Batal</a>
</div>
@endif