@extends('mobile.layouts')

@section('content')
<!-- Featured Slider -->
<div class="featured-slider animated fadeInRight">
	@foreach(App\Models\oc_banner::all() as $banner)
	<div class="featured-item"><!-- 1. Featured Slider Item -->
		<div class="thumb">
			<img src="{{asset('image/banner/'.App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['image'])}}" alt="">
		</div>
		<div class="overlay"></div>
		<div class="caption">
			<p>{{$banner->name}}</p>
			<a class="featured-btn blue" href="{{url(App\Models\oc_banner_image::where('banner_id',$banner->id)->first()['link'])}}">{{App\Models\oc_banner_image_description::where('banner_id',$banner->id)->first()['title']}} <i class="fa fa-angle-double-right"></i></a>
		</div>
	</div><!-- 1. End Featured Slider Item -->
    @endforeach
</div>
<!-- End Featured Slider -->

<!-- CONTENT CONTAINER -->
<div class="content-container animated fadeInUp">
    @forelse($ocCategories as $category)
	<!-- Category Section -->	
        @if($category->parent_id==0)
	<div class="page-block margin-bottom">

		<h2 class="block-title">
			<span>{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}</span><!-- <span> tag to make blue border on this text only -->
			<a href="{{ $category->urlmobile }}" class="list-all">
				<i class="fa fa-th-list"></i>
			</a>
		</h2>

		<!-- Category Listing -->
		<ol class="category-list">
            @forelse(App\Models\oc_product_to_category::where('category_id',$category->id)->paginate(6) as $ProductToCategory)
			<li><!-- Category list item #1 -->
				<div class="thumb">
					<a href="{{url('mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">
						<img src="image/product/sampul/{{ App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$ProductToCategory->product_id)->first()['image'] }}" alt="">
					</a>
				</div>
				<div class="category-ctn">
					<div class="cat-name">
						<a href="{{url('/mobile/'.$ProductToCategory->category_id.'/'.$ProductToCategory->product_id)}}">{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['name'], 3,' ....')}}</a>
					</div>
					<div class="cat-desc">
						{{\Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$ProductToCategory->product_id)->first()['description'], 10,'....')}}
					</div>
				</div>
			</li><!-- End Category list item #1 -->
            @empty
            <li><!-- Category list item #1 -->
				<div class="thumb">
					<a href="{{ asset('#') }}">
						<img src="{{ asset('stylemobile/images/240x240.png') }}" alt="">
					</a>
				</div>
				<div class="category-ctn">
					<div class="cat-name">
						<a href="{{ asset('#') }}">Data tidak ditemukkan</a>
					</div>
					<div class="cat-desc">
						Silahkan hubungi admin
					</div>
				</div>
			</li><!-- End Category list item #1 -->
            @endforelse
		</ol>
		<div class="clear"></div><!-- Use this class (.clear) to clearing float -->
	</div>
		@endif
	@empty
	    Category Tidak Ada
	<!-- End Category Section -->
	@endforelse
</div>
<!-- END CONTENT CONTAINER -->
@endsection
