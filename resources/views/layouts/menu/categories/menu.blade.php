@php 
$active = Request::is('ocCategories*') ? 'active' : Request::is('ocCategoryDescriptions*') ? 'active' : ''; 
@endphp

<li class="{{ $active }} treeview menu-open">
    <a href="#">
        <span>Kategori</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocCategories.index') !!}"><i class="fa fa-list"></i><span>Kategori</span></a>
        </li>
        <li class="{{ Request::is('ocCategoryDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryDescriptions.index') !!}"><i class="fa fa-files-o"></i><span>Deskripsi Kategori</span></a>
        </li>
    </ul>
</li>