<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_bannerRequest;
use App\Http\Requests\Updateoc_bannerRequest;
use App\Repositories\oc_bannerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_bannerController extends AppBaseController
{
    /** @var  oc_bannerRepository */
    private $ocBannerRepository;

    public function __construct(oc_bannerRepository $ocBannerRepo)
    {
        $this->ocBannerRepository = $ocBannerRepo;
    }

    /**
     * Display a listing of the oc_banner.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerRepository->pushCriteria(new RequestCriteria($request));
        $ocBanners = $this->ocBannerRepository->all();

        return view('oc_banners.index')
            ->with('ocBanners', $ocBanners);
    }

    /**
     * Show the form for creating a new oc_banner.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_banners.create');
    }

    /**
     * Store a newly created oc_banner in storage.
     *
     * @param Createoc_bannerRequest $request
     *
     * @return Response
     */
    public function store(Createoc_bannerRequest $request)
    {
        $input = $request->all();

        $ocBanner = $this->ocBannerRepository->create($input);

        Flash::success('Oc Banner saved successfully.');

        return redirect(route('ocBanners.index'));
    }

    /**
     * Display the specified oc_banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            Flash::error('Oc Banner not found');

            return redirect(route('ocBanners.index'));
        }

        return view('oc_banners.show')->with('ocBanner', $ocBanner);
    }

    /**
     * Show the form for editing the specified oc_banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            Flash::error('Oc Banner not found');

            return redirect(route('ocBanners.index'));
        }

        return view('oc_banners.edit')->with('ocBanner', $ocBanner);
    }

    /**
     * Update the specified oc_banner in storage.
     *
     * @param  int              $id
     * @param Updateoc_bannerRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_bannerRequest $request)
    {
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            Flash::error('Oc Banner not found');

            return redirect(route('ocBanners.index'));
        }

        $ocBanner = $this->ocBannerRepository->update($request->all(), $id);

        Flash::success('Oc Banner updated successfully.');

        return redirect(route('ocBanners.index'));
    }

    /**
     * Remove the specified oc_banner from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            Flash::error('Oc Banner not found');

            return redirect(route('ocBanners.index'));
        }

        $this->ocBannerRepository->delete($id);

        Flash::success('Oc Banner deleted successfully.');

        return redirect(route('ocBanners.index'));
    }
}
