<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProduct->id !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $ocProduct->model !!}</p>
</div>

<!-- Sku Field -->
<div class="form-group">
    {!! Form::label('sku', 'SKU:') !!}
    <p>{!! $ocProduct->sku !!}</p>
</div>

<!-- Upc Field -->
<div class="form-group">
    {!! Form::label('upc', 'UPC:') !!}
    <p>{!! $ocProduct->upc !!}</p>
</div>

<!-- Ean Field -->
<div class="form-group">
    {!! Form::label('ean', 'EAN:') !!}
    <p>{!! $ocProduct->ean !!}</p>
</div>

<!-- Jan Field -->
<div class="form-group">
    {!! Form::label('jan', 'JAN:') !!}
    <p>{!! $ocProduct->jan !!}</p>
</div>

<!-- Isbn Field -->
<div class="form-group">
    {!! Form::label('isbn', 'ISBN:') !!}
    <p>{!! $ocProduct->isbn !!}</p>
</div>

<!-- Mpn Field -->
<div class="form-group">
    {!! Form::label('mpn', 'MPN:') !!}
    <p>{!! $ocProduct->mpn !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Lokasi:') !!}
    <p>{!! $ocProduct->location !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Jumlah:') !!}
    <p>{!! $ocProduct->quantity !!}</p>
</div>

<!-- Stock Status Id Field -->
<div class="form-group">
    {!! Form::label('stock_status_id', 'Status Stok Id:') !!}
    <p>{!! $ocProduct->stock_status_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Gambar:') !!}
    <p>{!! $ocProduct->image !!}</p>
</div>

<!-- Manufacturer Id Field -->
<div class="form-group">
    {!! Form::label('manufacturer_id', 'Manufaktur Id:') !!}
    <p>{!! $ocProduct->manufacturer_id !!}</p>
</div>

<!-- Shipping Field -->
<div class="form-group">
    {!! Form::label('shipping', 'Pengiriman:') !!}
    <p>{!! $ocProduct->shipping !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Harga:') !!}
    <p>{!! $ocProduct->price !!}</p>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Poin:') !!}
    <p>{!! $ocProduct->points !!}</p>
</div>

<!-- Tax Class Id Field -->
<div class="form-group">
    {!! Form::label('tax_class_id', 'Kelas Pajak Id:') !!}
    <p>{!! $ocProduct->tax_class_id !!}</p>
</div>

<!-- Date Available Field -->
<div class="form-group">
    {!! Form::label('date_available', 'Tanggal Berlaku:') !!}
    <p>{!! $ocProduct->date_available !!}</p>
</div>

<!-- Weight Field -->
<div class="form-group">
    {!! Form::label('weight', 'Berat:') !!}
    <p>{!! $ocProduct->weight !!}</p>
</div>

<!-- Weight Class Id Field -->
<div class="form-group">
    {!! Form::label('weight_class_id', 'Kelas Berat Id:') !!}
    <p>{!! $ocProduct->weight_class_id !!}</p>
</div>

<!-- Length Field -->
<div class="form-group">
    {!! Form::label('length', 'Panjang:') !!}
    <p>{!! $ocProduct->length !!}</p>
</div>

<!-- Width Field -->
<div class="form-group">
    {!! Form::label('width', 'Lebar:') !!}
    <p>{!! $ocProduct->width !!}</p>
</div>

<!-- Height Field -->
<div class="form-group">
    {!! Form::label('height', 'Tinggi:') !!}
    <p>{!! $ocProduct->height !!}</p>
</div>

<!-- Length Class Id Field -->
<div class="form-group">
    {!! Form::label('length_class_id', 'Kelas Tinggi Id:') !!}
    <p>{!! $ocProduct->length_class_id !!}</p>
</div>

<!-- Subtract Field -->
<div class="form-group">
    {!! Form::label('subtract', 'Mengurangi:') !!}
    <p>{!! $ocProduct->subtract !!}</p>
</div>

<!-- Minimum Field -->
<div class="form-group">
    {!! Form::label('minimum', 'Minimum:') !!}
    <p>{!! $ocProduct->minimum !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Urutan Order:') !!}
    <p>{!! $ocProduct->sort_order !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocProduct->status !!}</p>
</div>

<!-- Viewed Field -->
<div class="form-group">
    {!! Form::label('viewed', 'Tampilan:') !!}
    <p>{!! $ocProduct->viewed !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Tanggal Ditambah:') !!}
    <p>{!! $ocProduct->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Tanggal Dimodifikasi:') !!}
    <p>{!! $ocProduct->date_modified !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Tanggal Dihapus:') !!}
    <p>{!! $ocProduct->deleted_at !!}</p>
</div>

