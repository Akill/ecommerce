<!-- Product Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('product_id', 'Produk:') !!}
    @php $items = App\Models\oc_product::where('user_id',Auth::user()->id)->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::where('product_id',$i->id)->first()['name'] }}</option>
    @endforeach
    </select>
</div>
<!-- Oc Category Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('category_id', 'Kategori:') !!}
    @php $items = App\Models\oc_category::all(); @endphp
    <select class="form-control" id="category_id" name="category_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_category_description::where('oc_category_id',$i->id)->first()['name'] }}</option>
    @endforeach
    </select>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductToCategories.index') !!}" class="btn btn-default">Batal</a>
</div>
