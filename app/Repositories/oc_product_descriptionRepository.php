<?php

namespace App\Repositories;

use App\Models\oc_product_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_descriptionRepository
 * @package App\Repositories
 * @version October 14, 2017, 9:32 pm UTC
 *
 * @method oc_product_description findWithoutFail($id, $columns = ['*'])
 * @method oc_product_description find($id, $columns = ['*'])
 * @method oc_product_description first($columns = ['*'])
*/
class oc_product_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'language_id',
        'name',
        'description',
        'tag',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_description::class;
    }
}
