<footer class="main" id="main-footer">
    <div class="footer-top-area">
        <div class="container">
            <div class="row row-wrap">
                <div class="col-md-3">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
                        <a href="/">
                            <img src="{{asset('cs.png')}}" alt="logo" title="logo">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3"></div>
                    <div class="col-md-12" >
                        <ul class="list list-social">
                            <li>
                                <a class="fa fa-facebook box-icon" href="#" data-toggle="tooltip" title="Facebook"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter box-icon" href="#" data-toggle="tooltip" title="Twitter"></a>
                            </li>
                            <li>
                                <a class="fa fa-flickr box-icon" href="#" data-toggle="tooltip" title="Flickr"></a>
                            </li>
                            <li>
                                <a class="fa fa-linkedin box-icon" href="#" data-toggle="tooltip" title="LinkedIn"></a>
                            </li>
                            <li>
                                <a class="fa fa-tumblr box-icon" href="#" data-toggle="tooltip" title="Tumblr"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <h4>Daftarkan email anda</h4>
                    <div class="box">
                        <form>
                            <div class="form-group mb10">
                                <label>E-mail</label>
                                <input type="text" class="form-control" />
                            </div>
                            <p class="mb10">Daftarkan email anda untuk mendapatkan informasi terbaru tentang kami.</p>
                            <input type="submit" class="btn btn-primary" value="Sign Up" />
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-6">
                        <h4>Kontak Kami</h4>
                    </div>
                    <div class="col-md-12" >
                        <ul class="list list-social">
                            <li>
                                <a class="fa fa-phone box-icon" href="#" data-toggle="tooltip" title="082271111545"></a>
                            </li>
                            <li>
                                <a class="fa fa-inbox box-icon" href="#" data-toggle="tooltip" title="contact@celebestore.com"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <h4>Produk Terbaru</h4>
                    <ul class="thumb-list">
                        @forelse(App\Models\oc_category::all() as $category)
                            @if($category->parent_id==0)
                            <li>
                                <a href="{{ url('/'.$category->id) }}">
                                    <img src="{!! asset('image/category/catalog/'.$category->image) !!}" alt="Image Alternative text" title="The Hidden Power of the Heart" />
                                </a>
                                <h5 class="thumb-list-item-title">
                                    <p class="thumb-list-item-meta">{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['created_at'] !!}</p>
                                    <a href="{{ url('/'.$category->id) }}">
                                        {!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
                                    </a>
                                    <p class="thumb-list-item-desciption">
                                    {{ \Illuminate\Support\Str::words(App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['description'], 3,'....') }}
                                    </p>
                                </h5>
                            </li>
                            @endif
                        @empty
                            <li>
                                <a href="#">
                                    Category Tidak Ada
                                </a>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Copyright © 2017, Celebestore, All Rights Reserved</p>
                </div>
                <!-- 
                <div class="col-md-6 col-md-offset-2">
                    <div class="pull-right">
                        <ul class="list-inline list-payment">
                            <li>
                                <img src="{{asset('img/payment/american-express-curved-32px')}}.png" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/cirrus-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/discover-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/ebay-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/maestro-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/mastercard-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                            <li>
                                <img src="{{asset('img/payment/visa-curved-32px.png')}}" alt="Image Alternative text" title="Image Title" />
                            </li>
                        </ul>
                    </div>
                </div> 
                -->
            </div>
        </div>
    </div>
</footer>