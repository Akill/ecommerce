<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_tax_classRequest;
use App\Http\Requests\Updateoc_tax_classRequest;
use App\Repositories\oc_tax_classRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_tax_classController extends AppBaseController
{
    /** @var  oc_tax_classRepository */
    private $ocTaxClassRepository;

    public function __construct(oc_tax_classRepository $ocTaxClassRepo)
    {
        $this->ocTaxClassRepository = $ocTaxClassRepo;
    }

    /**
     * Display a listing of the oc_tax_class.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxClassRepository->pushCriteria(new RequestCriteria($request));
        $ocTaxClasses = $this->ocTaxClassRepository->all();

        return view('oc_tax_classes.index')
            ->with('ocTaxClasses', $ocTaxClasses);
    }

    /**
     * Show the form for creating a new oc_tax_class.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_tax_classes.create');
    }

    /**
     * Store a newly created oc_tax_class in storage.
     *
     * @param Createoc_tax_classRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_classRequest $request)
    {
        $input = $request->all();

        $ocTaxClass = $this->ocTaxClassRepository->create($input);

        Flash::success('Oc Tax Class saved successfully.');

        return redirect(route('ocTaxClasses.index'));
    }

    /**
     * Display the specified oc_tax_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            Flash::error('Oc Tax Class not found');

            return redirect(route('ocTaxClasses.index'));
        }

        return view('oc_tax_classes.show')->with('ocTaxClass', $ocTaxClass);
    }

    /**
     * Show the form for editing the specified oc_tax_class.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            Flash::error('Oc Tax Class not found');

            return redirect(route('ocTaxClasses.index'));
        }

        return view('oc_tax_classes.edit')->with('ocTaxClass', $ocTaxClass);
    }

    /**
     * Update the specified oc_tax_class in storage.
     *
     * @param  int              $id
     * @param Updateoc_tax_classRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_classRequest $request)
    {
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            Flash::error('Oc Tax Class not found');

            return redirect(route('ocTaxClasses.index'));
        }

        $ocTaxClass = $this->ocTaxClassRepository->update($request->all(), $id);

        Flash::success('Oc Tax Class updated successfully.');

        return redirect(route('ocTaxClasses.index'));
    }

    /**
     * Remove the specified oc_tax_class from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            Flash::error('Oc Tax Class not found');

            return redirect(route('ocTaxClasses.index'));
        }

        $this->ocTaxClassRepository->delete($id);

        Flash::success('Oc Tax Class deleted successfully.');

        return redirect(route('ocTaxClasses.index'));
    }
}
