<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_transaksiuserRequest;
use App\Http\Requests\Updatecs_transaksiuserRequest;
use App\Repositories\cs_transaksiuserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

class cs_transaksiuserController extends AppBaseController
{
    /** @var  cs_transaksiuserRepository */
    private $csTransaksiuserRepository;

    public function __construct(cs_transaksiuserRepository $csTransaksiuserRepo)
    {
        $this->csTransaksiuserRepository = $csTransaksiuserRepo;
    }

    /**
     * Display a listing of the cs_transaksiuser.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csTransaksiuserRepository->pushCriteria(new RequestCriteria($request));
        $csTransaksiusers = $this->csTransaksiuserRepository->all();

        return view('cs_transaksiusers.index')
            ->with('csTransaksiusers', $csTransaksiusers);
    }

    /**
     * Show the form for creating a new cs_transaksiuser.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_transaksiusers.create');
    }

    /**
     * Store a newly created cs_transaksiuser in storage.
     *
     * @param Createcs_transaksiuserRequest $request
     *
     * @return Response
     */
    public function store(Createcs_transaksiuserRequest $request)
    {
        if ($request->file('confirm')) {
            $file       = $request->file('confirm');
            $fileName   = $file->getClientOriginalName();
            $request->file('confirm')->move("image/saldo/".Auth::user()->id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $input['confirm'] = $fileName;

        $csTransaksiuser = $this->csTransaksiuserRepository->create($input);

        Flash::success('Cs Transaksiuser saved successfully.');

        return redirect(route('csTransaksiusers.index'));
    }

    /**
     * Display the specified cs_transaksiuser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csTransaksiuser = $this->csTransaksiuserRepository->findWithoutFail($id);

        if (empty($csTransaksiuser)) {
            Flash::error('Cs Transaksiuser not found');

            return redirect(route('csTransaksiusers.index'));
        }

        return view('cs_transaksiusers.show')->with('csTransaksiuser', $csTransaksiuser);
    }

    /**
     * Show the form for editing the specified cs_transaksiuser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csTransaksiuser = $this->csTransaksiuserRepository->findWithoutFail($id);

        if (empty($csTransaksiuser)) {
            Flash::error('Cs Transaksiuser not found');

            return redirect(route('csTransaksiusers.index'));
        }

        return view('cs_transaksiusers.edit')->with('csTransaksiuser', $csTransaksiuser);
    }

    /**
     * Update the specified cs_transaksiuser in storage.
     *
     * @param  int              $id
     * @param Updatecs_transaksiuserRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_transaksiuserRequest $request)
    {
        $csTransaksiuser = $this->csTransaksiuserRepository->findWithoutFail($id);

        if (empty($csTransaksiuser)) {
            Flash::error('Cs Transaksiuser not found');

            return redirect(route('csTransaksiusers.index'));
        }
        if ($request->file('confirm')) {
            $file       = $request->file('confirm');
            $fileName   = $file->getClientOriginalName();
            $request->file('confirm')->move("image/saldo/".Auth::user()->id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $input['confirm'] = $fileName;

        $csTransaksiuser = $this->csTransaksiuserRepository->update($input, $id);

        Flash::success('Cs Transaksiuser updated successfully.');

        return redirect(route('csTransaksiusers.index'));
    }

    /**
     * Remove the specified cs_transaksiuser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csTransaksiuser = $this->csTransaksiuserRepository->findWithoutFail($id);

        if (empty($csTransaksiuser)) {
            Flash::error('Cs Transaksiuser not found');

            return redirect(route('csTransaksiusers.index'));
        }

        $this->csTransaksiuserRepository->delete($id);

        Flash::success('Cs Transaksiuser deleted successfully.');

        return redirect(route('csTransaksiusers.index'));
    }
}
