<table class="table table-responsive" id="ocCategoryDescriptions-table">
    <thead>
        <tr>
        <th>Kategori</th>
        <th>Nama</th>
        <th>Judul Halaman</th>
        <th>Deskripsi Halaman</th>
        <th>Kata Kunci Halaman</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocCategoryDescriptions as $ocCategoryDescription)
        <tr>
            <td>{!! $ocCategoryDescription->oc_category_id !!}</td>
            <td>{!! $ocCategoryDescription->name !!}</td>
            <td>{!! $ocCategoryDescription->meta_title !!}</td>
            <td>{!! $ocCategoryDescription->meta_description !!}</td>
            <td>{!! $ocCategoryDescription->meta_keyword !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryDescriptions.destroy', $ocCategoryDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryDescriptions.show', [$ocCategoryDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryDescriptions.edit', [$ocCategoryDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>