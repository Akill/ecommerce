<?php

namespace App\Repositories;

use App\Models\oc_product_image;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_imageRepository
 * @package App\Repositories
 * @version October 14, 2017, 10:11 pm UTC
 *
 * @method oc_product_image findWithoutFail($id, $columns = ['*'])
 * @method oc_product_image find($id, $columns = ['*'])
 * @method oc_product_image first($columns = ['*'])
*/
class oc_product_imageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'image',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_image::class;
    }
}
