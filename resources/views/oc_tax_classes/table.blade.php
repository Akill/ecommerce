<table class="table table-responsive" id="ocTaxClasses-table">
    <thead>
        <tr>
            <th>Judul</th>
        <th>Deskripsi</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocTaxClasses as $ocTaxClass)
        <tr>
            <td>{!! $ocTaxClass->title !!}</td>
            <td>{!! $ocTaxClass->description !!}</td>
            <td>
                {!! Form::open(['route' => ['ocTaxClasses.destroy', $ocTaxClass->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocTaxClasses.show', [$ocTaxClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocTaxClasses.edit', [$ocTaxClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>