<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Celebestore | Invoice</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
@page { margin: 20px 20px; }
.header{
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    /*border: 1px solid #ddd;*/
}

th, td {
    text-align: center;
    /*padding: 8px;*/
}
.kiri {
    border-collapse: collapse;
    border-spacing: 0;
    width: 50%;
}
.kiri-th {
    text-align: left;
    /*padding: 8px;*/
}
.tinggi {
    height: 20px;
    /*text-align: left;*/
    padding: 0px;
}
.fnt {
  font-size: 12px;
}

</style>
</head>
<body>
<table class="header" border="1" style="width: 100%;">
    <tr>
      <td>
          <br>
          <div style="overflow-x:auto;">
            <table class="header">
              <tr>
                <th rowspan="3"><img src="{{asset('/cs.png')}}" width="70" class="img-circle" alt="User Image"/></th>
                <th colspan="3" class="kiri-th">CELEBESTORE</th>
                <th width="10%" class="kiri-th"></th>
              </tr>
              <tr>
                <td colspan="3" class="kiri-th">Jl.Andi Magga Amirullah No.1, Sengkang, Sulawesi Selatan</td>
              </tr>
              <tr>
                <td colspan="3" class="kiri-th">http://celebestore.com | sulaimanmide@gmail.com</td>
              </tr>
            </table>
          </div>
          <br>
          <div style="overflow-x:auto;">
            <table class="kiri" style="margin: 0px 35px; width: 100%;">
              <tr>
                <td class="kiri-th"><h1>INVOICE</h1></td>
              </tr>
            </table>
          </div>
          <div style="overflow-x:auto;">
            <table class="kiri" style="margin: 0px 35px; width: 100%;">
              <tr>
                <th class="kiri-th">#{{$nomor}}</td>
              </tr>
            </table>
          </div>
          <br>
          <div style="overflow-x:auto;">
            <table class="header" style="margin: 0px 35px;">
                <tbody>
                  <tr>
                    <td class="kiri-th" width="15%">Provinsi</td>
                    <td class="kiri-th" width="2%">:</td>
                    <td class="kiri-th" width="30%">{{$csShoppingcarts->province}}</td>
                    <td rowspan="5" class="kiri-th" width="10%"><br></td>
                    <td rowspan="5" class="kiri-th">{!! App\Models\cs_transfer_description::first()['description'] !!}</td>
                  </tr>
                  <tr>
                    <td class="kiri-th">Kota</td>
                    <td class="kiri-th">:</td>
                    <td class="kiri-th">{{$csShoppingcarts->city}}</td>
                  </tr>
                  <tr>
                    <td class="kiri-th">Kecamatan</td>
                    <td class="kiri-th">:</td>
                    <td class="kiri-th">{{$csShoppingcarts->districts}}</td>
                  </tr>
                  <tr>
                    <td class="kiri-th">Alamat</td>
                    <td class="kiri-th">:</td>
                    <td class="kiri-th">{{$csShoppingcarts->address}}</td>
                  </tr>
                  <tr>
                    <td class="kiri-th">Kode Pos</td>
                    <td class="kiri-th">:</td>
                    <td class="kiri-th">{{$csShoppingcarts->zip_kode}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <br>
          <div style="overflow-x:auto;">
            <table class="header" rules="rows" style="margin: 0px 35px;">
                <thead>
                    <tr>
                      <th class="kiri-th">Id</th>
                      <th class="kiri-th">Nama</th>
                      <th class="kiri-th">Harga</th>
                      <th class="kiri-th">Jumlah</th>
                      <th class="kiri-th">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                @php
                $data = json_decode($csShoppingcarts->content, true);
                $tot = 0; 
                @endphp
                @foreach($data as $key)
                  @php $tot += $key['subtotal']; @endphp
                  <tr>
                    <td class="kiri-th">{{$key['id']}}</td>
                    <td class="kiri-th">{{$key['name']}}</td>
                    <td class="kiri-th">{{$key['price']}}</td>
                    <td class="kiri-th">{{$key['qty']}}</td>
                    <td class="kiri-th">{{$key['subtotal']}}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
          </div>
          <br>
          <div align="right" style="margin: 0px 20px; width: 100%;">Makassar, @php echo date("d M Y") @endphp</div> 
          <br>
          <div style="overflow-x:auto;">
            <table class="header" style="margin: 0px 35px; width: 100%;">
              <tr>
                <td width="60%" rowspan="4"></td>
                <td class="kiri-th">Sub Total</td>
                <td class="kiri-th" width="2%">:</td>
                <td class="kiri-th">Rp. {{number_format($tot,2,',','.')}}</td>
              </tr>
              @php
                $stat = json_decode($csShoppingcarts->status, true);
              @endphp
              <tr>
                <td class="kiri-th">Pengiriman</td>
                <td class="kiri-th" width="2%">:</td>
                <td class="kiri-th">Rp. {{number_format($stat['value'],2,',','.')}}</td>
              </tr>
              <tr>
                <td class="kiri-th">Paket</td>
                <td class="kiri-th" width="2%">:</td>
                <td class="kiri-th">{{strtoupper($stat['paket'])}} - {{$stat['etd']}}</td>
              </tr>
              <tr>
                <td class="kiri-th">Total</td>
                <td class="kiri-th" width="2%">:</td>
                <td class="kiri-th">Rp. {{number_format($stat['value']+$tot,2,',','.')}}</td>
              </tr>
            </table>
          </div>
          <br>
          <p>
          Terima kasih sudah berkunjung ke toko online kami.
          <br>Temukan produk pilihan mu dengan harga terjangkau.
          </p> 

          <br>
      </td>
    </tr>
</table>
</body>
</html>
