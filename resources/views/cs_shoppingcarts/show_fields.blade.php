<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $csShoppingcart->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $csShoppingcart->user_id !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $csShoppingcart->address !!}</p>
</div>

<!-- Districts Field -->
<div class="form-group">
    {!! Form::label('districts', 'Districts:') !!}
    <p>{!! $csShoppingcart->districts !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $csShoppingcart->city !!}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{!! $csShoppingcart->province !!}</p>
</div>

<!-- Zip Kode Field -->
<div class="form-group">
    {!! Form::label('zip_kode', 'Zip Kode:') !!}
    <p>{!! $csShoppingcart->zip_kode !!}</p>
</div>

<!-- Bukti Field -->
<div class="form-group">
    {!! Form::label('bukti', 'Bukti:') !!}
    <p>{!! $csShoppingcart->bukti !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $csShoppingcart->content !!}</p>
</div>

<!-- Paket Field -->
<div class="form-group">
    {!! Form::label('paket', 'Paket:') !!}
    <p>{!! $csShoppingcart->paket !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $csShoppingcart->status !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $csShoppingcart->end_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $csShoppingcart->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $csShoppingcart->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $csShoppingcart->deleted_at !!}</p>
</div>

