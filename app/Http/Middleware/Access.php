<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Determine whether the current user is an administrator
        if (Auth::user()->access != 'admin') {
            abort(404);
        }
            return $next($request);
    }
}
