<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $csTransaksi->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $csTransaksi->user_id !!}</p>
</div>

<!-- Request Field -->
<div class="form-group">
    {!! Form::label('request', 'Request:') !!}
    <p>{!! $csTransaksi->request !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $csTransaksi->status !!}</p>
</div>

<!-- Confirm Field -->
<div class="form-group">
    {!! Form::label('confirm', 'Confirm:') !!}
    <p>
    <img src="/image/saldo/{!! $csTransaksi->user_id !!}/{!! $csTransaksi->confirm !!}">
    </p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $csTransaksi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $csTransaksi->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $csTransaksi->deleted_at !!}</p>
</div>

