<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_category
 * @package App\Models
 * @version October 14, 2017, 4:48 pm UTC
 *
 * @property string image
 * @property integer parent_id
 * @property boolean top
 * @property integer column
 * @property integer sort_order
 * @property boolean status
 */
class oc_category extends Model
{
    use SoftDeletes;

    public $table = 'oc_category';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'image',
        'user_id',
        'parent_id',
        'top',
        'column',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'image' => 'string',
        'parent_id' => 'integer',
        'top' => 'boolean',
        'column' => 'integer',
        'sort_order' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Get the oc_category that wrote the oc_category_description.
     */
    public function detail()
    {
        return $this->hasOne('App\Models\oc_category_description', 'oc_category_id','id');
    }
    
}
