<div class="form-group col-sm-6">
    <div class="form-group col-sm-6">
    <img src="http://placehold.it/250x250" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
    </div>
    <!-- Image Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('image', 'Gambar:') !!}
        {!! Form::file('image',null,array('id'=>'image','class'=>'form-control')) !!}
    </div>
</div>

<div class="form-group col-sm-6">
    <!-- Parent Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('parent_id', 'Parent Id:') !!}
        {!! Form::number('parent_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Column Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('column', 'Kolom:') !!}
        {!! Form::number('column', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Sort Order Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('sort_order', 'Urutan Pesanan:') !!}
        {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Top Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('top', 'Atas:') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('top', false) !!}
            {!! Form::checkbox('top', '1', null) !!} 1
        </label>
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('status', 'Status:') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', '1', null) !!} 1
        </label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCategories.index') !!}" class="btn btn-default">Batal</a>
</div>
