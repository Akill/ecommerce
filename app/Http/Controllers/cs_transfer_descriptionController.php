<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcs_transfer_descriptionRequest;
use App\Http\Requests\Updatecs_transfer_descriptionRequest;
use App\Repositories\cs_transfer_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cs_transfer_descriptionController extends AppBaseController
{
    /** @var  cs_transfer_descriptionRepository */
    private $csTransferDescriptionRepository;

    public function __construct(cs_transfer_descriptionRepository $csTransferDescriptionRepo)
    {
        $this->csTransferDescriptionRepository = $csTransferDescriptionRepo;
    }

    /**
     * Display a listing of the cs_transfer_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->csTransferDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $csTransferDescriptions = $this->csTransferDescriptionRepository->all();

        return view('cs_transfer_descriptions.index')
            ->with('csTransferDescriptions', $csTransferDescriptions);
    }

    /**
     * Show the form for creating a new cs_transfer_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('cs_transfer_descriptions.create');
    }

    /**
     * Store a newly created cs_transfer_description in storage.
     *
     * @param Createcs_transfer_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createcs_transfer_descriptionRequest $request)
    {
        $input = $request->all();

        $csTransferDescription = $this->csTransferDescriptionRepository->create($input);

        Flash::success('Cs Transfer Description saved successfully.');

        return redirect(route('csTransferDescriptions.index'));
    }

    /**
     * Display the specified cs_transfer_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $csTransferDescription = $this->csTransferDescriptionRepository->findWithoutFail($id);

        if (empty($csTransferDescription)) {
            Flash::error('Cs Transfer Description not found');

            return redirect(route('csTransferDescriptions.index'));
        }

        return view('cs_transfer_descriptions.show')->with('csTransferDescription', $csTransferDescription);
    }

    /**
     * Show the form for editing the specified cs_transfer_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $csTransferDescription = $this->csTransferDescriptionRepository->findWithoutFail($id);

        if (empty($csTransferDescription)) {
            Flash::error('Cs Transfer Description not found');

            return redirect(route('csTransferDescriptions.index'));
        }

        return view('cs_transfer_descriptions.edit')->with('csTransferDescription', $csTransferDescription);
    }

    /**
     * Update the specified cs_transfer_description in storage.
     *
     * @param  int              $id
     * @param Updatecs_transfer_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecs_transfer_descriptionRequest $request)
    {
        $csTransferDescription = $this->csTransferDescriptionRepository->findWithoutFail($id);

        if (empty($csTransferDescription)) {
            Flash::error('Cs Transfer Description not found');

            return redirect(route('csTransferDescriptions.index'));
        }

        $csTransferDescription = $this->csTransferDescriptionRepository->update($request->all(), $id);

        Flash::success('Cs Transfer Description updated successfully.');

        return redirect(route('csTransferDescriptions.index'));
    }

    /**
     * Remove the specified cs_transfer_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $csTransferDescription = $this->csTransferDescriptionRepository->findWithoutFail($id);

        if (empty($csTransferDescription)) {
            Flash::error('Cs Transfer Description not found');

            return redirect(route('csTransferDescriptions.index'));
        }

        $this->csTransferDescriptionRepository->delete($id);

        Flash::success('Cs Transfer Description deleted successfully.');

        return redirect(route('csTransferDescriptions.index'));
    }
}
