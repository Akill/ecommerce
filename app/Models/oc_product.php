<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product
 * @package App\Models
 * @version October 14, 2017, 8:11 pm UTC
 *
 * @property string model
 * @property string sku
 * @property string upc
 * @property string ean
 * @property string jan
 * @property string isbn
 * @property string mpn
 * @property string location
 * @property integer quantity
 * @property integer stock_status_id
 * @property string image
 * @property integer manufacturer_id
 * @property boolean shipping
 * @property integer price
 * @property integer points
 * @property integer tax_class_id
 * @property date date_available
 * @property decimal weight
 * @property integer weight_class_id
 * @property decimal length
 * @property decimal width
 * @property decimal height
 * @property integer length_class_id
 * @property boolean subtract
 * @property integer minimum
 * @property integer sort_order
 * @property boolean status
 * @property integer viewed
 */
class oc_product extends Model
{
    use SoftDeletes;

    public $table = 'oc_product';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'model',
        'user_id',
        'sku',
        'upc',
        'ean',
        'jan',
        'isbn',
        'mpn',
        'location',
        'quantity',
        'stock_status_id',
        'image',
        'manufacturer_id',
        'shipping',
        'price',
        'points',
        'tax_class_id',
        'date_available',
        'weight',
        'weight_class_id',
        'length',
        'width',
        'height',
        'length_class_id',
        'subtract',
        'minimum',
        'sort_order',
        'status',
        'viewed'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'model' => 'string',
        'sku' => 'string',
        'upc' => 'string',
        'ean' => 'string',
        'jan' => 'string',
        'isbn' => 'string',
        'mpn' => 'string',
        'location' => 'string',
        'quantity' => 'integer',
        'stock_status_id' => 'integer',
        'image' => 'string',
        'manufacturer_id' => 'integer',
        'shipping' => 'boolean',
        'price' => 'integer',
        'points' => 'integer',
        'tax_class_id' => 'integer',
        'date_available' => 'date',
        'weight_class_id' => 'integer',
        'length_class_id' => 'integer',
        'subtract' => 'boolean',
        'minimum' => 'integer',
        'sort_order' => 'integer',
        'status' => 'boolean',
        'viewed' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    
    /**
     * Get the oc_product that wrote the oc_product_description.
     */
    public function detail()
    {
        // return $this->belongsTo('App\Models\oc_product_description');
        return $this->hasOne('App\Models\oc_product_description', 'product_id','id');
    }

    
}
