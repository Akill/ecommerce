<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use Flash;
use Response;

use App\Http\Requests\Createoc_productRequest;
use App\Http\Requests\Updateoc_productRequest;
use App\Repositories\oc_productRepository;

use App\Http\Requests\Createoc_product_descriptionRequest;
use App\Http\Requests\Updateoc_product_descriptionRequest;
use App\Repositories\oc_product_descriptionRepository;

use App\Http\Requests\Createoc_product_to_categoryRequest;
use App\Http\Requests\Updateoc_product_to_categoryRequest;
use App\Repositories\oc_product_to_categoryRepository;

use App\Http\Requests\Createoc_product_imageRequest;
use App\Http\Requests\Updateoc_product_imageRequest;
use App\Repositories\oc_product_imageRepository;

use App\Models\oc_product;
use App\Models\oc_product_image;
use App\Models\oc_product_description;
use App\Models\oc_product_to_category;
use App\Models\oc_category;
use Cart;
use Session;

class cs_productController extends Controller
{
    /** @var  oc_product_imageRepository */
    private $ocProductImageRepository;

    /** @var  oc_product_to_categoryRepository */
    private $ocProductToCategoryRepository;

    /** @var  oc_productRepository */
    private $ocProductRepository;

    /** @var  oc_product_descriptionRepository */
    private $ocProductDescriptionRepository;

    public function __construct(oc_productRepository $ocProductRepo, oc_product_descriptionRepository $ocProductDescriptionRepo, oc_product_to_categoryRepository $ocProductToCategoryRepo, oc_product_imageRepository $ocProductImageRepo)
    {
        $this->ocProductImageRepository = $ocProductImageRepo;
        $this->ocProductRepository = $ocProductRepo;
        $this->ocProductDescriptionRepository = $ocProductDescriptionRepo;
        $this->ocProductToCategoryRepository = $ocProductToCategoryRepo;
    }

    /**
     * Display a listing of the front_product.
     *
     * @param $product id
     * @return Response
     */
    public function index($category,$product)
    {
        $ocProduct = oc_product::where('id',$product)->first();

        $ocProductImage = oc_product_image::where('product_id',$product)->get();
        $ocProductDescription = oc_product_description::where('product_id',$product)->first();
        $ocProductToCategory = oc_product_to_category::where('category_id',$category)->paginate(3);
        $ocCategory = oc_category::all();

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('cs.home'));
        }

        session()->push('products.recently_viewed', $product);

        return view('cs_product.index',[
            'ocProduct' => $ocProduct, 
            'ocProductImage' => $ocProductImage,
            'ocProductDescription' => $ocProductDescription, 
            'ocProductToCategory' => $ocProductToCategory,
            'ocCategory' => $ocCategory,
            'session' => session()->get('products.recently_viewed')
            ]
        );
    }

    /**
     * Display a listing of the front_product.
     *
     * @param $product id
     * @return Response
     */
    public function mobile($category,$product)
    {
        $ocProduct = oc_product::where('id',$product)->first();

        $ocProductImage = oc_product_image::where('product_id',$product)->get();
        $ocProductDescription = oc_product_description::where('product_id',$product)->first();
        $ocProductToCategory = oc_product_to_category::where('category_id',$category)->paginate(3);
        $ocCategory = oc_category::all();

        if (empty($ocProduct)) {
            Flash::error('Oc Product not found');

            return redirect(route('cs.home'));
        }

        //session get recently viewed
        // session()->get('key',$product);
        // Session::put('key',$product);
        session()->push('products.recently_viewed', $product);

        return view('mobile.cs_product.index',[
            'ocProduct' => $ocProduct, 
            'ocProductImage' => $ocProductImage,
            'ocProductDescription' => $ocProductDescription, 
            'ocProductToCategory' => $ocProductToCategory,
            'ocCategories' => $ocCategory,
            'session' => session()->get('products.recently_viewed')
            ]
        );
    }
}
