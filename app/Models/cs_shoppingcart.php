<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cs_shoppingcart
 * @package App\Models
 * @version October 15, 2017, 1:32 pm UTC
 *
 * @property integer user_id
 * @property string address
 * @property string districts
 * @property string city
 * @property string province
 * @property string zip_kode
 * @property string bukti
 * @property string content
 * @property string paket
 * @property string status
 * @property date end_date
 */
class cs_shoppingcart extends Model
{
    use SoftDeletes;

    public $table = 'cs_shoppingcart';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'address',
        'districts',
        'city',
        'province',
        'zip_kode',
        'bukti',
        'content',
        'paket',
        'status',
        'end_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'address' => 'string',
        'districts' => 'string',
        'city' => 'string',
        'province' => 'string',
        'zip_kode' => 'string',
        'bukti' => 'string',
        'content' => 'string',
        'paket' => 'string',
        'status' => 'string',
        'end_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
