<?php

namespace App\Repositories;

use App\Models\cs_transaksiuser;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_transaksiuserRepository
 * @package App\Repositories
 * @version October 29, 2017, 12:39 pm UTC
 *
 * @method cs_transaksiuser findWithoutFail($id, $columns = ['*'])
 * @method cs_transaksiuser find($id, $columns = ['*'])
 * @method cs_transaksiuser first($columns = ['*'])
*/
class cs_transaksiuserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'request',
        'status',
        'confirm'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_transaksiuser::class;
    }
}
