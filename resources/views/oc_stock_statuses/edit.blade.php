@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status Stok
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocStockStatus, ['route' => ['ocStockStatuses.update', $ocStockStatus->id], 'method' => 'patch']) !!}

                        @include('oc_stock_statuses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection