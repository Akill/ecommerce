	<div id="slide-out-right" class="side-nav">
		<!-- TABS -->
		<div class="sidebar-tabs">
			
			<!-- Tabs Menu -->
			<ul class="tabs">
				<li class="tab"><a class="active" href="#yourcart">Keranjang</a></li>
				<li class="tab"><a href="#latestblog">Pilih Kota</a></li>
			</ul>
			<!-- End Tabs Menu -->
		</div>
		<!-- Right Sidebar Tabs Content -->
		<div class="sidebar-tabs_content">
			<!-- Your Cart Tabs -->
			<div id="yourcart">
				<ol class="cart-item">
				@foreach(Cart::content() as $carts)
					<li>
						<div class="thumb">
							<img src="/image/product/sampul/{{ App\Models\oc_product::where('id',$carts->id)->first()['model'].'/'.App\Models\oc_product::where('id',$carts->id)->first()['image'] }}" alt="">
						</div>
						<div class="cart-delete">
							<a href="{{ asset('#') }}">
								<i class="fa fa-times"></i>
							</a>
						</div>
						<div class="cart-detail">
							<h3 class="product-name">
								<a href="{{url('/'.App\Models\oc_product_to_category::where('product_id',$carts->id)->first()['category_id'].'/'.$carts->id)}}">
								{{\Illuminate\Support\Str::words($carts->name, 3,'....')}}
								</a>
							</h3>
							<div class="price">
								<span>Harga</span> {{number_format($carts->price,2,',','.')}}
							</div>
							<div class="qty">
								<span>Jumlah</span> <input type="text" name="cart-quantity" id="cart-quantity" class="cart-quantity" value="{{$carts->qty}}" />
							</div>
						</div>
					</li>
				@endforeach
				</ol>

				<div class="cart-action">
					<div class="subtotal">
						<span class="title">Subtotal</span>
						<span class="price">Rp. {{ Cart::subtotal() }}</span>
					</div>
					<div class="total">
						<span class="title">Total</span>
						<span class="price">Rp. {{ Cart::total() }}</span>
					</div>
			        {!!Form::open(['route' => 'cs.mobile.checkout_create'])!!}
				        <button type="submit"  class="btn blue block btn-sm btn-squared">
				            Lanjutkan Pembayaran
				        </button>
						
				        <input type="hidden" name="mm" id="mm" style="display: none;" value="mobile">
				        <input type="hidden" name="checkout" id="checkout" style="display: none;" value="{{ Cart::content() }}"> 
			        {!!Form::close()!!}
				</div>
			</div>
			<!-- End Your Cart Tabs -->

			<!-- Latest Blog Tabs -->
			<div id="latestblog">
				<ol class="latest-blog">
                    @php $prov = RajaOngkir::Provinsi()->all(); @endphp
					@foreach($prov as $i)
                        @php $kota = RajaOngkir::Kota()->byProvinsi($i['province_id'])->get(); @endphp
                        <h5>{{$i['province']}}</h5>
                        @foreach($kota as $ko)
	                    <li>
							<span class="meta">{{$ko['postal_code']}}</span>
							<h3 class="blog-title"><a href="{{ url('/mobile/search/'.$ko['city_id']) }}"> {{$ko['type']}} {{$ko['city_name']}}</a></h3>
						</li>    
                        @endforeach
                    @endforeach
				</ol>
			</div> 
			<!-- End Latest Blog Tabs -->
		</div>
		<!-- End Right Sidebar Tabs Content -->
	</div>