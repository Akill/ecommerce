<?php

namespace App\Repositories;

use App\Models\cs_saldo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_saldoRepository
 * @package App\Repositories
 * @version October 28, 2017, 8:38 am UTC
 *
 * @method cs_saldo findWithoutFail($id, $columns = ['*'])
 * @method cs_saldo find($id, $columns = ['*'])
 * @method cs_saldo first($columns = ['*'])
*/
class cs_saldoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'saldo',
        'akun'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_saldo::class;
    }
}
