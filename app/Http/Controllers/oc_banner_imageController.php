<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_banner_imageRequest;
use App\Http\Requests\Updateoc_banner_imageRequest;
use App\Repositories\oc_banner_imageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_banner_imageController extends AppBaseController
{
    /** @var  oc_banner_imageRepository */
    private $ocBannerImageRepository;

    public function __construct(oc_banner_imageRepository $ocBannerImageRepo)
    {
        $this->ocBannerImageRepository = $ocBannerImageRepo;
    }

    /**
     * Display a listing of the oc_banner_image.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerImageRepository->pushCriteria(new RequestCriteria($request));
        $ocBannerImages = $this->ocBannerImageRepository->all();

        return view('oc_banner_images.index')
            ->with('ocBannerImages', $ocBannerImages);
    }

    /**
     * Show the form for creating a new oc_banner_image.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_banner_images.create');
    }

    /**
     * Store a newly created oc_banner_image in storage.
     *
     * @param Createoc_banner_imageRequest $request
     *
     * @return Response
     */
    public function store(Createoc_banner_imageRequest $request)
    {
        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/banner/".$request->product_id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['image'] = $fileName;

        $ocBannerImage = $this->ocBannerImageRepository->create($input);

        Flash::success('Oc Banner Image saved successfully.');

        return redirect(route('ocBannerImages.index'));
    }

    /**
     * Display the specified oc_banner_image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            Flash::error('Oc Banner Image not found');

            return redirect(route('ocBannerImages.index'));
        }

        return view('oc_banner_images.show')->with('ocBannerImage', $ocBannerImage);
    }

    /**
     * Show the form for editing the specified oc_banner_image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            Flash::error('Oc Banner Image not found');

            return redirect(route('ocBannerImages.index'));
        }

        return view('oc_banner_images.edit')->with('ocBannerImage', $ocBannerImage);
    }

    /**
     * Update the specified oc_banner_image in storage.
     *
     * @param  int              $id
     * @param Updateoc_banner_imageRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_banner_imageRequest $request)
    {
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            Flash::error('Oc Banner Image not found');

            return redirect(route('ocBannerImages.index'));
        }

        if ($request->file('image')) {
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("image/banner/".$request->product_id."/", $fileName);
        }
        else {
            $fileName = 'default.jpg';
        }
        $input = $request->all();

        $input['image'] = $fileName;

        $ocBannerImage = $this->ocBannerImageRepository->update($input, $id);

        Flash::success('Oc Banner Image updated successfully.');

        return redirect(route('ocBannerImages.index'));
    }

    /**
     * Remove the specified oc_banner_image from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            Flash::error('Oc Banner Image not found');

            return redirect(route('ocBannerImages.index'));
        }

        $this->ocBannerImageRepository->delete($id);

        Flash::success('Oc Banner Image deleted successfully.');

        return redirect(route('ocBannerImages.index'));
    }
}
