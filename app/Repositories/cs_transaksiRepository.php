<?php

namespace App\Repositories;

use App\Models\cs_transaksi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cs_transaksiRepository
 * @package App\Repositories
 * @version October 29, 2017, 11:47 am UTC
 *
 * @method cs_transaksi findWithoutFail($id, $columns = ['*'])
 * @method cs_transaksi find($id, $columns = ['*'])
 * @method cs_transaksi first($columns = ['*'])
*/
class cs_transaksiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'request',
        'status',
        'confirm'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cs_transaksi::class;
    }
}
