<!-- Request Field -->
<div class="form-group col-sm-6">
    {!! Form::label('request', 'Request Jumlah Deposite:') !!}
    {!! Form::text('request', null, ['class' => 'form-control', 'placeholder' => '50000']) !!}
</div>

<div class="form-group col-sm-12">
<img src="http://placehold.it/480x480" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
</div>

<!-- Confirm Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirm', 'Confirm:') !!}
    {!! Form::file('confirm', null, array('id'=>'confirm','class'=>'form-control')) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('csTransaksiusers.index') !!}" class="btn btn-default">Cancel</a>
</div>