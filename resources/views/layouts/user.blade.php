<li class="header">Menu Pelanggan</li>

<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li>
	@if(Session::get('hp') != NULL)
    <a href="{!! url(Session::get('hp').'/') !!}">
        <i class="fa fa-home"></i> <span>Celebestore</span>
    </a>
    @else
    <a href="{!! url('/') !!}">
        <i class="fa fa-home"></i> <span>Celebestore</span>
    </a>
    @endif
</li>

<li class="{{ Request::is('csTransaksiusers*') ? 'active' : '' }}">
    <a href="{!! route('csTransaksiusers.index') !!}"><i class="fa fa-money"></i><span>Deposite Saldo</span></a>
</li>

<li class="{{ Request::is('csShoppingcarts*') ? 'active' : '' }}">
    <a href="{!! route('csShoppingcarts.index') !!}"><i class="fa fa-shopping-cart"></i><span>Keranjang Belanja</span></a>
</li>

<li class="header">Menu Pelapak</li>

@include('layouts.menu.products.menu')

<li class="{{ Request::is('csOrders*') ? 'active' : '' }}">
    <a href="{!! route('csOrders.index') !!}"><i class="fa fa-user-plus"></i><span>Pesanan Pelanggan</span></a>
</li>


