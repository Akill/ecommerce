<table class="table table-responsive" id="ocCategories-table">
    <thead>
        <tr>
            <th>Gambar</th>
            <th>Parent Id</th>
            <th>Atas</th>
            <th>Kolom</th>
            <th>Urutan Pesanan</th>
            <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ocCategories as $ocCategory)
        <tr>
            <td>
            <img src="{!! asset('image/category/catalog/'.$ocCategory->image) !!}" alt="Image" width="100" height="100">
            </td>
            <td>{!! $ocCategory->parent_id !!}</td>
            <td>{!! $ocCategory->top !!}</td>
            <td>{!! $ocCategory->column !!}</td>
            <td>{!! $ocCategory->sort_order !!}</td>
            <td>{!! $ocCategory->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategories.destroy', $ocCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategories.show', [$ocCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategories.edit', [$ocCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>