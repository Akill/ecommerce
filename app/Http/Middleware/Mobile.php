<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Auth;

class Mobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return redirect('/mobile/login');
        }
        // return redirect('/mobile/');

        return $next($request);
    }
}
