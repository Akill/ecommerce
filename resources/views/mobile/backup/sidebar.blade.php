<div id="slide-out-left" class="side-nav">
	<!-- Form Search -->
	<div class="top-left-nav">
		<form class="sign-up" name="oke" method="POST" action="{{url('/mobile/search')}}">
		{!! csrf_field() !!}
			<div class="searchbar">
				<i class="fa fa-search"></i>
				<input type="text" id="search" name="search" placeholder="Cari yang anda butuhkan" onkeypress="return runScript(event)" >
			</div>
	    </form>
	</div>
	<!-- End Form Search -->
	
	<!-- App/Site Menu -->
	<div id="main-menu">
		<ul>
			<li><a href="{{ url('/mobile') }}"><i class="fa fa-home"></i> Celebestore</a></li>
			<li><a href="{{ url('/mobile/pulsa') }}"><i class="fa fa-mobile-phone"></i> Pulsa</a></li>
			<!-- <li><a href="{{ url('/mobile/paketdata') }}"><i class="fa fa-qrcode"></i> Paket Data</a></li> -->
			<!-- <li><a href="{{ url('/mobile/gojekgrab') }}"><i class="fa fa-road"></i> Gojek & Grab</a></li> -->
			<!-- <li><a href="{{ url('/mobile/pln') }}"><i class="fa fa-bolt"></i> Listrik</a></li> -->
			<li class="has-sub"><a href="{{ url('#') }}"><i class="fa fa-th-list"></i> Categories</a>
				<ul>
			    @foreach($ocCategories as $category)
					<li>
						<a href="{{ url('/mobile/'.$category->id) }}">
						{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
						</a>
					</li>
				@endforeach
				</ul>
			</li>
			<li><a href="{{ url('/mobile/cart') }}"><i class="fa fa-shopping-bag"></i> Keranjang</a></li>
		</ul>
	</div>
	<!-- End Site/App Menu -->
</div>
<SCRIPT LANGUAGE="javascript">
function runScript(e) {
    if (e.keyCode == 13) {
        document.oke.submit();
        return false;
    }
}
</SCRIPT>