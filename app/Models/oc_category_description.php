<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_category_description
 * @package App\Models
 * @version October 14, 2017, 5:09 pm UTC
 *
 * @property integer oc_category_id
 * @property integer language_id
 * @property string name
 * @property string description
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 */
class oc_category_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_category_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'oc_category_id',
        'user_id',
        'language_id',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'oc_category_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'meta_keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Get the oc_category that wrote the oc_category_description.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\oc_category');
    }
}
