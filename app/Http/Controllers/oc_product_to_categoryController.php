<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_to_categoryRequest;
use App\Http\Requests\Updateoc_product_to_categoryRequest;
use App\Repositories\oc_product_to_categoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\oc_product_to_category;

class oc_product_to_categoryController extends AppBaseController
{
    /** @var  oc_product_to_categoryRepository */
    private $ocProductToCategoryRepository;

    public function __construct(oc_product_to_categoryRepository $ocProductToCategoryRepo)
    {
        $this->ocProductToCategoryRepository = $ocProductToCategoryRepo;
    }

    /**
     * Display a listing of the oc_product_to_category.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocProductToCategories = $this->ocProductToCategoryRepository->all();
        if (!Auth::user()->status) {
            $ocProductToCategories = oc_product_to_category::where('user_id',Auth::user()->id)->get();
        }
        return view('oc_product_to_categories.index')
            ->with('ocProductToCategories', $ocProductToCategories);
    }

    /**
     * Show the form for creating a new oc_product_to_category.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_to_categories.create');
    }

    /**
     * Store a newly created oc_product_to_category in storage.
     *
     * @param Createoc_product_to_categoryRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_categoryRequest $request)
    {
        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        $ocProductToCategory = $this->ocProductToCategoryRepository->create($input);

        Flash::success('Oc Product To Category saved successfully.');

        return redirect(route('ocProductToCategories.index'));
    }

    /**
     * Display the specified oc_product_to_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            Flash::error('Oc Product To Category not found');

            return redirect(route('ocProductToCategories.index'));
        }

        return view('oc_product_to_categories.show')->with('ocProductToCategory', $ocProductToCategory);
    }

    /**
     * Show the form for editing the specified oc_product_to_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            Flash::error('Oc Product To Category not found');

            return redirect(route('ocProductToCategories.index'));
        }

        return view('oc_product_to_categories.edit')->with('ocProductToCategory', $ocProductToCategory);
    }

    /**
     * Update the specified oc_product_to_category in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_to_categoryRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_categoryRequest $request)
    {
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        $input = $request->all();

        $input['user_id'] = Auth::user()->id;

        if (empty($ocProductToCategory)) {
            Flash::error('Oc Product To Category not found');

            return redirect(route('ocProductToCategories.index'));
        }

        $ocProductToCategory = $this->ocProductToCategoryRepository->update($input, $id);

        Flash::success('Oc Product To Category updated successfully.');

        return redirect(route('ocProductToCategories.index'));
    }

    /**
     * Remove the specified oc_product_to_category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            Flash::error('Oc Product To Category not found');

            return redirect(route('ocProductToCategories.index'));
        }

        $this->ocProductToCategoryRepository->delete($id);

        Flash::success('Oc Product To Category deleted successfully.');

        return redirect(route('ocProductToCategories.index'));
    }
}
